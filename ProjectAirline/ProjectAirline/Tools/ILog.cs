﻿using ProjectAirline.UserInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace ProjectAirline.Tools
{
    public static class ILog
    {
        public static void Initialize()
        {
        }

        public static void Write(string message)
        {
            Application.Current.Dispatcher.InvokeAsync(() =>
            {
                if ((Application.Current.MainWindow as MainWindow) != null)
                {
                    string fullMessage = Time.CurrentTime.ToString() + " => " + message;

                    (Application.Current.MainWindow as MainWindow).AddLogData(
                        fullMessage, Palette.GetBrush(System.Drawing.Color.Black));
                }
            });
        }

        public static void Close()
        {
        }
    }
}
