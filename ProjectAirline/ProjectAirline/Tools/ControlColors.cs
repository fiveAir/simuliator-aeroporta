﻿using System.Drawing;

namespace ProjectAirline
{
    public class ControlColors
    {
        public Color _colorBack { get; }
        public Color _colorControls { get; }
        public Color _colorText { get; }
        public Color _colorBody { get; }
        public Color _colorTools { get; }

        public ControlColors(Color back, Color controls, Color text, Color body, Color tools)
        {
            _colorBack = back;
            _colorControls = controls;
            _colorText = text;
            _colorBody = body;
            _colorTools = tools;
        }
    }
}
