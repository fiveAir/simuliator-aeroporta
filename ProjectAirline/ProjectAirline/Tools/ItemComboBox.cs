﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAirline.Tools
{
    public class ItemComboBox
    {
        public object Item { get; }
        private string content;

        public ItemComboBox(object item, string content)
        {
            this.content = content;
            Item = item;
        }

        public override string ToString()
        {
            return content;
        }
    }
}
