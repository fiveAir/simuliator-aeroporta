﻿namespace ProjectAirline
{
    /// <summary>
    /// Класс преобразований цвета
    /// </summary>
    public static class Palette
    {
        /// <summary>
        /// Преобразует System.Drawing.Color в Brush
        /// </summary>
        /// <param name="color">Экземпляр System.Drawing.Color</param>
        /// <returns></returns>
        public static System.Windows.Media.SolidColorBrush GetBrush(System.Drawing.Color color)
        {
            System.Windows.Media.Color mediaColor = new System.Windows.Media.Color()
            {
                A = color.A,
                B = color.B,
                G = color.G,
                R = color.R
            };
            System.Windows.Media.SolidColorBrush brush = new System.Windows.Media.SolidColorBrush(mediaColor);
            return brush;
        }

        /// <summary>
        /// Преобразует строковое шестнадцатеричное представление цвета в Brush
        /// </summary>
        /// <param name="colorStr">Строка, представляющая цвет</param>
        /// <returns></returns>
        public static System.Windows.Media.SolidColorBrush GetBrush(string colorStr)
        {
            System.Windows.Media.Color mediaColor = (System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString(colorStr);
            System.Windows.Media.SolidColorBrush brush = new System.Windows.Media.SolidColorBrush(mediaColor);
            return brush;
        }
    }
}
