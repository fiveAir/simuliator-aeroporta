﻿using ProjectAirline.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectAirline.Core;

namespace ProjectAirline.Tools.DataBaseTools
{
    public abstract class Aircraft : Connection
    {

        public static СамолетИгрыИгрока GetAircraft(long id_aircraft)
        {
            var query =
                from самолет in db.СамолетИгрыИгрока
                where самолет.id == id_aircraft
                select самолет;
            if (query.ToList().Count == 0)
            {
                return null;
            }
            return query.ToList()[0];
        }

        public static СамолетИгрока GetAircraft(СамолетИгрыИгрока aircraft)
        {
            return
                (from самолет in db.СамолетИгрока
                 where самолет.id_СамолетаИгрыИгрока == aircraft.id
                 select самолет).ToList()[0];
        }

        public static СамолетИгрока GetСамолетИгрока(long id_СамолетИгрыИгрока)
        {
            return
                (from самолет in db.СамолетИгрока
                 where самолет.id_СамолетаИгрыИгрока == id_СамолетИгрыИгрока
                 select самолет).ToList()[0];
        }

        /// <summary>
        /// Возвращает список имеющихся у игрока самолётов
        /// </summary>
        /// <param name="player">Игрок, владелец самолётов</param>
        /// <returns>Список самолётов игрока</returns>
        public static List<СамолетИгрока> GetAircrafts()
        {
            Game.DBMutex.WaitOne();
            var query =
                from самолет in db.СамолетИгрока
                where самолет.id_Игрок == Game.Player.id
                select самолет;
            Game.DBMutex.ReleaseMutex();
            return query.ToList();
            
        }

        /// <summary>
        /// Возвращает список имеющихся у игрока грузовых самолётов
        /// </summary>
        /// <param name="player">Игрок, владелец самолётов</param>
        /// <returns>Список самолётов игрока</returns>
        public static List<СамолетИгрока> GetCargoAircrafts()
        {
            Game.DBMutex.WaitOne();
            var query =
                from самолет in db.СамолетИгрока
                where самолет.id_Игрок == Game.Player.id && самолет.СамолетИгрыИгрока.МодельСамолета.ГрузовойСамолет != null
                select самолет;
            Game.DBMutex.ReleaseMutex();
            return query.ToList();
            
        }

        /// <summary>
        /// Получить стоимость продажи самолета
        /// </summary>
        /// <param name="aircraft">Самолет для продажи</param>
        /// <returns>Стоимость продажи</returns>
        public static double GetSellPrice(СамолетИгрока aircraft)
        {
            return aircraft.СамолетИгрыИгрока.МодельСамолета.СтандартнаяСтоимость / 2;
        }
        /// <summary>
        /// Возвращает список имеющихся у игрока пассажирских самолётов
        /// </summary>
        /// <param name="player">Игрок, владелец самолётов</param>
        /// <returns>Список самолётов игрока</returns>
        public static List<СамолетИгрока> GetPassengerAircrafts()
        {
            Game.DBMutex.WaitOne();
            var query =
                from самолет in db.СамолетИгрока
                where самолет.id_Игрок == Game.Player.id && самолет.СамолетИгрыИгрока.МодельСамолета.ПассажирскийСамолет != null
                select самолет;
            Game.DBMutex.ReleaseMutex();
            return query.ToList();
            
        }
        
        #region Определение типа самолёта
        public static ПассажирскийСамолет IsPassenger(СамолетИгрока aircraft)
        {
            return IsPassenger(aircraft.СамолетИгрыИгрока);
        }

        public static ПассажирскийСамолет IsPassenger(СамолетИгрыИгрока aircraft)
        {
            return IsPassenger(aircraft.МодельСамолета);
        }

        public static ПассажирскийСамолет IsPassenger(МодельСамолета aircraft)
        {
            return aircraft.ПассажирскийСамолет ?? null;
        }

        public static ГрузовойСамолет IsCargo(СамолетИгрока aircraft)
        {
            return IsCargo(aircraft.СамолетИгрыИгрока);
        }

        public static ГрузовойСамолет IsCargo(СамолетИгрыИгрока aircraft)
        {
            return IsCargo(aircraft.МодельСамолета);
        }

        public static ГрузовойСамолет IsCargo(МодельСамолета aircraft)
        {
            return aircraft.ГрузовойСамолет ?? null;
        }
        #endregion

        public static СамолетИгрыИгрока GenerateAircraft()
        {
            Game.DBMutex.WaitOne();
            DateTime minBuildDate = new DateTime(1980, 1, 1);
            var query =
                from МодельСамолета in db.МодельСамолета
                select МодельСамолета;
            List<МодельСамолета> aircraftsModels = query.ToList();
            int randomRow = Rand.Get(0, aircraftsModels.Count - 1);
            СамолетИгрыИгрока aircraft = new СамолетИгрыИгрока()
            {
                id_Игрок = Game.Player.id,
                МодельСамолета = aircraftsModels[randomRow],
                ДатаСборки = Rand.GenDate(minBuildDate, Time.CurrentTime).ToString()
            };
            Game.DBMutex.ReleaseMutex();
            return aircraft;
           
        }

        /// <summary>
        /// Создание нескольких самолетов игры, которые можно будет купить
        /// </summary>
        /// <param name="count">Количество самолетов</param>
        /// <param name="currentTime">Текущее время игры</param>
        /// <returns></returns>
        public static List<СамолетИгрыИгрока> GenerateFewAircrafts(int count)
        {
            Game.DBMutex.WaitOne();
            List<СамолетИгрыИгрока> aircrafts = new List<СамолетИгрыИгрока>();
            for (int i = 0; i < count; i++)
            {
                aircrafts.Add(GenerateAircraft());
            }
            Game.DBMutex.ReleaseMutex();
            return aircrafts;
            
        }

        /// <summary>
        /// Удаление информации о самолете игрока из БД
        /// </summary>
        /// <param name="PlayersAircraft">Самолет Игрока</param>
        public static void RemovePlayersAircraft(СамолетИгрока PlayersAircraft)
        {
            Game.DBMutex.WaitOne();
            ДоговорНаСамолет contract = PlayersAircraft.ДоговорНаСамолет;
            СамолетИгрыИгрока gamesAircraft = PlayersAircraft.СамолетИгрыИгрока;
            db.СамолетИгрока.Remove(PlayersAircraft);
            db.СамолетИгрыИгрока.Remove(gamesAircraft);
            if (contract.Покупка != null)
                db.Покупка.Remove(contract.Покупка);
            if (contract.Аренда != null)
                db.Аренда.Remove(contract.Аренда);
            if (contract.Лизинг != null)
                db.Лизинг.Remove(contract.Лизинг);
            db.ДоговорНаСамолет.Remove(contract);
            db.SaveChanges();
            Game.DBMutex.ReleaseMutex();
        }

        /// <summary>
        /// Продажа самолета игрока
        /// </summary>
        /// <param name="PlayersAircraft">Самолет для продажы</param>
        public static void SellAircraft(СамолетИгрока PlayersAircraft)
        {
            Game.DBMutex.WaitOne();
            Player.ChangeBalance(PlayersAircraft.СамолетИгрыИгрока.МодельСамолета.СтандартнаяСтоимость / 2);
            RemovePlayersAircraft(PlayersAircraft);
            Game.DBMutex.ReleaseMutex();
        }
        
        /// <summary>
        /// Отмена аренды самолета
        /// </summary>
        /// <param name="PlayersAircraft">Самолет в аренде</param>
        public static void StopRent(СамолетИгрока PlayersAircraft)
        {
            Game.DBMutex.WaitOne();
            RemovePlayersAircraft(PlayersAircraft);
            Game.DBMutex.ReleaseMutex();
        }

        /// <summary>
        /// Отмена лизинга самолета
        /// </summary>
        /// <param name="PlayersAircraft">Самолет в лизинге</param>
        public static void StopLease(СамолетИгрока PlayersAircraft)
        {
            Game.DBMutex.WaitOne();
            RemovePlayersAircraft(PlayersAircraft);
            Game.DBMutex.ReleaseMutex();
        }

        /// <summary>
        /// Определить, находится ли самолет в выбранном аэропорте
        /// </summary>
        /// <param name="aircraft">Самолет</param>
        /// <param name="airport">Аэропорт</param>
        /// <returns></returns>
        public static bool IsAircraftInAirport(СамолетИгрыИгрока aircraft, Аэропорт airport)
        {
            var query =
                from СамолетИгрока in db.СамолетИгрока
                where СамолетИгрока.id_СамолетаИгрыИгрока == aircraft.id 
                && СамолетИгрока.id_ТекущийАэропорт==airport.id
                select СамолетИгрока;
            if (query.ToList().Count > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Проверяет готовность самолета к вылету, назначенному в раписании
        /// </summary>
        /// <param name="record">Строка расписания</param>
        /// <returns>Готовность</returns>
        public static bool IsAircraftReadyToFlight(Расписание record)
        {
            Аэропорт startAirport = TimeTable.GetFlight(record.ДоговорНаРейс).Перелет.Аэропорт1;
            return IsAircraftInAirport(record.СамолетИгрыИгрока, startAirport);
        }

        /// <summary>
        /// Расчитать время полета самолета
        /// </summary>
        /// <param name="aircraft">Самолет</param>
        /// <param name="fly">Перелет</param>
        /// <returns>Время полета</returns>
        public static TimeSpan GetFlyingTime(СамолетИгрыИгрока aircraft, Перелет fly)
        {
            double time=Flight.GetDistance(fly) / aircraft.МодельСамолета.Скорость;
            int hours = (int)time;
            double minutes;
            if (hours != 0)
                minutes = time % hours;
            else
                minutes = time;
            minutes = minutes * 60;
            return new TimeSpan(hours, (int)minutes, 0);
        }

        /// <summary>
        /// Проверка соответствует ли заданный самолет рейсу
        /// </summary>
        /// <param name="aircraft">Самолет</param>
        /// <param name="flight">Рейс</param>
        public static void AircraftToFlight(СамолетИгрыИгрока aircraft, РейсИгрыИгрока flight)
        {
            if (flight.ПассажирскийРейс!= null)
            {
                if (aircraft.МодельСамолета.ПассажирскийСамолет.КоличествоМест < flight.ПассажирскийРейс.МаксимальноеКоличествоПассажиров)
                {
                    throw new Exception("Выбранный самолет не соответствует\n" +
                                        " требуемой пассажировместивомсти");
                }

            }
            else
            {
                if (flight.ГрузовойРейс!= null)
                {
                    if (aircraft.МодельСамолета.ГрузовойСамолет.Грузоподъемность<flight.ГрузовойРейс.ВесГруза)
                    {
                        throw new Exception("Выбранный самолет не соответствует\n" +
                                            "    требуемой грузоподъемности");
                    }
                }
            }
            if (Flight.GetDistance(flight.Перелет)>aircraft.МодельСамолета.МаксимальнаяДальность)
            {
                throw new Exception("Выбранный самолет не соответствует\n" +
                                    "   требуемой дальности полета");
            }
            return;
        }

        /// <summary>
        /// Проверка соответствует ли заданный самолет перелету
        /// </summary>
        /// <param name="aircraft">Самолет</param>
        /// <param name="from">Аэропорт отправления</param>
        /// <param name="to">Аэропорт прибытия</param>
        public static void AircraftToFlight(СамолетИгрыИгрока aircraft,Аэропорт from, Аэропорт to)
        {
            Перелет fly = new Перелет()
            {
                Аэропорт1 = from,
                Аэропорт = to
            };
            if (Flight.GetDistance(fly)>aircraft.МодельСамолета.МаксимальнаяДальность)
            {
                throw new Exception("Выбранный самолет не соответствует\n" +
                                    "   требуемой дальности полета");
            }
            return;
        }

        /// <summary>
        /// Получить бортовой номер самолета
        /// </summary>
        /// <param name="aircraft">Самолет</param>
        /// <returns>Бортовой номер</returns>
        public static string GetTailNumber(СамолетИгрыИгрока aircraft)
        {
            return "PP"+aircraft.id.ToString();
        }
    }
}
