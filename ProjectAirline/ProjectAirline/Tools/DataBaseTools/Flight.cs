﻿using ProjectAirline.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectAirline.Core;
using ProjectAirline.Events;
using System.Device.Location;

namespace ProjectAirline.Tools.DataBaseTools
{
    public abstract class Flight : Connection
    {
        public enum dayTime{ Утро, День, Вечер, Ночь };



        public static ДоговорНаРейс GetContract(long id_contract)
        {
            var query =
                from договор in db.ДоговорНаРейс
                 where договор.id == id_contract
                 select договор;
            if (query.ToList().Count == 0)
            {
                return null;
            }
            return query.ToList()[0];
        }

        public static РейсИгрыИгрока GetFlight(ДоговорНаРейс Contract)
        {
            var query =
                from рейс in db.РейсИгрыИгрока
                 where рейс.id_Договор == Contract.id
                 select рейс;
            return query.ToList()[0];
        }

        /// <summary>
        /// Возвращает всех рейсов
        /// </summary>
        /// <param name="player">Игрок, которому доступны рейсы</param>
        /// <returns>Список рейсов</returns>
        public static List<РейсИгрыИгрока> GetAllFlights()
        {
            Game.DBMutex.WaitOne();
            var query =
                from рейс in db.РейсИгрыИгрока
                where рейс.id_Игрок == Game.Player.id
                && рейс.ДоговорНаРейс.Неустойка!=0
                select рейс;
            Game.DBMutex.ReleaseMutex();
            return query.ToList();
        }

        /// <summary>
        /// Возвращает список рейсов, доступных для взятия
        /// </summary>
        /// <returns>Список рейсов</returns>
        public static List<РейсИгрыИгрока> GetPossibleFlights()
        {
            Game.DBMutex.WaitOne();
            var query =
                from рейс in db.РейсИгрыИгрока
                where рейс.id_Игрок == Game.Player.id && рейс.ДоговорНаРейс==null
                                && рейс.ДоговорНаРейс.Неустойка != 0
                select рейс;
            Game.DBMutex.ReleaseMutex();
            return query.ToList();
        }

        /// <summary>
        /// Возвращает список выбранных рейсов
        /// </summary>
        /// <param name="player">Игрок, взявший рейсы</param>
        /// <returns>Список рейсов</returns>
        public static List<РейсИгрыИгрока> GetSelectedFlights()
        {
            Game.DBMutex.WaitOne();
            var query =
                from рейс in db.РейсИгрыИгрока
                where рейс.id_Игрок == Game.Player.id && рейс.ДоговорНаРейс!=null
                && (рейс.ГрузовойРейс!=null || рейс.ПассажирскийРейс!= null)
                select рейс;
            Game.DBMutex.ReleaseMutex();
            return query.ToList();
        }

        /// <summary>
        /// Возвращает список выбранных грузовых рейсов
        /// </summary>
        /// <param name="player">Игрок, взявший рейсы</param>
        /// <returns>Список рейсов</returns>
        public static List<РейсИгрыИгрока> GetSelectedCargoFlights()
        {
            Game.DBMutex.WaitOne();
            var query =
                from рейс in db.РейсИгрыИгрока
                join договор in db.ДоговорНаРейс
                    on рейс.id_Договор equals договор.id
                where рейс.id_Игрок == Game.Player.id && рейс.ГрузовойРейс != null
                select рейс;
            Game.DBMutex.ReleaseMutex();
            return query.ToList();
        }

        /// <summary>
        /// Возвращает список выбранных пассажирский рейсов
        /// </summary>
        /// <param name="player">Игрок, взявший рейсы</param>
        /// <returns>Список рейсов</returns>
        public static List<РейсИгрыИгрока> GetSelectedPassengerFlights()
        {
            Game.DBMutex.WaitOne();
            var query =
                from рейс in db.РейсИгрыИгрока
                join договор in db.ДоговорНаРейс
                    on рейс.id_Договор equals договор.id
                where рейс.id_Игрок == Game.Player.id && рейс.ПассажирскийРейс != null
                select рейс;
            Game.DBMutex.ReleaseMutex();
            return query.ToList();
        }

        /// <summary>
        /// Сгенерировать Перелет между двумя случайными аэропортами
        /// </summary>
        /// <returns>Перелет</returns>
        public static Перелет GetFly()
        {
            Game.DBMutex.WaitOne();
            var query =
                from Аэропорт in db.Аэропорт
                select Аэропорт;
            List<Аэропорт> airports;
            try
            {
                airports = query.ToList();
            }
            catch (Exception ex)
            {
                airports = new List<Аэропорт>();
                string s = ex.InnerException.Message;
            }
            int firstAirport;
            int secondAirport;
            do
            {
                firstAirport = Rand.Get(0, airports.Count - 1);
                secondAirport = Rand.Get(0, airports.Count - 1);
            }
            while (firstAirport == secondAirport);
            Перелет fly = new Перелет()
            {
                Аэропорт1 = airports[firstAirport],
                Аэропорт = airports[secondAirport]
            };
            Game.DBMutex.ReleaseMutex();
            return fly;
        }

        /// <summary>
        /// Сгенерировать Пассажирский рейс
        /// </summary>
        /// <returns>Пассажирский рейс</returns>
        private static ПассажирскийРейс GetPassengerFlight()
        {
            Game.DBMutex.WaitOne();
            int maxCount = 350; //Масксимальное значение для максимального количества пассажиров на рейсе
            int minCount = 30; //Минимальное значение для максимального количества пассажиров на рейсе
            double minPricePassengerKm = 0.0003; // Минимальное значения для цены пассажира за км
            double maxPricePassengerKm = 0.0007; // Максимальное значения для цены пассажира за км
            var query =
                from РаспределениеПассажиров in db.РаспределениеПассажиров
                select РаспределениеПассажиров;
            List<РаспределениеПассажиров> distributions = query.ToList();
            int idDistrbution = Rand.Get(0, distributions.Count - 1);
            int maxCountPassenger = Rand.Get(minCount, maxCount);
            double pricePassengerKm = Rand.Get(minPricePassengerKm, maxPricePassengerKm);
            ПассажирскийРейс passengerFlight = new ПассажирскийРейс()
            {
                РаспределениеПассажиров = distributions[idDistrbution],
                ЦенаПассажирКм = pricePassengerKm,
                МаксимальноеКоличествоПассажиров = maxCountPassenger
            };
            Game.DBMutex.ReleaseMutex();
            return passengerFlight;
        }

        /// <summary>
        /// Сгенерировать грузовой рейс
        /// </summary>
        /// <returns>Грузовой рейс</returns>
        private static ГрузовойРейс GetCargoFlight()
        {
            Game.DBMutex.WaitOne();
            double maxCountCargoWeight = 300;
            double minCountCargoWeight = 10;
            double minPricePassengerKm = 0.0003; // Минимальное значения для цены тонны за км
            double maxPricePassengerKm = 0.0007; // Максимальное значения для цены тонны за км
            double cargoWeight = Rand.Get(minCountCargoWeight, maxCountCargoWeight);
            double price = Rand.Get(minPricePassengerKm, maxPricePassengerKm);
            ГрузовойРейс cargoFlight = new ГрузовойРейс()
            {
                ВесГруза = cargoWeight,
                ЦенаТоннаКм = price
            };
            Game.DBMutex.ReleaseMutex();
            return cargoFlight;
        }
         
        /// <summary>
        /// Сгенерировать разовый рес
        /// </summary>
        /// <returns>Разовый рейс</returns>
        private static РазовыйРейс GetOneTimeFlight()
        {
            Game.DBMutex.WaitOne();
            int minDate = 5; // Минимальное количество дней от текущего времени до Дедлайна
            int maxDate = 30; // Максимально количество дней от текущего времени до Дедлайна
            DateTime dateOfCompletion = Rand.GenDate(Time.CurrentTime + new TimeSpan(minDate, 0, 0, 0), Time.CurrentTime + new TimeSpan(maxDate, 0, 0, 0));
            РазовыйРейс oneTimeFlight = new РазовыйРейс()
            {
                ДатаВыполнения=dateOfCompletion.ToString()
            };
            Game.DBMutex.ReleaseMutex();
            return oneTimeFlight;
        }

        /// <summary>
        /// Сгенерировать регулярный рейс
        /// </summary>
        /// <returns>Регулярный рейс</returns>
        private static РегулярныйРейс GetRegularFlight()
        {
            Game.DBMutex.WaitOne();
            List<Регулярность> regulars = db.Регулярность.ToList();
            if (regulars.Count > 0)
            {
                РегулярныйРейс regularFlight = new РегулярныйРейс()
                {
                    Регулярность = regulars[Rand.Get(regulars.Count - 1)]
                };
                Game.DBMutex.ReleaseMutex();
                return regularFlight;
            }
            else
            {
                Game.DBMutex.ReleaseMutex();
                throw new Exception("Таблица регулярности пустая");
            }       
        }

        /// <summary>
        /// Сгенерировать Грузовой Рейс, который можно взять
        /// </summary>
        /// <returns>Грузовой рейс</returns>
        private static РейсИгрыИгрока GenerateCargoFlight()
        {
            Game.DBMutex.WaitOne();
            РейсИгрыИгрока cargoFlight = new РейсИгрыИгрока()
            {
                Игрок = Game.Player,
                Перелет=GetFly(),
                ГрузовойРейс=GetCargoFlight(),
                РазовыйРейс=GetOneTimeFlight() 
            };
            Game.DBMutex.ReleaseMutex();
            return cargoFlight;
        }

        /// <summary>
        /// Сгенерировать разовый пассажирский рейс
        /// </summary>
        /// <returns>Разовый пассажирский рейс</returns>
        private static РейсИгрыИгрока GeneratePassengerOneTimeFlight()
        {
            Game.DBMutex.WaitOne();
            РейсИгрыИгрока passengerOneTimeFlight = new РейсИгрыИгрока()
            {
                Игрок = Game.Player,
                Перелет = GetFly(),
                ПассажирскийРейс = GetPassengerFlight(),
                РазовыйРейс = GetOneTimeFlight()
            };
            Game.DBMutex.ReleaseMutex();
            return passengerOneTimeFlight;
        }

        /// <summary>
        /// Сгенерировать пассажирский регулярный рейс
        /// </summary>
        /// <returns>Пассажирский регулярный рейс</returns>
        private static РейсИгрыИгрока GeneratePassengerRegularFlight()
        {
            Game.DBMutex.WaitOne();
            РейсИгрыИгрока passengerRegularFlight = new РейсИгрыИгрока()
            {
                Игрок = Game.Player,
                Перелет = GetFly(),
                ПассажирскийРейс = GetPassengerFlight(),
                РегулярныйРейс1=GetRegularFlight()
            };
            Game.DBMutex.ReleaseMutex();
            return passengerRegularFlight;
        }
        /// <summary>
        /// Сгенерировать список новый грузовых рейсов
        /// </summary>
        /// <param name="count">Количество рейсов</param>
        /// <returns>Список новых рейсов</returns>
        private static List<РейсИгрыИгрока> GenerateCargoFlights(int count)
        {
            Game.DBMutex.WaitOne();
            List<РейсИгрыИгрока> newFlights = new List<РейсИгрыИгрока>();
            for (int i = 0; i < count; i++)
            {
                newFlights.Add(GenerateCargoFlight());
            }
            Game.DBMutex.ReleaseMutex();
            return newFlights;
        }

        /// <summary>
        /// Сгенерировать список разовых пассажирских рейсов
        /// </summary>
        /// <param name="count">Количество рейсов</param>
        /// <returns>Список регулярных пассажирских рейсов</returns>
        private static List<РейсИгрыИгрока> GenerateOneTimePassengerFlights(int count)
        {
            Game.DBMutex.WaitOne();
            List<РейсИгрыИгрока> newFlights = new List<РейсИгрыИгрока>();
            for (int i = 0; i < count; i++)
            {
                newFlights.Add(GeneratePassengerOneTimeFlight());
            }
            Game.DBMutex.ReleaseMutex();
            return newFlights;
        }

        /// <summary>
        /// Сгенерировать список регулярных пассажирских рейсов
        /// </summary>
        /// <param name="count">Количество рейсов</param>
        /// <returns>Список регулярных пассажирских рейсов</returns>
        private static List<РейсИгрыИгрока> GenerateRegularPassengerFlights(int count)
        {
            Game.DBMutex.WaitOne();
            List<РейсИгрыИгрока> newFlights = new List<РейсИгрыИгрока>();
            for (int i = 0; i < count; i++)
            {
                newFlights.Add(GeneratePassengerRegularFlight());
            }
            Game.DBMutex.ReleaseMutex();
            return newFlights;
        }

        /// <summary>
        /// Сгенерировать новые рейсы
        /// </summary>
        public static void GenerateFlights()
        {
            Game.DBMutex.WaitOne();
            List<РейсИгрыИгрока> flights = new List<РейсИгрыИгрока>();
            List<РейсИгрыИгрока> flightsToDel = GetFlightToDel(0.5);
            flights.AddRange(GenerateCargoFlights(3));
            flights.AddRange(GenerateOneTimePassengerFlights(2));
            flights.AddRange(GenerateRegularPassengerFlights(5));
            foreach (РейсИгрыИгрока flight in flightsToDel)
            {
                db.РейсИгрыИгрока.Remove(flight);
            }
            foreach (РейсИгрыИгрока flight in flights)
            {
                db.РейсИгрыИгрока.Add(flight);
            }
            db.SaveChanges();
            Game.DBMutex.ReleaseMutex();
        }

        /// <summary>
        /// Получение списка рейсов, которые необходимо удалить
        /// </summary>
        /// <param name="possibility">Вероятность удаления</param>
        /// <returns>Список на удаление</returns>
        private static List<РейсИгрыИгрока> GetFlightToDel(double possibility)
        {
            List<РейсИгрыИгрока> listOfFlights = GetPossibleFlights();
            List<РейсИгрыИгрока> listToDel = new List<РейсИгрыИгрока>();
            foreach (РейсИгрыИгрока flight in listOfFlights)
            {
                double random = Rand.Get(0, 1.0);
                if (random < possibility)
                {
                    listToDel.Add(flight);
                }
            }
            return listToDel;
        }

        /// <summary>
        /// Взять разовый рейс на выполнение
        /// </summary>
        /// <param name="flight">Рейс</param>
        public static void TakeFlight(РейсИгрыИгрока flight)
        {
            Game.DBMutex.WaitOne();
            ДоговорНаРейс contract = GenerateContract(flight);
            flight.ДоговорНаРейс = contract;
            db.SaveChanges();
            Calendar.AddEvent(new CheckOnlyFlightEvent(contract));
            Game.DBMutex.ReleaseMutex();
        }

        /// <summary>
        /// Взять регулярный рейс на выполнение
        /// </summary>
        /// <param name="flight">Рейс</param>
        /// <param name="dateEnd">Дата окончания договора</param>
        public static void TakeFlight(РейсИгрыИгрока flight, DateTime dateEnd)
        {
            Game.DBMutex.WaitOne();
            ДоговорНаРейс contract = GenerateContract(flight, dateEnd);
            РейсИгрыИгрока reverseFlight = GenerateReverseFlight(flight, dateEnd);
            flight.РегулярныйРейс1.РейсИгрыИгрока = reverseFlight;
            flight.ДоговорНаРейс = contract;
            db.РейсИгрыИгрока.Add(reverseFlight);
            db.SaveChanges();
            StartCheckMultiEvent(contract);
            Game.DBMutex.ReleaseMutex();
        }

        /// <summary>
        /// Запуск события проверки регулярного рейса
        /// </summary>
        /// <param name="contract">Договор на рейс</param>
        private static void StartCheckMultiEvent(ДоговорНаРейс contract)
        {
            РейсИгрыИгрока рейсИгрыИгрока = GetFlight(contract);
            DateTime startDate = DateTime.Parse(contract.ДатаНачала) + TimeSpan.FromDays(рейсИгрыИгрока.РегулярныйРейс1.Регулярность.Диапазон);
            Calendar.AddEvent(new CheckMultiFlightEvent(startDate, contract));
        }
        
        /// <summary>
        /// Получить стоимость неустойки за отмену рейса
        /// </summary>
        /// <param name="flight">Рейс для отмены</param>
        /// <returns>Неустойка</returns>
        public static double GetPriceCancelFlight(РейсИгрыИгрока flight)
        {
            double penalty;
            if (flight.РазовыйРейс != null)
            {
                penalty = flight.ДоговорНаРейс.Неустойка;
            }
            else
            {
                penalty = flight.ДоговорНаРейс.Неустойка * 2;
            }
            return penalty;
        }

        /// <summary>
        /// Отмена рейса
        /// </summary>
        /// <param name="flight">Рейс для отмены</param>
        public static void CancelFlight(РейсИгрыИгрока flight)
        {
            if (flight.РазовыйРейс!= null)
            {
                CancelOneTimeFlight(flight);
                //TODO: вызов потока для удаления разового рейса
            }
            else
            {
                CancelRegularFlight(flight);
                //TODO: вызов потока для удаления регулярного рейса
            }
        }

        /// <summary>
        /// Отмена разового рейса и получение информации о неустойке
        /// </summary>
        /// <param name="flight">Рейс для отмены</param>
        public static void CancelOneTimeFlight(РейсИгрыИгрока flight)
        {
            Game.DBMutex.WaitOne();
            Player.ChangeBalance(-GetPriceCancelFlight(flight));
            RemoveOneTimeFlight(flight); // Удаление рейса
            Game.DBMutex.ReleaseMutex();
        }

        /// <summary>
        /// Удаление разового рейса
        /// </summary>
        /// <param name="flight">Рейс для отмены</param>
        public static void RemoveOneTimeFlight(РейсИгрыИгрока flight)
        {
            Game.DBMutex.WaitOne();
            TimeTable.RemoveTableRecords(flight);
            db.ДоговорНаРейс.Remove(flight.ДоговорНаРейс);
            if (flight.РазовыйРейс != null)
            {
                db.РазовыйРейс.Remove(flight.РазовыйРейс);
            }
            if (flight.ПассажирскийРейс != null)
            {
                db.ПассажирскийРейс.Remove(flight.ПассажирскийРейс);
            }
            if (flight.ГрузовойРейс != null)
            {
                db.ГрузовойРейс.Remove(flight.ГрузовойРейс);
            }
            db.РейсИгрыИгрока.Remove(flight);
            db.SaveChanges();

            Game.DBMutex.ReleaseMutex();
        }

        /// <summary>
        /// Отмена регулярного рейса и получение информации о неустойке
        /// </summary>
        /// <param name="flight">Рейс для отмены</param>
        public static void CancelRegularFlight(РейсИгрыИгрока flight)
        {
            Game.DBMutex.WaitOne();
            Player.ChangeBalance(-GetPriceCancelFlight(flight));
            RemoveRegularFlight(flight); // Удаление рейса
            Game.DBMutex.ReleaseMutex();
        }

        /// <summary>
        /// Удаление регулярного рейса
        /// </summary>
        /// <param name="flight">Рейс для отмены</param>
        public static void RemoveRegularFlight(РейсИгрыИгрока flight)
        {
            Game.DBMutex.WaitOne();
            TimeTable.RemoveTableRecords(flight);
            db.ДоговорНаРейс.Remove(flight.ДоговорНаРейс);
            db.ПассажирскийРейс.Remove(flight.ПассажирскийРейс);
            db.ДоговорНаРейс.Remove(flight.РегулярныйРейс1.РейсИгрыИгрока.ДоговорНаРейс);
            db.РегулярныйРейс.Remove(flight.РегулярныйРейс1.РейсИгрыИгрока.РегулярныйРейс1);
            db.РейсИгрыИгрока.Remove(flight.РегулярныйРейс1.РейсИгрыИгрока);
            db.РегулярныйРейс.Remove(flight.РегулярныйРейс1);
            db.SaveChanges();
            db.РейсИгрыИгрока.Remove(flight);
            db.SaveChanges();
            Game.DBMutex.ReleaseMutex();
        }

        /// <summary>
        /// Генерация обратного рейса для регулярных рейсов
        /// </summary>
        /// <param name="flight">Прямой рейс</param>
        /// <returns></returns>
        private static РейсИгрыИгрока GenerateReverseFlight(РейсИгрыИгрока flight, DateTime dateEnd)
        {
            Game.DBMutex.WaitOne();
            РегулярныйРейс reverseRegularFlight = new РегулярныйРейс()
            {
                Регулярность = flight.РегулярныйРейс1.Регулярность,
                РейсИгрыИгрока=flight
            };
            Перелет reverseFly = new Перелет()
            {
                Аэропорт1 = flight.Перелет.Аэропорт,
                Аэропорт = flight.Перелет.Аэропорт1
            };
            РейсИгрыИгрока reverseFlight = new РейсИгрыИгрока()
            {
                Игрок = Game.Player,
                Перелет = reverseFly,
                ПассажирскийРейс = flight.ПассажирскийРейс,
                РегулярныйРейс1 = reverseRegularFlight
            };
            reverseFlight.ДоговорНаРейс = GenerateContract(reverseFlight,dateEnd);
            Game.DBMutex.ReleaseMutex();
            return reverseFlight;
        }

        /// <summary>
        /// Сгенерировать договор на рейс для разового рейса
        /// </summary>
        /// <param name="flight">Рейс</param>
        /// <returns></returns>
        private static ДоговорНаРейс GenerateContract(РейсИгрыИгрока flight)
        {
            Game.DBMutex.WaitOne();
            double penalty;
            if (flight.ГрузовойРейс != null)
                penalty = GetCargoPenalty(flight); 
            else
                penalty = GetPassengerPenalty(flight); 
            ДоговорНаРейс contract = new ДоговорНаРейс()
            {
                ДатаНачала = Time.CurrentTime.ToString(),
                ДатаОкончания = flight.РазовыйРейс.ДатаВыполнения,
                id_СтатусДоговора = 1,
                Неустойка = penalty,
                Количество_выполненных = 0
            };
            db.ДоговорНаРейс.Add(contract);
            db.SaveChanges();
            Game.DBMutex.ReleaseMutex();
            return contract;
        }

        /// <summary>
        /// Сгенерировать договор на рейс для регулярного рейса
        /// </summary>
        /// <param name="flight">Рейс</param>
        /// <param name="dateEnd">Дата окончания</param>
        /// <returns></returns>
        private static ДоговорНаРейс GenerateContract(РейсИгрыИгрока flight, DateTime dateEnd)
        {
            Game.DBMutex.WaitOne();
            double penalty = GetPassengerPenalty(flight);
            ДоговорНаРейс contract = new ДоговорНаРейс()
            {
                ДатаНачала = Time.CurrentTime.ToString(),
                ДатаОкончания = dateEnd.ToString(),
                id_СтатусДоговора = 1,
                Неустойка = penalty,
                Количество_выполненных = 0
            };
            Game.DBMutex.ReleaseMutex();
            return contract;
        }

        /// <summary>
        /// Расстояние между двумя аэропортами
        /// </summary>
        /// <param name="fly">Полет</param>
        /// <returns>Расстояние</returns>
        public static double GetDistance(Перелет fly)
        {
            GeoCoordinate firstAirport = new GeoCoordinate(fly.Аэропорт1.Местоположение.Широта, fly.Аэропорт1.Местоположение.Долгота);
            GeoCoordinate secondAirporty = new GeoCoordinate(fly.Аэропорт.Местоположение.Широта, fly.Аэропорт.Местоположение.Долгота);
            return firstAirport.GetDistanceTo(secondAirporty)/1000;
        }

        /// <summary>
        /// Получить прибыль от грузового рейса
        /// </summary>
        /// <param name="cargoFlight">Грузовой рейс</param>
        /// <returns>Прибыль</returns>
        public static double GetCargoPayment(РейсИгрыИгрока cargoFlight)
        {
            Game.DBMutex.WaitOne();
            double distance = GetDistance(cargoFlight.Перелет);
            double payment= distance* cargoFlight.ГрузовойРейс.ВесГруза * cargoFlight.ГрузовойРейс.ЦенаТоннаКм;
            Game.DBMutex.ReleaseMutex();
            return payment;
        }

        public static double GetCargoPayment(Расписание record)
        {
            Game.DBMutex.WaitOne();
            РейсИгрыИгрока cargoFlight = TimeTable.GetFlight(record.ДоговорНаРейс);
            double distance = GetDistance(cargoFlight.Перелет);
            double payment = distance * cargoFlight.ГрузовойРейс.ВесГруза * cargoFlight.ГрузовойРейс.ЦенаТоннаКм;
            Game.DBMutex.ReleaseMutex();
            return payment;
        }
        /// <summary>
        /// Получить неустойку за невыполнение грузового рейса
        /// </summary>
        /// <param name="cargoFlight">Грузовой рейс</param>
        /// <returns>Неустойка</returns>
        public static double GetCargoPenalty(РейсИгрыИгрока cargoFlight)
        {
            return GetCargoPayment(cargoFlight) / 2;
        }

        /// <summary>
        /// Доход от рейса с распределением пассажиров
        /// </summary>
        /// <param name="пиковоеВремя">Пиковое время рейса</param>
        /// <param name="времяВылета">Фактическое время вылета</param>
        /// <param name="passengerFlight">Рейс</param>
        /// <returns>Прибыль</returns>
        private static double PassengersDistibution(TimeSpan пиковоеВремя, TimeSpan времяВылета, РейсИгрыИгрока passengerFlight)
        {
            Game.DBMutex.WaitOne();
            long максКолвоПассажиров = passengerFlight.ПассажирскийРейс.МаксимальноеКоличествоПассажиров;
            double ценаЗаПассажираЗаКм= (double)passengerFlight.ПассажирскийРейс.ЦенаПассажирКм;
            double дисперсия = DateTime.Parse(passengerFlight.ПассажирскийРейс.РаспределениеПассажиров.Дисперсия).TimeOfDay.TotalMinutes;
            double коэффПиковыхЧасов = 1 - Math.Abs((пиковоеВремя - времяВылета).TotalMinutes) / (12 * 60);
            double матожиданиеПассажиров = максКолвоПассажиров * 0.5 + максКолвоПассажиров * 0.4 * коэффПиковыхЧасов;
            IGenerator нормРаспределение = new GeneratorGauss(new ParamCollection<double>
            {
                { "nu", матожиданиеПассажиров },
                { "sigma", дисперсия }
            });
            long колвоПассажиров = Math.Abs((long)Math.Floor(нормРаспределение.Next()));
            if (колвоПассажиров > максКолвоПассажиров)
            {
                колвоПассажиров = максКолвоПассажиров;
            }
            double доход = колвоПассажиров * ценаЗаПассажираЗаКм * 3000;
            Game.DBMutex.ReleaseMutex();
            return доход;
        }

        /// <summary>
        /// Получить прибыль за пассажирский рейс не из раписания
        /// </summary>
        /// <param name="passengerFlight">Пассажирский рейс</param>
        /// <returns>Прибыль за один рейс</returns>
        public static double GetPassengerPayment(РейсИгрыИгрока passengerFlight)
        {
            Game.DBMutex.WaitOne();
            TimeSpan пиковоеВремя = DateTime.Parse(passengerFlight.ПассажирскийРейс.РаспределениеПассажиров.ПиковоеВремяДня).TimeOfDay;
            TimeSpan времяВылета = пиковоеВремя;
            Game.DBMutex.ReleaseMutex();
            return PassengersDistibution(пиковоеВремя, времяВылета, passengerFlight);
        }

        /// <summary>
        /// Получить прибыль за пассажирский рейс из раписания
        /// </summary>
        /// <param name="passengerFlight">Пассажирский рейс</param>
        /// <returns>Прибыль за один рейс</returns>
        public static double GetPassengerPayment(Расписание record)
        {
            Game.DBMutex.WaitOne();
            РейсИгрыИгрока passengerFlight = TimeTable.GetFlight(record.ДоговорНаРейс);
            TimeSpan пиковоеВремя = DateTime.Parse(passengerFlight.ПассажирскийРейс.РаспределениеПассажиров.ПиковоеВремяДня).TimeOfDay;
            TimeSpan времяВылета = DateTime.Parse(record.ДатаВремяВылета).TimeOfDay;
            Game.DBMutex.ReleaseMutex();
            return PassengersDistibution(пиковоеВремя, времяВылета, passengerFlight);
        }

        /// <summary>
        /// Получить неустойку за пассажирский рейс
        /// </summary>
        /// <param name="passengerFlight"></param>
        /// <returns>Неустойка</returns>
        public static double GetPassengerPenalty(РейсИгрыИгрока passengerFlight)
        {
            Game.DBMutex.WaitOne();
            var passengerPayment = GetPassengerPayment(passengerFlight);
            if (passengerFlight.РегулярныйРейс1 != null)
            {
                Game.DBMutex.ReleaseMutex();
                return passengerPayment * passengerFlight.РегулярныйРейс1.Регулярность.КоличествоПовторов / 2;
            }
            Game.DBMutex.ReleaseMutex();
            return passengerPayment / 2;
        }

        /// <summary>
        /// Добавить пустой перелет между двумя аэропортами
        /// </summary>
        /// <param name="firstAirport">Аэропорт отправления</param>
        /// <param name="secondAirport">Аэропорт прибытия</param>
        /// <param name="aircraft">Самолет</param>
        /// <returns></returns>
        public static void AddEmptyFly(Аэропорт firstAirport, Аэропорт secondAirport, СамолетИгрыИгрока aircraft, DateTime time)
        {
            Game.DBMutex.WaitOne();
            ДоговорНаРейс contract = new ДоговорНаРейс()
            {
                ДатаНачала = Time.CurrentTime.ToString(),
                ДатаОкончания=Time.CurrentTime.ToString(),
                Неустойка=0,
                id_СтатусДоговора=1
            };
            Перелет fly = new Перелет()
            {
                Аэропорт1 = firstAirport,
                Аэропорт = secondAirport
            };
            РейсИгрыИгрока flight = new РейсИгрыИгрока()
            {
                Игрок=Game.Player,
                Перелет=fly,
                ДоговорНаРейс=contract
            };
            if (!TimeTable.IsRecord(contract, aircraft, time))
            {
                db.Перелет.Add(fly);
                db.ДоговорНаРейс.Add(contract);
                db.РейсИгрыИгрока.Add(flight);
                Расписание record = new Расписание()
                {
                    ДатаВремяВылета = time.ToString(),
                    ДоговорНаРейс = contract,
                    СамолетИгрыИгрока = aircraft
                };
                db.Расписание.Add(record);
                db.SaveChanges();
                Calendar.AddEvent(new UpAircraftEvent(record));
            }
            Game.DBMutex.ReleaseMutex();   
        }

        /// <summary>
        /// Расчитать прибыль за выполнение рейса
        /// </summary>
        /// <param name="record">Запись в расписании</param>
        /// <returns>Прибыль</returns>
        public static double GetFlightsIncome(Расписание record)
        {
            Game.DBMutex.WaitOne();
            РейсИгрыИгрока flight = TimeTable.GetFlight(record.ДоговорНаРейс);
            double income;
            if (flight.ГрузовойРейс != null)
            {
                income = GetCargoPayment(flight);
            }
            else
            {
                if (flight.ПассажирскийРейс != null)
                {
                    income = GetPassengerPayment(flight);
                    if (flight.РегулярныйРейс1!= null &&
                        record.ДоговорНаРейс.Количество_выполненных > flight.РегулярныйРейс1.Регулярность.КоличествоПовторов)
                        income *= 0.8;
                }
                else
                {
                    income = 0;
                }
            }               
            Game.DBMutex.ReleaseMutex();
            return income;
        }
        
        /// <summary>
        /// Расчитать расход за выполнение рейса
        /// </summary>
        /// <param name="record">Строка расписания</param>
        /// <returns>Расход</returns>
        public static double GetFlightsCost(Расписание record)
        {
            Game.DBMutex.WaitOne();
            РейсИгрыИгрока flight = TimeTable.GetFlight(record.ДоговорНаРейс);
            double cost = GetDistance(flight.Перелет) * record.СамолетИгрыИгрока.МодельСамолета.РасходТоплива / 100000;
            Game.DBMutex.ReleaseMutex();
            return cost;
        }

        /// <summary>
        /// Получение времени дня с пиковой точкой
        /// </summary>
        /// <param name="flight">Рейс</param>
        /// <returns>Время дня</returns>
        public static dayTime GetPeakTime(ПассажирскийРейс flight)
        {
            TimeSpan time = DateTime.Parse(flight.РаспределениеПассажиров.ПиковоеВремяДня).TimeOfDay;
            int hours = time.Hours;
            if (hours>=0 && hours<6)
            {
                return dayTime.Ночь;
            }
            if (hours>=6 && hours<=12)
            {
                return dayTime.Утро;
            }
            if (hours > 12 && hours < 18)
            {
                return dayTime.День;
            }
            if (hours >= 18 && hours < 24)
            {
                return dayTime.Вечер;
            }
            throw new Exception("Такого времени суток нет");
        }

        /// <summary>
        /// Расчитать время полета
        /// </summary>
        /// <param name="record">Запись расписания</param>
        /// <returns>Время полета</returns>
        public static TimeSpan GetFlightTime(Расписание record)
        {
            РейсИгрыИгрока flight = GetFlight(record.ДоговорНаРейс);
            return Aircraft.GetFlyingTime(record.СамолетИгрыИгрока, flight.Перелет);        
        }
    }
}
