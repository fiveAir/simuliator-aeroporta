﻿using ProjectAirline.Core;
using ProjectAirline.Events;
using ProjectAirline.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAirline.Tools.DataBaseTools
{
    public abstract class Shop : Connection
    {
        /// <summary>
        /// Возвращает список самолётов, доступных в магазине
        /// </summary>
        /// <param name="playerId">Игрок, которому доступны самолёты</param>
        /// <returns>Список строк магазина</returns>
        public static List<Магазин> GetShopLines()
        {
            Game.DBMutex.WaitOne();
            var query =
                from Магазин in db.Магазин
                where Game.Player.id == Магазин.СамолетИгрыИгрока.id_Игрок
                select Магазин;
            Game.DBMutex.ReleaseMutex();
            return query.ToList();
        }

        /// <summary>
        /// Сформировать самолет игрока
        /// </summary>
        /// <param name="gamesAircraft">Самолет игры</param>
        /// <param name="contract">Договор на самолет</param>
        /// <returns></returns>
        public static СамолетИгрока GetPlayersAircraft(СамолетИгрыИгрока gamesAircraft, ДоговорНаСамолет contract)
        {
            Game.DBMutex.WaitOne();
            СамолетИгрока PlayersAircraft = new СамолетИгрока()
            {
                id_Игрок = Game.Player.id,
                СамолетИгрыИгрока = gamesAircraft,
                ДоговорНаСамолет = contract,
                id_ТекущийАэропорт=Game.Player.id_СтартовыйАэропорт
            };
            Game.DBMutex.ReleaseMutex();
            return PlayersAircraft;
        }

        /// <summary>
        /// Ежемесячная сумма выплаты по лизингу
        /// </summary>
        /// <param name="aircraftInShop">Магазин</param>
        /// <param name="duration">Срок лизинга</param>
        /// <returns></returns>
        public static double MonthPaymentLease(Лизинг lease, int duration)
        {
            try
            {
                double процентнаяСтавкаМес = lease.ПроцентнаяСтавкаГод / 12;
                double процентнаяПлата = 1 + процентнаяСтавкаМес * duration;
                double итоговаяЦенаСамолёта = процентнаяПлата * lease.ЦенаСамолета;
                double количествоВзносов = duration + 1; // Первоначальный + в конце каждого месяца
                return Math.Round(итоговаяЦенаСамолёта / количествоВзносов, 2); // ежемесячный платеж 
            }
            catch
            {
                throw new Exception("Извините, этот самолет уже купили");
            }
        }

        /// <summary>
        /// Покупка самолета в магазине
        /// </summary>
        /// <param name="aircraftInShop">Самолет для покупки</param>
        public static void BuyAircraft(Магазин aircraftInShop)
        {
            Game.DBMutex.WaitOne();
            double aircraftsPrice;
            try
            {
                aircraftsPrice = aircraftInShop.Покупка.СтоимостьСамолета;
            }
            catch
            {
                Game.DBMutex.ReleaseMutex();
                throw new Exception("Извините, этот самолет уже купили");
            }
            if (Game.Player.Баланс < aircraftsPrice)
            {
                Game.DBMutex.ReleaseMutex();
                throw new Exception("У вас недостаточно денежных" +
                                    "   средств для покупки!");
            }
            ДоговорНаСамолет contract = new ДоговорНаСамолет()
            {
                Авиакомпания = aircraftInShop.Авиакомпания,
                Покупка = aircraftInShop.Покупка,
                id_СтатусДоговора = 2
            };
            СамолетИгрока PlayersAircraft = GetPlayersAircraft(aircraftInShop.СамолетИгрыИгрока, contract);
            db.ДоговорНаСамолет.Add(contract);
            db.СамолетИгрока.Add(PlayersAircraft);
            Player.ChangeBalance(-aircraftInShop.Покупка.СтоимостьСамолета);
            db.Магазин.Remove(aircraftInShop);
            db.SaveChanges();
            Game.DBMutex.ReleaseMutex();
        }

        /// <summary>
        /// Аренда самолета из магазина
        /// </summary>
        /// <param name="aircraftInShop">Самолет для аренды</param>
        public static void RentAircraft(Магазин aircraftInShop)
        {
            Game.DBMutex.WaitOne();
            double aircraftsPrice;
            try
            {
                aircraftsPrice = aircraftInShop.Аренда.СтоимостьАрендыМес;
            }
            catch
            {
                Game.DBMutex.ReleaseMutex();
                throw new Exception("Извините, этот самолет уже купили");
            }
            if (Game.Player.Баланс < aircraftsPrice)
            {
                Game.DBMutex.ReleaseMutex();
                throw new Exception("У вас недостаточно денежных" +
                                    "   средств для аренды!");
            }
            ДоговорНаСамолет contract = new ДоговорНаСамолет()
            {
                Авиакомпания = aircraftInShop.Авиакомпания,
                Аренда = aircraftInShop.Аренда,
                id_СтатусДоговора = 1
            };
            СамолетИгрока PlayersAircraft = GetPlayersAircraft(aircraftInShop.СамолетИгрыИгрока, contract);
            db.ДоговорНаСамолет.Add(contract);
            db.СамолетИгрока.Add(PlayersAircraft);
            db.Магазин.Remove(aircraftInShop);
            db.SaveChanges();
            Calendar.AddEvent(new RentEvent(Time.CurrentTime, contract.id));
            Game.DBMutex.ReleaseMutex();

        }

        /// <summary>
        /// Лизинг самолета из магазина
        /// </summary>
        /// <param name="aircraftInShop">Самолет для лизинга</param>
        /// <param name="duration">Длительность лизинга</param>
        public static void LeaseAircraft(Магазин aircraftInShop, int duration)
        {
            Game.DBMutex.WaitOne();
            double monthlyPayment;
            try
            {
                monthlyPayment = MonthPaymentLease(aircraftInShop.Лизинг, duration);
            }
            catch
            {
                Game.DBMutex.ReleaseMutex();
                throw new Exception("Извините, этот самолет уже купили!");
            }
            if (Game.Player.Баланс < monthlyPayment)
            {
                Game.DBMutex.ReleaseMutex();
                throw new Exception("У вас недостаточно денежных" +
                                    "    средств для лизинга!");
            }
            try
            {
                ДоговорНаСамолет contract = new ДоговорНаСамолет()
                {
                    Авиакомпания = aircraftInShop.Авиакомпания,
                    Лизинг = aircraftInShop.Лизинг,
                    СрокЛизингаМес = duration,
                    id_СтатусДоговора = 1
                };    
                СамолетИгрока PlayersAircraft = GetPlayersAircraft(aircraftInShop.СамолетИгрыИгрока, contract);
                db.ДоговорНаСамолет.Add(contract);
                db.СамолетИгрока.Add(PlayersAircraft);
                db.Магазин.Remove(aircraftInShop);
                db.SaveChanges();
                Calendar.AddEvent(new LeasingEvent(Time.CurrentTime, duration, duration, contract.id));
                Game.DBMutex.ReleaseMutex();
            }
            catch
            {
                Game.DBMutex.ReleaseMutex();
                throw new Exception("Извините, этот самолет уже купили");
            }
        }

        /// <summary>
        /// Получить список записей магазина для удаления
        /// </summary>
        /// <param name="airline"></param>
        /// <returns></returns>
        private static Магазин GetShopsRecordToDel(Авиакомпания airline)
        {
            Game.DBMutex.WaitOne();
            var query =
                from Магазин in db.Магазин
                where Магазин.Авиакомпания.id == airline.id
                orderby Магазин.id_СамолетаИгрыИгрока ascending
                select Магазин;
            List<Магазин> shopList = query.ToList();
            //Если список самолетов авиакомпании не пустой, то удаляем самый старый в магазине
            if (shopList.Count > 0)
            {
                Game.DBMutex.ReleaseMutex();
                return shopList[0];
            }
            else
            {
                Game.DBMutex.ReleaseMutex();
                return null;
            }
        }

        /// <summary>
        /// Получить список записей магазина для добавления
        /// </summary>
        /// <param name="airline">Авиакомпания</param>
        /// <param name="currentTime"></param>
        /// <returns></returns>
        private static Магазин GetShopsRecordToAdd(Авиакомпания airline)
        {
            Game.DBMutex.WaitOne();
            СамолетИгрыИгрока aircraft = Aircraft.GenerateAircraft();
            db.СамолетИгрыИгрока.Add(aircraft);
            Магазин shopRecord= GetRecordShop(airline, aircraft);
            Game.DBMutex.ReleaseMutex();
            return shopRecord;
            
        }

        /// <summary>
        /// Сохранение изменений в БД после обновления магазина
        /// </summary>
        /// <param name="toAdd">Список записей магазина для добавления</param>
        /// <param name="toDel">Список записей магазина для удаления</param>
        private static void SaveChangesShop(List<Магазин> toAdd, List<Магазин> toDel)
        {
            Game.DBMutex.WaitOne();
            foreach (Магазин shopRecord in toDel)
            {
                СамолетИгрыИгрока temp = shopRecord.СамолетИгрыИгрока;
                if (shopRecord.Покупка != null)
                    db.Покупка.Remove(shopRecord.Покупка);
                if (shopRecord.Аренда != null)
                    db.Аренда.Remove(shopRecord.Аренда);
                if (shopRecord.Лизинг != null)
                    db.Лизинг.Remove(shopRecord.Лизинг);
                db.Магазин.Remove(shopRecord);
                db.СамолетИгрыИгрока.Remove(temp);
            }
            foreach (Магазин shopRecord in toAdd)
            {
                db.Магазин.Add(shopRecord);
            }
            db.SaveChanges();
            Game.DBMutex.ReleaseMutex();
        }

        /// <summary>
        /// Обновление самолетов всех авиакомпании
        /// </summary>
        /// <param name="currentTime"></param>
        public static void RefreshAllAircrafts()
        {
            Game.DBMutex.WaitOne();
            List<Магазин> toDel = new List<Магазин>();
            List<Магазин> toAdd = new List<Магазин>();
            foreach (Авиакомпания airline in db.Авиакомпания)
            {
                Магазин recordToDel = GetShopsRecordToDel(airline);
                if (recordToDel != null)
                    toDel.Add(recordToDel);
                Магазин recordToAdd = GetShopsRecordToAdd(airline);
                toAdd.Add(recordToAdd);
                //С вероятностью добавляем дополнительный самолет в магазин
                if (Rand.GetСhance(0.05))
                {
                    Магазин recordToAddExtra = GetShopsRecordToAdd(airline);
                    toAdd.Add(recordToAddExtra);
                }
            }
            SaveChangesShop(toAdd, toDel);
            Game.DBMutex.ReleaseMutex();
        }

        /// <summary>
        /// Получить сгенерированную запись магазина
        /// </summary>
        /// <param name="airline">Авиакомпания</param>
        /// <param name="aircraft">Самолет для продажи</param>
        /// <returns></returns>
        private static Магазин GetRecordShop(Авиакомпания airline, СамолетИгрыИгрока aircraft)
        {
            Game.DBMutex.WaitOne();
            Магазин shopLine = GetNewShop(airline, aircraft);
            Game.DBMutex.ReleaseMutex();
            return shopLine;
        }

        /// <summary>
        /// Выставляет параметры аредны, продажи и лизинга для строки магазина
        /// </summary>
        /// <param name="shopLine">Строка магазина</param>
        private static Магазин GetNewShop(Авиакомпания airline, СамолетИгрыИгрока aircraft)
        {
            Game.DBMutex.WaitOne();
            double standartCost = aircraft.МодельСамолета.СтандартнаяСтоимость;
            Магазин shop;
            do
            {
                shop = new Магазин()
                {
                    СамолетИгрыИгрока = aircraft,
                    Авиакомпания = airline,
                    Аренда = Rand.GetСhance(0.33) ? GetNewRent(standartCost) : null,
                    Покупка = Rand.GetСhance(0.33) ? GetNewBye(standartCost) : null,
                    Лизинг = Rand.GetСhance(0.33) ? GetNewLease(standartCost) : null,
                };
            }
            while (shop.Аренда == null && shop.Покупка == null && shop.Лизинг == null);
            Game.DBMutex.ReleaseMutex();
            return shop;
        }

        private static Аренда GetNewRent(double cost)
        {
            return new Аренда()
            {
                СтоимостьАрендыМес = cost / 20
            };
        }

        private static Покупка GetNewBye(double cost)
        {
            return new Покупка()
            {
                СтоимостьСамолета = cost
            };
        }

        private static Лизинг GetNewLease(double cost)
        {
            return new Лизинг()
            {
                ЦенаСамолета = cost,
                ПроцентнаяСтавкаГод = Rand.Get(0.4, 0.6)
            };
        }

    }
}
