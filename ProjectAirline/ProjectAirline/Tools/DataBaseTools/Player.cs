﻿using ProjectAirline.ORM;
using System.Device.Location;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using ProjectAirline.Events;

namespace ProjectAirline.Tools.DataBaseTools
{
    public abstract class Player : Connection
    {
        public static List<Игрок> GetAllPlayers()
        {
            var query =
               from игрок in db.Игрок
               select игрок;
            return query.ToList();
        }

        public static Игрок CreateNewPlayer(Игрок player)
        {
            db.Игрок.Add(player);
            db.SaveChanges();
            return player;
        }

        public static bool IsCorrectNameAirline(string name)
        {
            var queryPlayers =
               from игрок in db.Игрок
               where игрок.НазваниеАвиакомпании == name
               select игрок;
            var queryAirlines =
               from авиакомпания in db.Авиакомпания
               where авиакомпания.Наименование == name
               select авиакомпания;
            return queryPlayers.ToList().Count + queryAirlines.ToList().Count == 0;
        }

        /// <summary>
        /// Изменить баланс игрока
        /// </summary>
        /// <param name="change">Размер изменения</param>
        public static void ChangeBalance(double change)
        {
            Game.DBMutex.WaitOne();
            Game.Player.Баланс += change;
            ILog.Write(Math.Round(change, 2).ToString() + " Текущий баланс: " + Math.Round(Game.Player.Баланс, 2).ToString());
            db.SaveChanges();

            if (Game.Player.Баланс < 0)
            {
                ILog.Write("Конец игры!");
            }
            Game.DBMutex.ReleaseMutex();
        }
    }
}
