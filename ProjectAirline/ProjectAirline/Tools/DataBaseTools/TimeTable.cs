﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectAirline.ORM;
using ProjectAirline.Core;
using ProjectAirline.Events;

namespace ProjectAirline.Tools.DataBaseTools
{
    public abstract class TimeTable : Connection
    {
        /// <summary>
        /// Получить раписание для текущего игрока
        /// </summary>
        /// <returns>Раписание</returns>
        public static List<Расписание> GetTimeTableRecords()
        {
            Game.DBMutex.WaitOne();
            var query =
                from Расписание in db.Расписание
                where Расписание.СамолетИгрыИгрока.id_Игрок == Game.Player.id
                orderby Расписание.ДатаВремяВылета
                select Расписание;
            Game.DBMutex.ReleaseMutex();
            return query.ToList();
        }

        /// <summary>
        /// Получить рейс по договору
        /// </summary>
        /// <param name="contract">Договор на рейс</param>
        /// <returns>Рейс</returns>
        public static РейсИгрыИгрока GetFlight(ДоговорНаРейс contract)
        {
            Game.DBMutex.WaitOne();
            var query =
                from Рейс in db.РейсИгрыИгрока
                where Рейс.ДоговорНаРейс.id == contract.id
                select Рейс;
            Game.DBMutex.ReleaseMutex();
            return query.ToList()[0];
        }

        /// <summary>
        /// Добавить запись в расписание
        /// </summary>
        /// <param name="contract">Договор на рейс</param>
        /// <param name="aircraft">Самолет</param>
        /// <param name="datetime">Время рейса</param>
        /// <returns>Расписание</returns>
        public static void AddTimeTableRecord(ДоговорНаРейс contract, СамолетИгрыИгрока aircraft, DateTime datetime)
        {
            Game.DBMutex.WaitOne();
            if (!IsRecord(contract, aircraft, datetime))
            {
                Расписание record = new Расписание()
                {
                    ДоговорНаРейс = contract,
                    СамолетИгрыИгрока = aircraft,
                    ДатаВремяВылета = datetime.ToString()
                };
                db.Расписание.Add(record);
                db.SaveChanges();
                Calendar.AddEvent(new UpAircraftEvent(record));
            }
            Game.DBMutex.ReleaseMutex();
        }

        /// <summary>
        /// Удалить строку расписания
        /// </summary>
        /// <param name="record">Строка для удаления</param>
        public static void RemoveTimeTableRecord(Расписание record)
        {
            Game.DBMutex.WaitOne();
            db.Расписание.Remove(record);
            db.SaveChanges();
            Game.DBMutex.ReleaseMutex();
        }

        /// <summary>
        /// Удаление всех строк расписания с заданным рейсом
        /// </summary>
        /// <param name="flight"></param>
        public static void RemoveTableRecords(РейсИгрыИгрока flight)
        {
            Game.DBMutex.WaitOne();
            var query =
                from Расписание in db.Расписание
                where Расписание.ДоговорНаРейс.id == flight.ДоговорНаРейс.id
                select Расписание;
            List<Расписание> toDel = query.ToList();
            foreach (var Расписание in toDel)
            {
                db.Расписание.Remove(Расписание);
            }
            db.SaveChanges();
            Game.DBMutex.ReleaseMutex();
        }

        public static void RemoveTimeTableRecord(DateTime датаВремяВылета, ДоговорНаРейс договорНаРейс, СамолетИгрыИгрока самолетИгрыИгрока)
        {
            Game.DBMutex.WaitOne();
            foreach (Расписание record in db.Расписание)
            {
                if (record.id_ДоговорНаРейс == договорНаРейс.id &&
                    record.id_Самолет == самолетИгрыИгрока.id &&
                    record.ДатаВремяВылета == датаВремяВылета.ToString())
                {
                    db.Расписание.Remove(record);
                }
            }
            db.SaveChanges();
            Game.DBMutex.ReleaseMutex();
        }

        /// <summary>
        /// Получение строки расписания по её параметрам
        /// </summary>
        /// <param name="contract">Договор на рейс</param>
        /// <param name="aircraft">Самолет</param>
        /// <param name="time">Время вылета</param>
        /// <returns>Строка расписания</returns>
        public static Расписание GetRecord(ДоговорНаРейс contract, СамолетИгрыИгрока aircraft, DateTime time)
        {
            foreach (Расписание record in db.Расписание)
            {
                if (record.id_ДоговорНаРейс == contract.id &&
                    record.id_Самолет == aircraft.id &&
                    record.ДатаВремяВылета == time.ToString())
                {
                    return record;
                }
            }
            throw new Exception("Строка раписания отсутствует");
        }

        /// <summary>
        /// Получение строки расписания по её параметрам
        /// </summary>
        /// <param name="contract">Договор на рейс</param>
        /// <param name="aircraft">Самолет</param>
        /// <param name="time">Время вылета</param>
        /// <returns>Строка расписания</returns>
        public static bool IsRecord(ДоговорНаРейс contract, СамолетИгрыИгрока aircraft, DateTime time)
        {
            var query =
                from Расписание in db.Расписание
                where Расписание.id_ДоговорНаРейс == contract.id &&
                Расписание.id_Самолет == aircraft.id &&
                Расписание.ДатаВремяВылета == time.ToString()
                select Расписание;
            if (query.ToList().Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Расчитать время выполнения рейса в расписании
        /// </summary>
        /// <param name="record">Строка раписания</param>
        /// <returns>Время полета</returns>
        public static TimeSpan GetFlyingTime(Расписание record)
        {
            return Aircraft.GetFlyingTime(record.СамолетИгрыИгрока, GetFlight(record.ДоговорНаРейс).Перелет);
        }

        /// <summary>
        /// Получить информацию о перелете из записи в расписании
        /// </summary>
        /// <param name="record">Строка расписания</param>
        /// <returns>Информация о перелете</returns>
        public static Перелет GetFlyFromTimeTable(Расписание record)
        {
            РейсИгрыИгрока flight = GetFlight(record.ДоговорНаРейс);
            return flight.Перелет;
        }
    }
}
