﻿using ProjectAirline.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAirline.Tools.DataBaseTools
{
    public abstract class Contract :Connection
    {
        /// <summary>
        /// Проверка был ли выполнен договор на рейс
        /// </summary>
        /// <param name="contract">Договор на рейс</param>
        /// <returns>Результат выполнения</returns>
        public static bool IsContractMade(ДоговорНаРейс contract)
        {
            РейсИгрыИгрока flight = TimeTable.GetFlight(contract);
            int countFlights = (int)contract.Количество_выполненных;
            if (flight.РазовыйРейс != null)
            {
                if (countFlights>=1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (flight.РегулярныйРейс != null)
                {
                    if (countFlights >= flight.РегулярныйРейс1.Регулярность.КоличествоПовторов)
                        return true;
                    else
                        return false;
                }
                else
                {
                    return true;
                }
            }
        }
    }
}
