﻿using ProjectAirline.ORM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAirline.Tools.DataBaseTools
{
    public abstract class Airport : Connection
    {
        /// <summary>
        /// Получить список все аэропортов
        /// </summary>
        /// <returns>Список аэропортом</returns>
        public static List<Аэропорт> GetAllAirports()
        {
            if (Game.DBMutex != null)
                Game.DBMutex.WaitOne();
            var query =
                from аэропорт in db.Аэропорт
                select аэропорт;
            if (Game.DBMutex != null)
                Game.DBMutex.ReleaseMutex();
            return query.ToList();
        }

        /// <summary>
        /// Получение аэропорта по id
        /// </summary>
        /// <param name="id_airport">id Аэропорта</param>
        /// <returns>Аэропорт</returns>
        public static Аэропорт GetAirport(long id_airport)
        {
            var query =
                from Аэропорт in db.Аэропорт
                where id_airport == Аэропорт.id
                select Аэропорт;
            if (query.ToList().Count > 0)
            {
                return query.ToList()[0];
            }
            else
            {
                throw new Exception("Информация о заданном аэропорте отсутствует");
            }
        }
    }
}
