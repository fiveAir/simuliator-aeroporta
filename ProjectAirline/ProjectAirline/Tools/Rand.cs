﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAirline.Core
{
    public static class Rand
    {
        private static Random Random = new Random();

        public static int Get(int maxValue)
        {
            return Random.Next(maxValue);
        }

        public static double Get(double maxValue)
        {
            return (double)Random.Next((int)(maxValue * 1000000)) / 1000000;
        }

        public static int Get(int minValue, int maxValue)
        {
            return Random.Next(minValue, maxValue);
        }
        
        public static double Get(double minValue, double maxValue)
        {
            return Random.NextDouble() * (maxValue - minValue) + minValue;
        }

        public static long Get(long minValue, long maxValue)
        {
            return (long)(Random.NextDouble() * ((double)maxValue - (double)minValue) + (double)minValue);
        }

        public static DateTime GenDate(DateTime dateA, DateTime dateB)
        {
            DateTime firstDate = dateA > dateB ? dateB : dateA;
            DateTime secondDate = dateA > dateB ? dateA : dateB;
            return new DateTime(Get(firstDate.Ticks, secondDate.Ticks));
        }

        public static bool GetСhance(double chance)
        {
            return Get(100) <= chance * 100;
        }
    }
}
