﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAirline.Tools
{
    public enum RandomType
    {
        Discrete,
        Сontinuous
    }

    public class ParamCollection<T> : Dictionary<string, T>
    {

    }

    public static class ExtMath
    {
        private static long ProdTree(int l, int r)
        {
            if (l > r)
                return 1;
            if (l == r)
                return l;
            if (r - l == 1)
                return (long)l * r;
            int m = (l + r) / 2;
            return ProdTree(l, m) * ProdTree(m + 1, r);
        }

        public static long Factorial(int n)
        {
            if (n < 0)
                return 0;
            if (n == 0)
                return 1;
            if (n == 1 || n == 2)
                return n;
            return ProdTree(2, n);
        }

        public static bool Between(double what, double from, double to)
        {
            return (what <= to && what >= from) ? true : false;
        }

        public static bool BetweenSemiInter(double what, double from, double to)
        {
            return (what <= to && what > from) ? true : false;
        }

        public static double Gamma(double x)
        {
            if (x > 0 && x < 1)
            {
                return Gamma(x + 1) / x;
            }
            else if (x >= 1 && x <= 2)
            {
                return 1 - 0.577191652 * Math.Pow(x - 1, 1) + 0.988205891 * Math.Pow(x - 1, 2) -
                        0.897056937 * Math.Pow(x - 1, 3) + 0.918206857 * Math.Pow(x - 1, 4) -
                        0.756704078 * Math.Pow(x - 1, 5) + 0.482199394 * Math.Pow(x - 1, 6) -
                        0.193527818 * Math.Pow(x - 1, 7) + 0.03586843 * Math.Pow(x - 1, 8);
            }
            else
            {
                return (x - 1) * Gamma(x - 1);
            }
        }
    }

    interface IGenerator
    {
        double Next();
        void Reset();
        void SetParams(params double[] args);
        void SetParams(ParamCollection<double> args);
        Func<double, double> Distr();
        double GetMaxValue();
        double GetMinValue();
        string Name { get; }
        RandomType Type { get; }
    }

    public class GeneratorBase : IGenerator
    {
        private double a, b, c, X;

        public GeneratorBase()
        {
            Reset();
        }

        public RandomType Type
        {
            get
            {
                return RandomType.Сontinuous;
            }
        }

        string IGenerator.Name
        {
            get
            {
                return "Равномерное распределение";
            }
        }

        public Func<double, double> Distr()
        {
            return x => ExtMath.Between(x, 0, 1) ? 1 : 0;
        }

        public double GetMaxValue()
        {
            return 1;
        }

        public double GetMinValue()
        {
            return 0;
        }

        public double Next()
        {
            X = (a * X + b) % c;
            return X / c;
        }

        public void Reset()
        {
            a = 106;
            b = 1283;
            c = 6075;

            X = DateTime.Now.Millisecond;
        }

        public void SetParams(ParamCollection<double> args)
        {
            return;
        }

        public void SetParams(params double[] args)
        {
            return;
        }
    }

    public abstract class DerivedGenerator
    {
        protected GeneratorBase innerGen;

        public DerivedGenerator()
        {
            Init();
        }

        private void Init()
        {
            innerGen = new GeneratorBase();
        }
        public DerivedGenerator(ParamCollection<double> args)
        {
            Init();
            SetParams(args);
        }

        public void Reset()
        {
            innerGen.Reset();
        }

        public virtual void SetParams(ParamCollection<double> args)
        {
            return;
        }
    }

    // Рабочий 2.0
    public class GeneratorGauss : DerivedGenerator, IGenerator
    {
        private double nu = 0;
        private double sigma = 1;

        public GeneratorGauss(ParamCollection<double> args) : base(args)
        {

        }

        public RandomType Type
        {
            get
            {
                return RandomType.Сontinuous;
            }
        }

        public string Name
        {
            get
            {
                return "Нормальное распределение";
            }
        }

        public Func<double, double> Distr()
        {
            return x => 1 / (sigma * Math.Sqrt(2 * Math.PI)) * Math.Exp(-Math.Pow(x - nu, 2) / (2 * Math.Pow(sigma, 2)));
        }

        public double GetMaxValue()
        {
            return nu + 3 * sigma;
        }

        public double GetMinValue()
        {
            return nu - 3 * sigma;
        }

        public double Next()
        {
            double sum = 0D;
            int k = 12;
            for (int i = 0; i < k; i++)
            {
                sum += innerGen.Next();
            }
            return nu + sigma * (sum - 6);
        }

        public void SetParams(params double[] args)
        {
            throw new NotImplementedException();
        }

        public override void SetParams(ParamCollection<double> args)
        {
            args.TryGetValue("nu", out nu);
            args.TryGetValue("sigma", out sigma);
        }
    }

}
