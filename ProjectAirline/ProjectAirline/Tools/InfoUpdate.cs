﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ProjectAirline
{
    public class InfoUpdate
    {
        private TimeSpan speedUpdate;                   // Частота запуска функции обновления
        private List<Action> updateDelegates;           // Функции обновления
        private bool IsUpdating;                        // Работа потока InfoUpdate

        /// <summary>
        /// Потоки обновления
        /// </summary>
        /// <param name="time">Ссылка на игровое время</param>
        /// <param name="speedUpdate">Частота запуска функции обновления</param>
        /// <param name="updateDelegate">Функции обновления</param>
        public InfoUpdate(TimeSpan speedUpdate, List<Action> updateDelegates)
        {
            this.speedUpdate = speedUpdate;
            this.updateDelegates = updateDelegates;
        }

        /// <summary>
        /// Потоки обновления
        /// </summary>
        /// <param name="time">Ссылка на игровое время</param>
        /// <param name="speedUpdate">Частота запуска функции обновления</param>
        /// <param name="updateDelegate">Функция обновления</param>
        public InfoUpdate(TimeSpan speedUpdate, Action updateDelegate)
        {
            this.speedUpdate = speedUpdate;
            updateDelegates = new List<Action>();
            updateDelegates.Add(updateDelegate);
        }

        /// <summary>
        /// Запустить все обновления
        /// </summary>
        public void Start()
        {
            IsUpdating = true;
            foreach (Action action in updateDelegates)
            {
                new Thread(UpdateTime).Start(action);
            }
        }

        /// <summary>
        /// Остановить все обновления
        /// </summary>
        public void Stop()
        {
            IsUpdating = false;
        }

        /// <summary>
        /// Поток обновления
        /// </summary>
        private void UpdateTime(object updateDelegate)
        {
            Action action = updateDelegate as Action;
            while (IsUpdating)
            {
                action();
                Thread.Sleep(speedUpdate);
            }
        }
    }
}
