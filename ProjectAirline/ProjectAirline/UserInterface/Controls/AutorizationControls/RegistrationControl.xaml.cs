﻿using ProjectAirline.ORM;
using ProjectAirline.Tools;
using ProjectAirline.Tools.DataBaseTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjectAirline.UserInterface.Controls.AutorizationControls
{
    /// <summary>
    /// Логика взаимодействия для RegistrationControl.xaml
    /// </summary>
    public partial class RegistrationControl : UserControl
    {
        public event EventHandler ClickStartGame;
        public event EventHandler ClickBack;

        public Аэропорт SelectedAirport {
            get
            {
                return (cboAirlines.SelectedItem as ItemComboBox).Item as Аэропорт;
            }
        }

        public string NameAirline
        {
            get
            {
                return txtNameAirline.Text;
            }
        }

        public RegistrationControl()
        {
            InitializeComponent();
            foreach (Аэропорт airport in Airport.GetAllAirports())
            {
                string contextAirport = airport.Наименование + " (" + airport.Местоположение.Город.Страна.Наименование + " " + airport.Местоположение.Город.Название + ")";
                ItemComboBox item = new ItemComboBox(airport, contextAirport);
                cboAirlines.Items.Add(item);
            }
            cboAirlines.SelectedIndex = 1;
        }

        private void btnBack(object sender, RoutedEventArgs e)
        {
            ClickBack?.Invoke(sender, e);
        }

        private void btnStartGame(object sender, RoutedEventArgs e)
        {
            string pattern = @"^[a-zA-Zа-яА-ЯёЁ0-9-_.]{1,15}$";
            Regex regex = new Regex(pattern);
            if (!regex.IsMatch(txtNameAirline.Text))
            {
                MessageBox.Show("В названии авиакомпании можно использовать только буквы, цифры, символы точки, нижнего подчеркивания и тире.\n" + "Название авиакомпании не может быть пустым!");
                return;
            }
            if (Player.IsCorrectNameAirline(txtNameAirline.Text))
            {
                ClickStartGame?.Invoke(sender, e);
            }
            else
            {
                MessageBox.Show("Данное имя занято другим игроком или авиакомпанией.");
            }
        }
    }
}
