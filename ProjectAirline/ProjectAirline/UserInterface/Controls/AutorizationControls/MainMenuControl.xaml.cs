﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjectAirline.UserInterface.Controls.AutorizationControls
{
    /// <summary>
    /// Логика взаимодействия для MainMenu.xaml
    /// </summary>
    public partial class MainMenuControl : UserControl
    {
        public event EventHandler ClickNewGame;
        public event EventHandler ClickLoadGame;
        public event EventHandler ClickExit;

        public MainMenuControl()
        {
            InitializeComponent();
        }

        private void NewGame(object sender, RoutedEventArgs e)
        {
            ClickNewGame?.Invoke(sender, e);
        }

        private void LoadGame(object sender, RoutedEventArgs e)
        {
            ClickLoadGame?.Invoke(sender, e);
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            ClickExit?.Invoke(sender, e);
        }
    }
}
