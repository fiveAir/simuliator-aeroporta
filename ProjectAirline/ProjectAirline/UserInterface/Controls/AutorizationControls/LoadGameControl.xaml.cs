﻿using ProjectAirline.ORM;
using ProjectAirline.Tools.DataBaseTools;
using ProjectAirline.UserInterface.Controls.AirListView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjectAirline.UserInterface.Controls.AutorizationControls
{
    /// <summary>
    /// Логика взаимодействия для LoadPlayersControl.xaml
    /// </summary>
    public partial class LoadGameControl : UserControl
    {
        public event EventHandler ClickStartGame; // кнопка "продолжить игру" - передать параметром sender - выбранного игрока
        public event EventHandler ClickBack; // кнопка "назад"

        private ViewAirList _viewAirList;   //Список игроков
        
        public LoadGameControl()
        {
            InitializeComponent();
            _viewAirList = new ViewAirList("Загрузить игру");
            List<Игрок> listPLayer = Player.GetAllPlayers();
            _viewAirList.AddButtonRow("Играть", butPlay_Click);
            _viewAirList.AddItems(listPLayer);
            grdControl.Children.Add(_viewAirList);
        }

        private void butPlay_Click(object sender, RoutedEventArgs e)
        {
            ClickStartGame?.Invoke(((sender as ButtonItem).RowItemAirList as ItemAirList).Item as Игрок, e);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ClickBack?.Invoke(sender, e);
        }
    }
}
