﻿using ProjectAirline.UserInterface.Controls.AirListView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ProjectAirline.ORM;
using ProjectAirline.Tools.DataBaseTools;
using ProjectAirline.UserInterface.Controls.MessageBoxControls;

namespace ProjectAirline.UserInterface.Controls
{
    /// <summary>
    /// Логика взаимодействия для MyAirplaneControl.xaml
    /// </summary>
    public partial class MyAirplaneControl : UserControl
    {
        private ViewAirList _viewAirList;

        public MyAirplaneControl()
        {
            InitializeComponent();
            _viewAirList = new ViewAirList("Мои самолеты");
            grdAirplaneControl.Children.Add(_viewAirList);
        }

        public void UpdateBody()
        {
            _viewAirList.Clear();
            List<СамолетИгрока> listAirplane = Aircraft.GetAircrafts();
            if (listAirplane != null)
            {
                AddMyAirplane(listAirplane);
            }
        }

        public void AddMyAirplane(List<СамолетИгрока> listAirplane)
        {
            foreach (СамолетИгрока самолет in listAirplane)
            {
                AddMyAirplane(самолет);
            }
        }

        /// <summary>
        /// Добавление самолета 
        /// </summary>
        /// <param name="airplane">Самолет</param>
        public void AddMyAirplane(СамолетИгрока airplane)
        {
            ItemAirList itemAirplane = new ItemAirList(airplane);
            //<Добавление кнопки>
            ДоговорНаСамолет contract = airplane.ДоговорНаСамолет;
            itemAirplane.AddButton("  Поставить\n" + "в расписание", AddInTimetable, itemAirplane);
            if (contract.Покупка != null)
            {
                itemAirplane.AddButton("Продать", SellPlaneEvent, itemAirplane);
            }
            if (contract.Аренда != null)
            {
                itemAirplane.AddButton("Вернуть\n(Аренда)", AnLeasePlaneEvent, itemAirplane);
            }
            if (contract.Лизинг != null)
            {
                itemAirplane.AddButton("Вернуть\n(Лизинг)", AnLeasingPlaneEvent, itemAirplane);
            }
            //</Добавление кнопки>
            _viewAirList.AddItem(itemAirplane);
        }

        ChangeTimetable ctrlChangeTimetable;

        /// <summary>
        /// Обработчик кнопки "Поставить в расписание"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddInTimetable(Object sender, EventArgs e)
        {
            _viewAirList.Visibility = Visibility.Hidden;
            ctrlChangeTimetable = new ChangeTimetable("Составление расписания", ChangeTimetable.TypeChange.Add);
            ctrlChangeTimetable.Cancel += CancelChangeTimetable;
            grdAirplaneControl.Children.Add(ctrlChangeTimetable);
        }

        private void CancelChangeTimetable(object sender, EventArgs e)
        {
            ctrlChangeTimetable.Visibility = Visibility.Hidden;
            _viewAirList.Visibility = Visibility.Visible;
        }

        ItemAirList itemAirList;
        СамолетИгрока airplane;

        /// <summary>
        /// Обработка покупки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SellPlaneEvent(Object sender, EventArgs e)
        {
            itemAirList = (sender as ButtonItem).RowItemAirList as ItemAirList;
            airplane = itemAirList.Item as СамолетИгрока;
            AcceptMessageboxControl amsbControl = new AcceptMessageboxControl("Вы действительно хотите продать самолет?",
                airplane.СамолетИгрыИгрока.МодельСамолета.Наименование +" за " + airplane.ДоговорНаСамолет.Покупка.СтоимостьСамолета + " ©",
                grdAirplaneControl, grdEventAirplaneControl);
            amsbControl.Accept += AmsbControl_Accept_Sell;
            amsbControl.ShowDialog();
        }

        private void AmsbControl_Accept_Sell(object sender, EventArgs e)
        {
            Aircraft.SellAircraft(airplane);
            _viewAirList.Remove(itemAirList);
        }

        /// <summary>
        /// Обработка аренды
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AnLeasePlaneEvent(Object sender, EventArgs e)
        {
            itemAirList = (sender as ButtonItem).RowItemAirList as ItemAirList;
            airplane = itemAirList.Item as СамолетИгрока;
            AcceptMessageboxControl amsbControl = new AcceptMessageboxControl("Вы действительно хотите вернуть самолет?",
                airplane.СамолетИгрыИгрока.МодельСамолета.Наименование,
                grdAirplaneControl, grdEventAirplaneControl);
            amsbControl.Accept += AmsbControl_Accept_Rent;
            amsbControl.ShowDialog();
        }

        private void AmsbControl_Accept_Rent(object sender, EventArgs e)
        {
            Aircraft.StopRent(airplane);
            _viewAirList.Remove(itemAirList);
        }

        /// <summary>
        /// Обработка лизинга
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AnLeasingPlaneEvent(Object sender, EventArgs e)
        {
            itemAirList = (sender as ButtonItem).RowItemAirList as ItemAirList;
            airplane = itemAirList.Item as СамолетИгрока;
            AcceptMessageboxControl amsbControl = new AcceptMessageboxControl("Вы действительно хотите вернуть самолет?",
                airplane.СамолетИгрыИгрока.МодельСамолета.Наименование,
                grdAirplaneControl, grdEventAirplaneControl);
            amsbControl.Accept += AmsbControl_Accept_Lease;
            amsbControl.ShowDialog();
        }

        private void AmsbControl_Accept_Lease(object sender, EventArgs e)
        {
            Aircraft.StopLease(airplane);
            _viewAirList.Remove(itemAirList);
        }
    }
}
