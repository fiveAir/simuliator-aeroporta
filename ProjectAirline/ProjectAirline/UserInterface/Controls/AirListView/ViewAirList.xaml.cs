﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using ProjectAirline.ORM;

namespace ProjectAirline.UserInterface.Controls.AirListView
{
    /// <summary>
    /// Таблица представления данных в AIRSIM
    /// </summary>
    public partial class ViewAirList : UserControl
    {
        private bool _canRowSelected = false;
        private ItemAirList _prevSelectedRow;
        List<ItemAirList.ButtonRow> _listButtonsRow;

        /// <summary>
        /// Таблица представления данных в AIRSIM
        /// </summary>
        /// <param name="nameTable">Название таблицы</param>
        public ViewAirList(String nameTable, bool canRowSelected = false, Action updateDelegate = null)
        {
            InitializeComponent();
            lblNameTable.Content = nameTable;
            _canRowSelected = canRowSelected;
            _listButtonsRow = new List<ItemAirList.ButtonRow>();

            if (updateDelegate != null)
            {
                btnUpdate.Visibility = Visibility.Visible;
                btnUpdate.Click += (object sender, RoutedEventArgs e) =>
                {
                    btnUpdate.IsEnabled = false;
                    updateDelegate.Invoke();
                    btnUpdate.IsEnabled = true;
                };
            }

            EnsureNonEmpty();
        }
        
        /// <summary>
        /// Добавить кнопки для строк
        /// </summary>
        /// <param name="listButtonsRow"></param>
        public void SetButtonsRow(List<ItemAirList.ButtonRow> listButtonsRow)
        {
            _listButtonsRow = listButtonsRow;
        }

        /// <summary>
        /// Добавление кнопки на строку
        /// </summary>
        /// <param name="name"></param>
        /// <param name="methodButton"></param>
        public void AddButtonRow(string name, RoutedEventHandler methodButton)
        {
            _listButtonsRow.Add(new ItemAirList.ButtonRow(name, methodButton));
        }

        /// <summary>
        /// Очистка таблицы
        /// </summary>
        public void Clear()
        {
            stpAirList.Children.Clear();
            EnsureNonEmpty();
        }

        public void Remove(ItemAirList itemAirList)
        {
            stpAirList.Children.Remove(itemAirList);
            EnsureNonEmpty();
        }

        /// <summary>
        /// Добавление ItemAirList в таблицу
        /// </summary>
        /// <param name="item"></param>
        public void AddItem(ItemAirList item)
        {
            RemovePlaceholder();
            if(_canRowSelected)
            {
                item.SetSelectedRow();
                item.SelectedRow += ChangedRows;
            }
            stpAirList.Children.Add(item);
        }

        /// <summary>
        /// Добавление в таблицу строк
        /// </summary>
        /// <param name="listItems"></param>
        public void AddItems(List<ItemAirList> listItems)
        {
            foreach (ItemAirList itemRow in listItems)
            {
                AddItem(itemRow);
            }
        }

        /// <summary>
        /// Добавление в таблицу строки Игрока
        /// </summary>
        /// <param name="listItems"></param>
        public void AddItem(Игрок item)
        {
            if (item != null)
            {
                AddItem(new ItemAirList(item, _listButtonsRow));
            }
        }

        /// <summary>
        /// Добавление в таблицу строк Аэропорт
        /// </summary>
        /// <param name="listItems"></param>
        public void AddItems(List<Аэропорт> listItems)
        {
            foreach (Аэропорт item in listItems)
            {
                AddItem(new ItemAirList(item, _listButtonsRow));
            }
        }

        /// <summary>
        /// Добавление в таблицу строки Аэропорт
        /// </summary>
        /// <param name="listItems"></param>
        public void AddItem(Аэропорт item)
        {
            if (item != null)
            {
                AddItem(new ItemAirList(item, _listButtonsRow));
            }
        }

        /// <summary>
        /// Добавление в таблицу строк Игрока
        /// </summary>
        /// <param name="listItems"></param>
        public void AddItems(List<Игрок> listItems)
        {
            foreach (Игрок item in listItems)
            {
                AddItem(new ItemAirList(item, _listButtonsRow));
            }
        }

        /// <summary>
        /// Добавление в таблицу строки Магазина
        /// </summary>
        /// <param name="item"></param>
        public void AddItem(Магазин item)
        {
            if (item != null)
            {
                AddItem(new ItemAirList(item, _listButtonsRow));
            }
        }

        /// <summary>
        /// Добавление в таблицу строк Магазинов
        /// </summary>
        /// <param name="listItems"></param>
        public void AddItems(List<Магазин> listItems)
        {
            foreach (Магазин item in listItems)
            {
                AddItem(new ItemAirList(item, _listButtonsRow));
            }
        }

        /// <summary>
        /// Добавление в таблицу строки Рейса    
        /// </summary>
        /// <param name="item"></param>
        public void AddItem(РейсИгрыИгрока item)
        {
            if (item != null)
            {
                AddItem(new ItemAirList(item, _listButtonsRow));
            }
        }

        /// <summary>
        /// Добавление в таблицу строк Рейсов
        /// </summary>
        /// <param name="listItems"></param>
        public void AddItems(List<РейсИгрыИгрока> listItems)
        {
            foreach (РейсИгрыИгрока item in listItems)
            {
                AddItem(new ItemAirList(item, _listButtonsRow));
            }
        }

        /// <summary>
        /// Добавление в таблицу строки Самолета
        /// </summary>
        /// <param name="item"></param>
        public void AddItem(СамолетИгрока item)
        {
            if (item != null)
            {
                AddItem(new ItemAirList(item, _listButtonsRow));
            }
        }

        /// <summary>
        /// Добавление в таблицу строк Самолетов
        /// </summary>
        /// <param name="listItems"></param>
        public void AddItems(List<СамолетИгрока> listItems)
        {
            foreach (СамолетИгрока item in listItems)
            {
                AddItem(new ItemAirList(item, _listButtonsRow));
            }
        }

        /// <summary>
        /// Добавление в таблицу строки Расписания
        /// </summary>
        /// <param name="item"></param>
        public void AddItem(Расписание item)
        {
            if (item != null)
            {
                AddItem(new ItemAirList(item, _listButtonsRow));
            }
        }

        /// <summary>
        /// Добавление в таблицу строк Расписания
        /// </summary>
        /// <param name="listItems"></param>
        public void AddItems(List<Расписание> listItems)
        {
            foreach (Расписание item in listItems)
            {
                AddItem(new ItemAirList(item, _listButtonsRow));
            }
        }

        public event ItemAirList.RowEventHendler SelectedRowChanged;


        private void ChangedRows(ItemAirList sender)
        {
            if (_prevSelectedRow == sender)
            {
                _prevSelectedRow.LeaveSelected();
                _prevSelectedRow = null;
            }
            else
            {
                if (_prevSelectedRow != null)
                {
                    _prevSelectedRow.LeaveSelected();
                }
                sender.EnterSelected();
                _prevSelectedRow = sender;
                SelectedRowChanged?.Invoke(sender);
            }
        }

        private PlaceholderItem placeholder = new PlaceholderItem();
        private void EnsureNonEmpty()
        {
            if (stpAirList.Children.Count == 0)
            {
                stpAirList.Children.Add(placeholder);
            }
        }

        private void RemovePlaceholder()
        {
            if (stpAirList.Children.Contains(placeholder))
            {
                stpAirList.Children.Remove(placeholder);
            }
        }

        public void SetPlaceholderText(string Text)
        {
            placeholder.label.Content = Text;
        }
    }
}
