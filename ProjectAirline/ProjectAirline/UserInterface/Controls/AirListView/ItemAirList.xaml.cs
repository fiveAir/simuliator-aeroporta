﻿using ProjectAirline.ORM;
using ProjectAirline.Tools.DataBaseTools;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using ProjectAirline.Tools;

namespace ProjectAirline.UserInterface.Controls.AirListView
{
    /// <summary>
    /// Логика взаимодействия для ItemAirList.xaml
    /// </summary>
    public partial class ItemAirList : UserControl
    {
        private int curIndexMain = 0;           //текущий индекс главных данных
        private int curIndexExtra = 0;          //текущий индекс дополнительных главных данных
        private int curIndexButton = 0;         //текущий индекс кнопок
        private int curIndexContext = 0;        //текущий индекс контекстных данных
        private int sizeFontMain = 18;          //размер шрифта главных данных
        private int sizeFontExtra = 14;         //размер шрифта дополнительных данных
        private bool _isSelected = false;
        private object _item;                   //объект строки       

        public object Item
        {
            get
            {
                return _item;
            }
        }


        /// <summary>
        /// Тип строки
        /// </summary>
        private enum TypeData
        {
            Main, Extra
        }

        /// <summary>
        /// Статическая инициализация контрола
        /// </summary>
        static ItemAirList()
        {
            ToolTipService.ShowDurationProperty.OverrideMetadata(
                typeof(DependencyObject), new FrameworkPropertyMetadata(Int32.MaxValue));
        }

        /// <summary>
        /// Строка для Игрока
        /// </summary>
        /// <param name="item">Игрок</param>
        /// <param name="listButton">Список кнопок</param>
        public ItemAirList(Игрок item, List<ButtonRow> listButton = null)
        {
            InitializeComponent();
            _item = item;
            AddMainData("Авиакомпания:", item.НазваниеАвиакомпании);
            AddExtraData("баланс:", Math.Round(item.Баланс, 2) + " ©");
            AddExtraData("время:", item.ИгровоеВремя.ToString());
            AddButtonsForList(listButton);
        }

        /// <summary>
        /// Строка для Рейса
        /// </summary>
        /// <param name="item">Рейса</param>
        /// <param name="listButton">Список кнопок</param>
        public ItemAirList(РейсИгрыИгрока item, List<ButtonRow> listButton = null)
        {
            InitializeComponent();
            _item = item;
            //инициализация строки
            AddMainData("от:", item.Перелет.Аэропорт1.Наименование);
            AddMainData("до:", item.Перелет.Аэропорт.Наименование);
            if (item.РазовыйРейс == null && item.РегулярныйРейс1 == null)
            {
                AddExtraData("", "Пустой перелет");
            }
            else
            {
                if (item.РазовыйРейс != null)
                {
                    AddExtraData("дата выполнения:", item.РазовыйРейс.ДатаВыполнения);
                }
                else
                {
                    AddExtraData("Регулярность:", item.РегулярныйРейс1.Регулярность.КоличествоПовторов + " раз(а) в " +
                        item.РегулярныйРейс1.Регулярность.Диапазон + " дней");
                    if (item.ДоговорНаРейс != null)
                    {
                        AddContextData("Дата взятия договора:", item.ДоговорНаРейс.ДатаНачала);
                        AddContextData("Дата окончания договора:", item.ДоговорНаРейс.ДатаОкончания);
                        AddContextData("Выполненная регулярность:", item.ДоговорНаРейс.Количество_выполненных + " раз(а) в " +
                            item.РегулярныйРейс1.Регулярность.Диапазон + " дней");
                    }
                }
                if (item.ПассажирскийРейс != null)
                {
                    AddExtraData("тип:", "Пассажирский");
                    AddContextData("Вместимость:", item.ПассажирскийРейс.МаксимальноеКоличествоПассажиров.ToString() + " мест");
                    AddContextData("Прибыль:", Math.Round(Flight.GetPassengerPayment(item), 2) + " ©");
                    AddContextData("Неустойка:", Math.Round(Flight.GetPassengerPenalty(item), 2) + " ©");
                    AddContextData("Выгодное время:", Flight.GetPeakTime(item.ПассажирскийРейс).ToString());
                }
                else
                {
                    AddExtraData("тип:", "Грузовой");
                    AddContextData("Грузоподъемность:", Math.Round(item.ГрузовойРейс.ВесГруза, 2).ToString() + " тонн");
                    AddContextData("Прибыль:", Math.Round(Flight.GetCargoPayment(item), 2) + " ©");
                    AddContextData("Неустойка:", Math.Round(Flight.GetCargoPenalty(item), 2) + " ©");
                }
            }
            AddContextData("Дальность:", Math.Round(Flight.GetDistance(item.Перелет), 2).ToString() + " км");
            //<Добавление кнопки>
            AddButtonsForList(listButton);
            //</Добавление кнопки>
        }

        public ItemAirList(Аэропорт item, List<ButtonRow> listButton = null)
        {
            InitializeComponent();
            _item = item;
            //инициализация строки
            AddMainData("аэропорт:", item.Наименование);
            AddExtraData("страна:", item.Местоположение.Город.Страна.Наименование);
            AddExtraData("город:", item.Местоположение.Город.Название);
            //<Добавление кнопки>
            AddButtonsForList(listButton);
            //</Добавление кнопки>
        }

        public ItemAirList(Расписание item, List<ButtonRow> listButton = null)
        {
            InitializeComponent();
            _item = item;
            //инициализация строки
            AddMainData("от:", TimeTable.GetFlight(item.ДоговорНаРейс).Перелет.Аэропорт1.Наименование);
            AddMainData("до:", TimeTable.GetFlight(item.ДоговорНаРейс).Перелет.Аэропорт.Наименование);
            AddExtraData("дата вылета:", item.ДатаВремяВылета);
            if (item.СамолетИгрыИгрока.МодельСамолета.ПассажирскийСамолет != null)
            {
                AddExtraData("тип:", "Пассажирский");
            }
            else
            {
                AddExtraData("тип:", "Грузовой");
            }
            if(Math.Round((item.ДоговорНаРейс.Неустойка * 2), 2)==0)
            {
                AddExtraData("Пустой перелет","");
            }
            else
            {
                AddExtraData("Прибыль:", Math.Round((item.ДоговорНаРейс.Неустойка * 2), 2).ToString());
            }
            AddContextData("Бортовой номер:", Aircraft.GetTailNumber(item.СамолетИгрыИгрока));
            AddContextData("Длительность полета:", Flight.GetFlightTime(item).ToString());
            //<Добавление кнопки>
            AddButtonsForList(listButton);
            //</Добавление кнопки>
        }

        /// <summary>
        /// Строка для Самолета Игрока
        /// </summary>
        /// <param name="item">Самолет Игрока</param>
        /// <param name="listButton">Список кнопок</param>
        public ItemAirList(СамолетИгрока item, List<ButtonRow> listButton = null)
        {
            InitializeComponent();
            _item = item;
            //инициализация строки
            FillAirplane(item.СамолетИгрыИгрока);
            if(item.Аэропорт == null)
            {
                AddExtraData("место:", "В полете");
            }
            else
            {
                AddExtraData("место:", item.Аэропорт.Наименование);
            }
            //<Добавление кнопки>
            AddButtonsForList(listButton);
            //</Добавление кнопки>
        }
               

        /// <summary>
        /// Заполнение строки для Самолета Игры Игрока
        /// </summary>
        /// <param name="item">Самолет Игры Игрока</param>
        public void FillAirplane(СамолетИгрыИгрока item)
        {
            AddMainData("Бортовой номер:", Aircraft.GetTailNumber(item));
            AddMainData("", item.МодельСамолета.Наименование);
            if (item.МодельСамолета.ПассажирскийСамолет != null)
            {
                AddExtraData("тип:", "Пассажирский");
                AddExtraData("Кол-во мест:", item.МодельСамолета.ПассажирскийСамолет.КоличествоМест.ToString());
            }
            else
            {
                AddExtraData("тип:", "Грузовой");
                AddExtraData("Макс. груз:", item.МодельСамолета.ГрузовойСамолет.Грузоподъемность.ToString());
            }
            AddContextData("Максимальная дистанция:", item.МодельСамолета.МаксимальнаяДальность.ToString() + " км");
            AddContextData("Максимальная скорость:", item.МодельСамолета.Скорость.ToString() + " км/ч");
            AddContextData("Расход топлива:", item.МодельСамолета.РасходТоплива.ToString() + " литр/км");
        }

        /// <summary>
        /// Строка для Магазина
        /// </summary>
        /// <param name="item">Магазин</param>
        /// <param name="listButton">Список кнопок</param>
        public ItemAirList(Магазин item, List<ButtonRow> listButton = null)
        {
            InitializeComponent();
            _item = item;
            //инициализация строки
            FillAirplane(item.СамолетИгрыИгрока);
            //<Добавление кнопки>
            AddButtonsForList(listButton);
            //</Добавление кнопки>
        }

        /// <summary>
        /// Структура для передачи метода обработки нажатия на кнопку
        /// </summary>
        public struct ButtonRow
        {
            public string nameButton;           //имя кнопки
            public RoutedEventHandler methodButton;   //метод обработки нажатия

            /// <summary>
            /// Метод обработки нажатия на кнопку
            /// </summary>
            /// <param name="name">Текст кнопки</param>
            /// <param name="method">Метод выполнения</param>
            public ButtonRow(string name, RoutedEventHandler method)
            {
                nameButton = name;
                methodButton = method;
            }

        }

        /// <summary>
        /// Добавление кнопок из списка
        /// </summary>
        /// <param name="listButton">Список кнопок</param>
        private void AddButtonsForList(List<ButtonRow> listButton)
        {
            if (listButton != null)
            {
                foreach (ButtonRow but in listButton)
                {
                    AddButton(but.nameButton, but.methodButton, this);
                }
            }
        }

        /// <summary>
        /// Разрешить выделение
        /// </summary>
        public void SetSelectedRow()
        {
            borderRow.MouseLeftButtonDown += SelectRow;
        }

        //делегат для события выделения строки
        public delegate void RowEventHendler(ItemAirList sender);
        //событие выделения строки
        public event RowEventHendler SelectedRow;

        /// <summary>
        /// Обработка нажатия на строку
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectRow(object sender, MouseButtonEventArgs e)
        {
            if (_isSelected)
            {
                SelectedRow?.Invoke(this);
            }
            else
            {
                SelectedRow?.Invoke(this);
            }
        }

        /// <summary>
        /// Выделение строки
        /// </summary>
        public void EnterSelected()
        {
            dockPanel.Background = Palette.GetBrush(Color.Green);
            _isSelected = true;
        }

        /// <summary>
        /// Снятие выделения строки
        /// </summary>
        public void LeaveSelected()
        {
            dockPanel.Background = Palette.GetBrush(Color.DimGray);
            _isSelected = false;
        }

        /// <summary>
        /// Добавление данных в главную строку
        /// </summary>
        /// <param name="prefix">Префикс данных</param>
        /// <param name="data">Значение данных</param>
        public void AddMainData(string prefix, string data)
        {
            grdMainData.ColumnDefinitions.Add(new ColumnDefinition());
            AddData(prefix, data, TypeData.Main);
            curIndexMain++;
        }

        /// <summary>
        /// Добавление данных в дополнительную строку
        /// </summary>
        /// <param name="prefix">Префикс данных</param>
        /// <param name="data">Значение данных</param>
        public void AddExtraData(string prefix, string data)
        {
            grdExtraData.ColumnDefinitions.Add(new ColumnDefinition());
            AddData(prefix, data, TypeData.Extra);
            curIndexExtra++;
        }

        /// <summary>
        /// Добавление данных в ячейку Grid
        /// </summary>
        /// <param name="prefix">Префикс ячейки</param>
        /// <param name="data">Значение ячейки</param>
        /// <param name="typeData">Тип ячейки</param>
        private void AddData(string prefix, string data, TypeData typeData)
        {
            StackPanel stcPanel = new StackPanel()
            {
                Orientation = Orientation.Horizontal,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center
            };
            Label lblPrefix = new Label()
            {
                Content = prefix,
                Foreground = Palette.GetBrush(Color.White)
            };
            Label lblData = new Label()
            {
                Content = data,
                Foreground = Palette.GetBrush(Color.White)
            };
            switch (typeData)
            {
                case TypeData.Main:
                    lblPrefix.FontSize = sizeFontMain;
                    lblData.FontSize = sizeFontMain;
                    stcPanel.Children.Add(lblPrefix);
                    stcPanel.Children.Add(lblData);
                    Grid.SetColumn(stcPanel, curIndexMain);
                    grdMainData.Children.Add(stcPanel);
                    break;
                case TypeData.Extra:
                    lblPrefix.FontSize = sizeFontExtra;
                    lblData.FontSize = sizeFontExtra;
                    stcPanel.Children.Add(lblPrefix);
                    stcPanel.Children.Add(lblData);
                    Grid.SetColumn(stcPanel, curIndexExtra);
                    grdExtraData.Children.Add(stcPanel);
                    break;
            }
        }

        /// <summary>
        /// Добавление кнопки для обработки строки
        /// </summary>
        /// <param name="name">Название</param>
        /// <param name="butEventHandler">обработчик нажатия кнопки</param>
        /// <param name="itemAirList">ItemAirList</param>
        /// <param name="Hidden">скрыть кнопку</param>
        public void AddButton(string name, RoutedEventHandler butEventHandler, ItemAirList itemAirList, double width = 0, bool Hidden = false)
        {
            grdCollectBtn.ColumnDefinitions.Add(new ColumnDefinition());
            ButtonItem butItem = new ButtonItem()
            {
                Content = name,
                Background = Palette.GetBrush(Color.DarkGray),
                Margin = new Thickness(0, 5, 5, 5),
                Padding = new Thickness(5, 0, 5, 0),
                Foreground = Palette.GetBrush(Color.White),
                RowItemAirList = itemAirList,
            };
            if (width != 0)
            {
                butItem.Width = width;
            }
            if (Hidden)
            {
                butItem.Visibility = Visibility.Hidden;
            }
            butItem.Click += butEventHandler;
            Grid.SetColumn(butItem, curIndexButton);
            grdCollectBtn.Children.Add(butItem);
            curIndexButton++;
        }

        /// <summary>
        /// Добавление данных в контекстное меню
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="data"></param>
        public void AddContextData(string prefix, string data)
        {
            //if (curIndexContext == 0)
            //{
            //    toolTip.Visibility = Visibility.Visible;
            //}
            grdContexData.RowDefinitions.Add(new RowDefinition());
            Label lblPrefix = new Label
            {
                Content = prefix
            };
            Grid.SetRow(lblPrefix, curIndexContext);
            Grid.SetColumn(lblPrefix, 0);
            Label lblData = new Label
            {
                Content = data
            };
            Grid.SetRow(lblData, curIndexContext);
            Grid.SetColumn(lblData, 1);
            grdContexData.Children.Add(lblPrefix);
            grdContexData.Children.Add(lblData);
            curIndexContext++;
        }

        private void dockPanel_MouseEnter(object sender, MouseEventArgs e)
        {
            popUp.IsOpen = true;
        }
        private void dockPanel_MouseLeave(object sender, MouseEventArgs e)
        {
            popUp.IsOpen = false;
        }
        
        private bool needPopupUpdate = false;
        private void dockPanel_LayoutUpdated(object sender, EventArgs e)
        {
            if (needPopupUpdate)
            {
                var mode = popUp.Placement;
                popUp.Placement = PlacementMode.Relative;
                popUp.Placement = mode;

                needPopupUpdate = false;
            }
        }

        private void dockPanel_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            needPopupUpdate = true;
        }
    }
}

