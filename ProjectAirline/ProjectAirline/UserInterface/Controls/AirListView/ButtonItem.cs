﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ProjectAirline.UserInterface.Controls.AirListView
{
    public partial class ButtonItem : Button
    {
        public ItemAirList RowItemAirList;      //строка, в которой содержится кнопка

        /// <summary>
        /// Кнопка строки ItemAirList
        /// </summary>
        public ButtonItem()
        {
            InitializeComponent();
        }
    }
}
