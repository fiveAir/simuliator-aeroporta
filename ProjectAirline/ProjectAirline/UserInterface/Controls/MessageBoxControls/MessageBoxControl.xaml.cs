﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjectAirline.UserInterface.Controls.MessageBoxControls
{
    /// <summary>
    /// Логика взаимодействия для MessageBoxControl.xaml
    /// </summary>
    public partial class MessageBoxControl : UserControl
    {
        private Grid _owner;
        private Grid _child;

        public MessageBoxControl(string text, Grid owner, Grid child)
        {
            InitializeComponent();
            lblText.Content = text;
            _owner = owner;
            _child = child;
        }

        public void ShowDialog()
        {
            _owner.Opacity = 0.5;
            _owner.IsEnabled = false;
            _child.Children.Add(this);
        }

        public event EventHandler Ok;

        private void butOk_Click(object sender, RoutedEventArgs e)
        {
            _owner.Opacity = 1;
            _owner.IsEnabled = true;
            _child.Children.Clear();
            Ok?.Invoke(sender, e);
        }
    }
}
