﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjectAirline.UserInterface.Controls.MessageBoxControls
{
    /// <summary>
    /// Логика взаимодействия для AcceptMessageboxControl.xaml
    /// </summary>
    public partial class AcceptMessageboxControl : UserControl
    {
        private Grid _owner;
        private Grid _child;

        public AcceptMessageboxControl(string Text, string TextAdd, Grid owner, Grid child)
        {
            InitializeComponent();
            lblText.Content = Text;
            lblTextAdd.Content = TextAdd;
            _owner = owner;
            _child = child;
        }

        public void ShowDialog()
        {
            _owner.Opacity = 0.5;
            _owner.IsEnabled = false;
            _child.Children.Add(this);
        }

        public event EventHandler Cancel;
        public event EventHandler Accept;

        private void butYes_Click(object sender, RoutedEventArgs e)
        {
            _owner.Opacity = 1;
            _owner.IsEnabled = true;
            _child.Children.Clear();
            Accept?.Invoke(sender, e);
        }

        private void butNo_Click(object sender, RoutedEventArgs e)
        {
            _owner.Opacity = 1;
            _owner.IsEnabled = true;
            _child.Children.Clear();
            Cancel?.Invoke(sender, e);
        }
    }
}
