﻿using ProjectAirline.ORM;
using ProjectAirline.Tools.DataBaseTools;
using ProjectAirline.UserInterface.Controls.AirListView;
using ProjectAirline.UserInterface.Controls.MessageBoxControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjectAirline.UserInterface.Controls
{
    /// <summary>
    /// Логика взаимодействия для MyFlightControl.xaml
    /// </summary>
    public partial class MyFlightControl : UserControl
    {
        private ViewAirList _viewAirList;
        public MyFlightControl()
        {
            InitializeComponent();
            _viewAirList = new ViewAirList("Мои рейсы", false, UpdateBody);
            _viewAirList.AddButtonRow("  Поставить\n" + "в расписание", AddInTimetable);
            _viewAirList.AddButtonRow("Отказаться", CancelFlight);
            grdMyFlightControl.Children.Add(_viewAirList);
        }

        public void UpdateBody()
        {
            _viewAirList.Clear();
            grdEventMyFlightControl.Children.Clear();
            grdMyFlightControl.Visibility = Visibility.Visible;
            grdMyFlightControl.Opacity = 1;
            grdMyFlightControl.IsEnabled = true;
            List<РейсИгрыИгрока> listAirplane = Flight.GetSelectedFlights();
            if (listAirplane != null)
            {
                _viewAirList.AddItems(listAirplane);
            }
        }
        
        ItemAirList itemAirList;
        РейсИгрыИгрока flight;

        /// <summary>
        /// Обработчик кнопки "Поставить в расписание"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddInTimetable(Object sender, EventArgs e)
        {
            itemAirList = (sender as ButtonItem).RowItemAirList as ItemAirList;
            flight = itemAirList.Item as РейсИгрыИгрока;
            if (flight.Игрок == null)
            {
                MessageBoxControl msgBoxControl = new MessageBoxControl("Срок выполнения данного рейса истек, обновитесь!", grdMyFlightControl, grdEventMyFlightControl);
                msgBoxControl.Ok += MsgBoxControl_Ok;
                msgBoxControl.ShowDialog();
            }
            else
            {
                ChangeTimetable ctrlChangeTimetable = new ChangeTimetable("Составление расписания", ChangeTimetable.TypeChange.Add, flight);
                ctrlChangeTimetable.AddChange += CtrlChangeTimetable_AddChange;
                ctrlChangeTimetable.Cancel += CancelChangeTimetable;
                grdEventMyFlightControl.Children.Add(ctrlChangeTimetable);
                grdMyFlightControl.Visibility = Visibility.Hidden;
            }
        }

        private void MsgBoxControl_Ok(object sender, EventArgs e)
        {
            UpdateBody();
        }

        private void CtrlChangeTimetable_AddChange(object sender, EventArgs e)
        {
            grdEventMyFlightControl.Children.Clear();
            grdMyFlightControl.Visibility = Visibility.Visible;
        }

        private void CancelChangeTimetable(object sender, EventArgs e)
        {
            grdEventMyFlightControl.Children.Clear();
            grdMyFlightControl.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Обработчик кнопки "Отказаться от рейса"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelFlight(Object sender, EventArgs e)
        {
            itemAirList = (sender as ButtonItem).RowItemAirList as ItemAirList;
            flight = itemAirList.Item as РейсИгрыИгрока;
            if (flight.ДоговорНаРейс == null)
            {
                MessageBoxControl msgBoxControl = new MessageBoxControl("Данные устарели, обновитесь!", grdMyFlightControl, grdEventMyFlightControl);
                msgBoxControl.Ok += MsgBoxControl_Ok;
                msgBoxControl.ShowDialog();
            }
            else
            {
                AcceptMessageboxControl amsbControl = new AcceptMessageboxControl("Вы действительно хотите отказаться от рейса?",
                "Вам придется оплатить неустойку: " + Math.Round(Flight.GetPriceCancelFlight(flight), 2) + " ©",
                 grdMyFlightControl, grdEventMyFlightControl);
                amsbControl.Accept += AmsbControl_Accept_Sell;
                amsbControl.ShowDialog();
            }
        }

        private void AmsbControl_Accept_Sell(object sender, EventArgs e)
        {            
            Flight.CancelFlight(flight);
            _viewAirList.Remove(itemAirList);
            UpdateBody();
        }

    }
}
