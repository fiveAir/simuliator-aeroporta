﻿using ProjectAirline.ORM;
using ProjectAirline.Tools.DataBaseTools;
using ProjectAirline.UserInterface.Controls.MessageBoxControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjectAirline.UserInterface.Controls
{
    /// <summary>
    /// Логика взаимодействия для LeaseDurationShopControl.xaml
    /// </summary>
    public partial class LeaseDurationShopControl : UserControl
    {
        private Grid _owner;
        private Grid _child;
        private Магазин _shop;

        public LeaseDurationShopControl(Магазин shop, Grid owner, Grid child)
        {
            InitializeComponent();
            _owner = owner;
            _child = child;
            _shop = shop;
            InitializeComboBox(cboDuration, 61);
            cboDuration.Items.RemoveAt(0);
            cboDuration.SelectedIndex = 0;
        }

        public void ShowDialog()
        {
            _owner.Opacity = 0.5;
            _owner.IsEnabled = false;
            _child.Children.Add(this);
        }

        private void InitializeComboBox(ComboBox cbo, int maxComponent)
        {
            cbo.Items.Clear();
            for (int i = 0; i < maxComponent; i++)
            {
                string str = i.ToString();
                if (str.Length == 1)
                    cbo.Items.Add("0" + str);
                else
                    cbo.Items.Add(str);
            }
        }

        public event EventHandler Cancel;
        public event EventHandler Accept;

        private void butYes_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Shop.LeaseAircraft(_shop, Int32.Parse(cboDuration.Text));
            }
            catch (Exception ex)
            {
                MessageBoxControl msbControl = new MessageBoxControl(ex.Message, grdBox, grdChildBox);
                msbControl.ShowDialog();
                return;
            }
            _owner.Opacity = 1;
            _owner.IsEnabled = true;
            _child.Children.Clear();
            Accept?.Invoke(sender, e);
        }

        private void butNo_Click(object sender, RoutedEventArgs e)
        {
            _owner.Opacity = 1;
            _owner.IsEnabled = true;
            _child.Children.Clear();
            Cancel?.Invoke(sender, e);
        }  

        
        private void cboDuration_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int duration = 1;
            Int32.TryParse((string)cboDuration.SelectedItem, out duration);
            lblPrice.Content = Shop.MonthPaymentLease(_shop.Лизинг, duration);
            lblFirstPrice.Content = Shop.MonthPaymentLease(_shop.Лизинг, duration);
        }
    }
}
