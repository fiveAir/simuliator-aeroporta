﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjectAirline.UserInterface.Controls.RegularityFlight
{
    /// <summary>
    /// Логика взаимодействия для RegFlightControl.xaml
    /// </summary>
    public partial class RegFlightControl : UserControl
    {
        public RegFlightControl(DateTime currentDateTime, DateTime maxDateTime)
        {
            InitializeComponent();
            lblHelp.Content = "Для назначения даты вылета используйте Ctrl,\n" +
                              "чтобы выделить несколько дней, и Shift, чтобы\n" +
                              "выделить несколько дней подряд.";
            InitializeComboBox(cboHour, 24);
            InitializeComboBox(cboMinute, 60);
            InitializeDateTime(currentDateTime, maxDateTime);
            calendar.SelectionMode = CalendarSelectionMode.MultipleRange;
        }
        
        public void InitializeDateTime(DateTime startDateTime, DateTime endDateTime)
        {
            calendar.DisplayDateStart = startDateTime;
            calendar.DisplayDateEnd = endDateTime;
            cboHour.SelectedIndex = startDateTime.Hour;
            cboMinute.SelectedIndex = startDateTime.Minute;
        }

        private void InitializeComboBox(ComboBox cbo, int maxComponent)
        {
            cbo.Items.Clear();
            for (int i = 0; i < maxComponent; i++)
            {
                string str = i.ToString();
                if (str.Length == 1)
                    cbo.Items.Add("0" + str);
                else
                    cbo.Items.Add(str);
            }
        }
        
        public List<DateTime> GetSelectedDateTime()
        {
            List<DateTime> listDateTime = new List<DateTime>();
            foreach(DateTime date in calendar.SelectedDates)
            {   
                DateTime tempDate = new DateTime(date.Year, date.Month, date.Day, Int32.Parse(cboHour.Text), Int32.Parse(cboMinute.Text), 0);
                listDateTime.Add(tempDate);
            }
            listDateTime.Sort();
            return listDateTime;
        }

    }
}
