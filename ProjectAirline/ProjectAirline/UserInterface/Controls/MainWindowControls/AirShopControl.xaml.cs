﻿    using ProjectAirline.UserInterface.Controls.AirListView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ProjectAirline.ORM;
using ProjectAirline.Tools.DataBaseTools;
using System.Threading;
using ProjectAirline.UserInterface.Controls.MessageBoxControls;

namespace ProjectAirline.UserInterface.Controls
{
    /// <summary>
    /// Логика взаимодействия для AirShopControl.xaml
    /// </summary>
    public partial class AirShopControl : UserControl
    {
        private ViewAirList _viewAirList;   //Список Самолетов

        public AirShopControl()
        {
            InitializeComponent();
            _viewAirList = new ViewAirList("Магазин самолетов", false, UpdateBody);
            _viewAirList.SetPlaceholderText("Магазин пуст");
            grdShopControl.Children.Add(_viewAirList);
        }

        public void UpdateBody()
        {
            _viewAirList.Clear();
            grdEventShopControl.Children.Clear();
            grdShopControl.Opacity = 1;
            grdShopControl.IsEnabled = true;
            List<Магазин> listShop = Shop.GetShopLines();
            if(listShop != null)
            {
                AddMyAirplane(listShop);
            }
        }

        public void AddMyAirplane(List<Магазин> listShop)
        {
            foreach (Магазин shop in listShop)
            {
                AddMyAirplane(shop);
            }
        }

        /// <summary>
        /// Добавление самолета 
        /// </summary>
        /// <param name="airplane">Самолет</param>
        public void AddMyAirplane(Магазин shop)
        {
            ItemAirList itemAirplane = new ItemAirList(shop);
            //<Добавление кнопки>
            if (shop.Покупка != null)
                itemAirplane.AddButton("Купить\n" + shop.Покупка.СтоимостьСамолета.ToString() + " ©", BuyPlaneEventHandler, itemAirplane, 80);
            else
                itemAirplane.AddButton("", BuyPlaneEventHandler, itemAirplane, 80, true);
            if (shop.Аренда != null)
                itemAirplane.AddButton("Аренда\n" + shop.Аренда.СтоимостьАрендыМес.ToString() + " ©/мес", RentPlaneEventHandler, itemAirplane, 80);
            else
                itemAirplane.AddButton("", RentPlaneEventHandler, itemAirplane, 80, true);
            if (shop.Лизинг != null)
                itemAirplane.AddButton("Лизинг\n" + shop.Лизинг.ЦенаСамолета.ToString() + " ©\n" + Math.Round(shop.Лизинг.ПроцентнаяСтавкаГод*100, 2) + "%", LeasingPlaneEventHandler, itemAirplane, 80);
            else
                itemAirplane.AddButton("", LeasingPlaneEventHandler, itemAirplane, 80, true);
            //</Добавление кнопки>
            _viewAirList.AddItem(itemAirplane);
        }


        ItemAirList shopItemAirList;
        Магазин shop;

        /// <summary>
        /// Обработка покупки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BuyPlaneEventHandler(Object sender, EventArgs e)
        {
            shopItemAirList = (sender as ButtonItem).RowItemAirList as ItemAirList;
            shop = shopItemAirList.Item as Магазин;
            try
            {
                Shop.BuyAircraft(shop);
                _viewAirList.Remove(shopItemAirList);
            }
            catch (Exception ex)
            {
                MessageBoxControl msbControl = new MessageBoxControl(ex.Message, grdShopControl, grdEventShopControl);
                msbControl.Ok += MsgBoxControl_Ok;
                msbControl.ShowDialog();
            }
        }

        private void MsgBoxControl_Ok(object sender, EventArgs e)
        {
            UpdateBody();
        }

        /// <summary>
        /// Обработка аренды
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RentPlaneEventHandler(Object sender, EventArgs e)
        {
            shopItemAirList = (sender as ButtonItem).RowItemAirList as ItemAirList;
            shop = shopItemAirList.Item as Магазин;
            try
            {
                Shop.RentAircraft(shop);
                _viewAirList.Remove(shopItemAirList);
            }
            catch (Exception ex)
            {
                MessageBoxControl msbControl = new MessageBoxControl(ex.Message, grdShopControl, grdEventShopControl);
                msbControl.Ok += MsgBoxControl_Ok;
                msbControl.ShowDialog();
            }
        }

        /// <summary>
        /// Обработка лизинга
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LeasingPlaneEventHandler(Object sender, EventArgs e)
        {
            try
            {
                shopItemAirList = (sender as ButtonItem).RowItemAirList as ItemAirList;
                shop = shopItemAirList.Item as Магазин;
                LeaseDurationShopControl ldShopControl = new LeaseDurationShopControl(shop, grdShopControl, grdEventShopControl);
                ldShopControl.Accept += LdShopControl_Accept;
                ldShopControl.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxControl msbControl = new MessageBoxControl(ex.Message, grdShopControl, grdEventShopControl);
                msbControl.Ok += MsgBoxControl_Ok;
                msbControl.ShowDialog();
            }
        }

        private void LdShopControl_Accept(object sender, EventArgs e)
        {
            _viewAirList.Remove(shopItemAirList);
        }
    }
}
