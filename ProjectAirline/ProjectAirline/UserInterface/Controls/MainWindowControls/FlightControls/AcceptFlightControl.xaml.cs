﻿using ProjectAirline.ORM;
using ProjectAirline.Tools.DataBaseTools;
using ProjectAirline.UserInterface.Controls.MessageBoxControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjectAirline.UserInterface.Controls.MainWindowControls.FlightControls
{
    /// <summary>
    /// Логика взаимодействия для AcceptFlightControl.xaml
    /// </summary>
    public partial class AcceptFlightControl : UserControl
    {
        private Grid _owner;
        private Grid _child;
        private РейсИгрыИгрока _flight;

        public AcceptFlightControl(РейсИгрыИгрока flight, Grid owner, Grid child)
        {
            InitializeComponent();
            _owner = owner;
            _child = child;
            _flight = flight;
            long range =  flight.РегулярныйРейс1.Регулярность.Диапазон;
            calendar.DisplayDateStart = Time.CurrentTime + TimeSpan.FromDays(range * 3);
            calendar.DisplayDateEnd = Time.CurrentTime + TimeSpan.FromDays(182);
            lblReg.Content = _flight.РегулярныйРейс1.Регулярность.КоличествоПовторов + " раз(а) в " +
                    _flight.РегулярныйРейс1.Регулярность.Диапазон + " дней";
        }

        public void ShowDialog()
        {
            _owner.Opacity = 0.5;
            _owner.IsEnabled = false;
            _child.Children.Add(this);
        }

        public event EventHandler Cancel;
        public event EventHandler Accept;

        private void butYes_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxControl msgBoxControl;
            try
            {
                if (calendar.SelectedDate != null)
                {
                    Flight.TakeFlight(_flight, calendar.SelectedDate.Value);
                }
                else
                {
                    msgBoxControl = new MessageBoxControl("Выберите дату", grdBox, grdChildBox);
                    msgBoxControl.ShowDialog();
                    return;
                }
            }
            catch (Exception ex)
            {
                msgBoxControl = new MessageBoxControl(ex.Message, grdBox, grdChildBox);
                msgBoxControl.ShowDialog();
                return;
            }
            _owner.Opacity = 1;
            _owner.IsEnabled = true;
            _child.Children.Clear();
            Accept?.Invoke(sender, e);
        }
        
        private void butNo_Click(object sender, RoutedEventArgs e)
        {
            _owner.Opacity = 1;
            _owner.IsEnabled = true;
            _child.Children.Clear();
            Cancel?.Invoke(sender, e);
        }
    }
}
