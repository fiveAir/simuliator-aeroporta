﻿using ProjectAirline.ORM;
using ProjectAirline.Tools.DataBaseTools;
using ProjectAirline.UserInterface.Controls.AirListView;
using ProjectAirline.UserInterface.Controls.MessageBoxControls;
using ProjectAirline.UserInterface.Controls.RegularityFlight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjectAirline.UserInterface.Controls
{
    /// <summary>
    /// Логика взаимодействия для ChangeTimetable.xaml
    /// </summary>
    public partial class ChangeTimetable : UserControl
    {
        public enum TypeChange
        {
            Change, Add
        }

        private RegFlightControl regFlightControl;
        private ItemAirList rowAirplane;
        private ItemAirList rowFlight;
        private РейсИгрыИгрока _flight;
        private СамолетИгрока _airplane;

        public ChangeTimetable(String nameControl, TypeChange typeChange)
        {
            InitializeComponent();
            Initialize(nameControl, typeChange);
        }

        public ChangeTimetable(String nameControl, TypeChange typeChange, СамолетИгрока airplane)
        {
            InitializeComponent();
            Initialize(nameControl, typeChange);
            _airplane = airplane;
            InitializeRowAirplane(airplane);
        }

        public ChangeTimetable(String nameControl, TypeChange typeChange, РейсИгрыИгрока flight)
        {
            InitializeComponent();
            Initialize(nameControl, typeChange);
            _flight = flight;
            InitializeRowFlight(flight);
        }

        private void Initialize(String nameControl, TypeChange typeChange)
        {
            regFlightControl = new RegFlightControl(Time.CurrentTime, Time.CurrentTime + TimeSpan.FromDays(30));
            grdDateTime.Children.Add(regFlightControl);
            lblNameControl.Content = nameControl;
            switch (typeChange)
            {
                case TypeChange.Change:
                    butAddChange.Content = "Изменить";
                    break;
                case TypeChange.Add:
                    butAddChange.Content = "Добавить";
                    butAddChange.Width = 100;
                    break;
            }
        }

        private void InitializeRowAirplane(СамолетИгрока airplane)
        {
            rowAirplane = new ItemAirList(airplane);
            _airplane = airplane;
           // if(_airplane.)
            rowAirplane.AddButton("Убрать", RemoveSelectedAirplane, rowAirplane);
            grdAirplane.Children.Add(rowAirplane);
            butAddAirplane.Visibility = Visibility.Hidden;
        }

        private void RemoveSelectedAirplane(Object sender, EventArgs e)
        {
            rowAirplane.Visibility = Visibility.Hidden;
            butAddAirplane.Visibility = Visibility.Visible;
            _airplane = null;
        }

        private void InitializeRowFlight(РейсИгрыИгрока flight)
        {
            regFlightControl.InitializeDateTime(Time.CurrentTime, DateTime.Parse(flight.ДоговорНаРейс.ДатаОкончания));
            rowFlight = new ItemAirList(flight);
            _flight = flight;
            rowFlight.AddButton("Убрать", RemoveSelectedFlight, rowFlight);
            grdFlight.Children.Add(rowFlight);
            butAddFlight.Visibility = Visibility.Hidden;
        }

        private void RemoveSelectedFlight(Object sender, EventArgs e)
        {
            rowFlight.Visibility = Visibility.Hidden;
            butAddFlight.Visibility = Visibility.Visible;
            _flight = null;
        }

        //событие отмены
        public event EventHandler Cancel;
        //событие добавление/изменения
        public event EventHandler AddChange;

        //нажатие кнопки отмены
        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            Cancel?.Invoke(sender, e);
        }

        //нажатие кнопки добавить/изменить
        private void ButtonAddChange_Click(object sender, RoutedEventArgs e)
        {
            List<DateTime> listDateTime = regFlightControl.GetSelectedDateTime();
            if (_airplane == null)
            {
                MessageBoxControl msgBoxControl = new MessageBoxControl("Выберите самолет!", grdAddRowFlight, grdEventAddRowFlight);
                msgBoxControl.ShowDialog();
                return;
            }
            if (_flight == null)
            {
                MessageBoxControl msgBoxControl = new MessageBoxControl("Выберите рейс!", grdAddRowFlight, grdEventAddRowFlight);
                msgBoxControl.ShowDialog();
                return;
            }
            if (listDateTime.Count == 0)
            {
                MessageBoxControl msgBoxControl = new MessageBoxControl("Выберите дату вылетов!", grdAddRowFlight, grdEventAddRowFlight);
                msgBoxControl.ShowDialog();
                return;
            }
            if (listDateTime[0] <= Time.CurrentTime)
            {
                MessageBoxControl msgBoxControl = new MessageBoxControl("Некорректная дата вылетов!", grdAddRowFlight, grdEventAddRowFlight);
                msgBoxControl.ShowDialog();
                return;
            }
            if (listDateTime[listDateTime.Count - 1] > DateTime.Parse(_flight.ДоговорНаРейс.ДатаОкончания))
            {
                MessageBoxControl msgBoxControl = new MessageBoxControl("Некорректная дата вылетов!", grdAddRowFlight, grdEventAddRowFlight);
                msgBoxControl.ShowDialog();
                return;
            }
            try
            {
                Aircraft.AircraftToFlight(_airplane.СамолетИгрыИгрока, _flight);
            }
            catch(Exception ex)
            {
                MessageBoxControl msgBoxControl = new MessageBoxControl(ex.Message, grdAddRowFlight, grdEventAddRowFlight);
                msgBoxControl.ShowDialog();
                return;
            }
            if (_flight.РазовыйРейс != null && listDateTime.Count != 1)
            {
                MessageBoxControl msgBoxControl = new MessageBoxControl("Для разового рейса можно\n" +
                    "указать только один вылет", grdAddRowFlight, grdEventAddRowFlight);
                msgBoxControl.ShowDialog();
                return;
            }
            foreach (DateTime date in listDateTime)
            {
                TimeTable.AddTimeTableRecord(_flight.ДоговорНаРейс, _airplane.СамолетИгрыИгрока, date);
            }
            AddChange?.Invoke(sender, e);
            grdEventAddRowFlight.Children.Clear();
        }

        private ChoiceAirplaneFlight choiseAirplaneFlight;

        //добавлние рейса
        private void ButtonAddFlight_Click(object sender, RoutedEventArgs e)
        {
            List<РейсИгрыИгрока> listFlight = new List<РейсИгрыИгрока>();
            var filter = ChoiceAirplaneFlight.FlightFilter.NoFilter;
            if (_airplane == null)
            {
                listFlight = Flight.GetSelectedFlights();
            }
            else
            {
                if(_airplane.СамолетИгрыИгрока.МодельСамолета.ГрузовойСамолет != null)
                {
                    listFlight = Flight.GetSelectedCargoFlights();
                    filter = ChoiceAirplaneFlight.FlightFilter.Cargo;
                }
                else
                {
                    listFlight = Flight.GetSelectedPassengerFlights();
                    filter = ChoiceAirplaneFlight.FlightFilter.Passenger;
                }
            }
            choiseAirplaneFlight = new ChoiceAirplaneFlight("Выберите рейс", listFlight, filter);
            choiseAirplaneFlight.ItemSelected += FlightSelected;
            choiseAirplaneFlight.Cancel += ChoiseAirplaneFlight_Cancel;
            grdEventAddRowFlight.Children.Add(choiseAirplaneFlight);            
        }

        private void FlightSelected(Object sender, EventArgs e)
        {
            _flight = (((sender as ButtonItem).RowItemAirList).Item as РейсИгрыИгрока);
            InitializeRowFlight(_flight);
            grdEventAddRowFlight.Children.Clear();
        }

        //добавление Самолета
        private void ButtonAddAirplane_Click(object sender, RoutedEventArgs e)
        {
            List<СамолетИгрока> listAirplane = new List<СамолетИгрока>();
            var filter = ChoiceAirplaneFlight.AirplaneFilter.NoFilter;
            if (_flight == null)
            {
                listAirplane = Aircraft.GetAircrafts();
            }
            else
            {
                if (_flight.ГрузовойРейс != null)
                {
                    listAirplane = Aircraft.GetCargoAircrafts();
                    filter = ChoiceAirplaneFlight.AirplaneFilter.Cargo;
                }
                else
                {
                    listAirplane = Aircraft.GetPassengerAircrafts();
                    filter = ChoiceAirplaneFlight.AirplaneFilter.Passenger;
                }
            }
            choiseAirplaneFlight = new ChoiceAirplaneFlight("Выберите самолет", listAirplane, filter);
            choiseAirplaneFlight.ItemSelected += AirplaneSelected;
            choiseAirplaneFlight.Cancel += ChoiseAirplaneFlight_Cancel;
            grdEventAddRowFlight.Children.Add(choiseAirplaneFlight);
        }

        private void AirplaneSelected(Object sender, EventArgs e)
        {
            _airplane = (((sender as ButtonItem).RowItemAirList).Item as СамолетИгрока);
            InitializeRowAirplane(_airplane);
            grdEventAddRowFlight.Children.Clear();
        }

        private void ChoiseAirplaneFlight_Cancel(object sender, EventArgs e)
        {
            grdEventAddRowFlight.Children.Clear();
        }

    }
}
