﻿using ProjectAirline.ORM;
using ProjectAirline.Tools.DataBaseTools;
using ProjectAirline.UserInterface.Controls.AirListView;
using ProjectAirline.UserInterface.Controls.MessageBoxControls;
using ProjectAirline.UserInterface.Controls.RegularityFlight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjectAirline.UserInterface.Controls.MainWindowControls.TaimtableControls
{
    /// <summary>
    /// Логика взаимодействия для AddFlyControl.xaml
    /// </summary>
    public partial class AddFlyControl : UserControl
    {
        public enum TypeChange
        {
            Change, Add
        }

        private RegFlightControl regFlightControl;
        private ItemAirList rowAirplane;
        private ItemAirList rowFlyFrom;
        private ItemAirList rowFlyTo;
        private Аэропорт  _flyFrom;
        private Аэропорт _flyTo;
        private СамолетИгрока _airplane;

        public AddFlyControl(String nameControl, TypeChange typeChange)
        {
            InitializeComponent();
            Initialize(nameControl, typeChange);
        }

        public AddFlyControl(String nameControl, TypeChange typeChange, СамолетИгрока airplane)
        {
            InitializeComponent();
            Initialize(nameControl, typeChange);
            _airplane = airplane;
            InitializeRowAirplane(airplane);
        }

        private void Initialize(String nameControl, TypeChange typeChange)
        {
            regFlightControl = new RegFlightControl(Time.CurrentTime, Time.CurrentTime + TimeSpan.FromDays(30));
            grdDateTime.Children.Add(regFlightControl);
            lblNameControl.Content = nameControl;
            switch (typeChange)
            {
                case TypeChange.Change:
                    butAddChange.Content = "Изменить";
                    break;
                case TypeChange.Add:
                    butAddChange.Content = "Добавить";
                    break;
            }
        }

        private void InitializeRowAirplane(СамолетИгрока airplane)
        {
            //if(_flyFrom == null)
            //{
            //    grdFlyFrom.Children.Remove(rowFlyFrom);
            //    _flyFrom = _airplane.Аэропорт;
            //    InitializeRowFlyFrom(_flyFrom);
            //}
            rowAirplane = new ItemAirList(airplane);
            _airplane = airplane;
            rowAirplane.AddButton("Убрать", RemoveSelectedAirplane, rowAirplane);
            grdAirplane.Children.Add(rowAirplane);
            butAddAirplane.Visibility = Visibility.Hidden;
        }

        private void RemoveSelectedAirplane(Object sender, EventArgs e)
        {
            rowAirplane.Visibility = Visibility.Hidden;
            butAddAirplane.Visibility = Visibility.Visible;
            _airplane = null;
        }

        //вылет
        private void InitializeRowFlyFrom(Аэропорт flyFrom)
        {
            _flyFrom = flyFrom;
            rowFlyFrom = new ItemAirList(flyFrom);           
            rowFlyFrom.AddButton("Убрать", RemoveSelectedFlyFrom, rowFlyFrom);
            grdFlyFrom.Children.Add(rowFlyFrom);
            butAddFrom.Visibility = Visibility.Hidden;
        }

        private void RemoveSelectedFlyFrom(Object sender, EventArgs e)
        {
            grdFlyFrom.Children.Remove(rowFlyFrom);
            butAddFrom.Visibility = Visibility.Visible;
            _flyFrom = null;
        }

        //прилет
        private void InitializeRowFlyTo(Аэропорт flyTo)
        {
            _flyTo = flyTo;
            rowFlyTo = new ItemAirList(flyTo);
            rowFlyTo.AddButton("Убрать", RemoveSelectedFlyTo, rowFlyTo);
            grdFlyTo.Children.Add(rowFlyTo);
            butAddTo.Visibility = Visibility.Hidden;
        }

        private void RemoveSelectedFlyTo(Object sender, EventArgs e)
        {
            grdFlyTo.Children.Remove(rowFlyTo);
            butAddTo.Visibility = Visibility.Visible;
            _flyTo = null;
        }

        //событие отмены
        public event EventHandler Cancel;
        //событие добавление/изменения
        public event EventHandler AddChange;

        //нажатие кнопки отмены
        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            Cancel?.Invoke(sender, e);
        }

        private void ButtonAddChange_Click(object sender, RoutedEventArgs e)
        {
            List<DateTime> listDateTime = regFlightControl.GetSelectedDateTime();
            if (_airplane == null)
            {
                MessageBoxControl msgBoxControl = new MessageBoxControl("Выберите самолет!", grdAddRowFly, grdEventAddRowFly);
                msgBoxControl.ShowDialog();
                return;
            }
            if (_flyFrom == null)
            {
                MessageBoxControl msgBoxControl = new MessageBoxControl("Выберите аэропорт отправления!", grdAddRowFly, grdEventAddRowFly);
                msgBoxControl.ShowDialog();
                return;
            }
            if (_flyTo == null)
            {
                MessageBoxControl msgBoxControl = new MessageBoxControl("Выберите аэропорт прибытия!", grdAddRowFly, grdEventAddRowFly);
                msgBoxControl.ShowDialog();
                return;
            }
            if (listDateTime.Count == 0)
            {
                MessageBoxControl msgBoxControl = new MessageBoxControl("Выберите дату вылетов!", grdAddRowFly, grdEventAddRowFly);
                msgBoxControl.ShowDialog();
                return;
            }
            if (listDateTime[0] <= Time.CurrentTime)
            {
                MessageBoxControl msgBoxControl = new MessageBoxControl("Некорректная дата вылетов!", grdAddRowFly, grdEventAddRowFly);
                msgBoxControl.ShowDialog();
                return;
            }
            if(_flyTo == _flyFrom)
            {
                MessageBoxControl msgBoxControl = new MessageBoxControl("Самолет уже в данном аэропорте!", grdAddRowFly, grdEventAddRowFly);
                msgBoxControl.ShowDialog();
                return;
            }
            try
            {
                Aircraft.AircraftToFlight(_airplane.СамолетИгрыИгрока, _flyFrom, _flyTo);
            }
            catch (Exception ex)
            {
                MessageBoxControl msgBoxControl = new MessageBoxControl(ex.Message, grdAddRowFly, grdEventAddRowFly);
                msgBoxControl.ShowDialog();
                return;
            }
            
            foreach (DateTime date in listDateTime)
            {
                Flight.AddEmptyFly(_flyFrom, _flyTo, _airplane.СамолетИгрыИгрока, date);
            }
            AddChange?.Invoke(sender, e);
            grdEventAddRowFly.Children.Clear();
        }

        private ChoiceAirplaneFlight choiseFlyAirplane;

        //добавлние места отправления
        private void ButtonAddFrom_Click(object sender, RoutedEventArgs e)
        {
            List<Аэропорт> listFlyFrom = new List<Аэропорт>();
            listFlyFrom = Airport.GetAllAirports();
            choiseFlyAirplane = new ChoiceAirplaneFlight("Выберите место отправления", listFlyFrom);
            choiseFlyAirplane.ItemSelected += FlyFromSelected;
            choiseFlyAirplane.Cancel += ChoiseFlyAirplane_Cancel;
            grdEventAddRowFly.Children.Add(choiseFlyAirplane);
        }

        private void FlyFromSelected(Object sender, EventArgs e)
        {
            _flyFrom = (((sender as ButtonItem).RowItemAirList).Item as Аэропорт);
            InitializeRowFlyFrom(_flyFrom);
            grdEventAddRowFly.Children.Clear();
        }

        //добавлние места прибытия
        private void ButtonAddTo_Click(object sender, RoutedEventArgs e)
        {
            List<Аэропорт> listFlyTo = new List<Аэропорт>();
            listFlyTo = Airport.GetAllAirports();
            choiseFlyAirplane = new ChoiceAirplaneFlight("Выберите место прибытия", listFlyTo);
            choiseFlyAirplane.ItemSelected += FlyToSelected;
            choiseFlyAirplane.Cancel += ChoiseFlyAirplane_Cancel;
            grdEventAddRowFly.Children.Add(choiseFlyAirplane);
        }

        private void FlyToSelected(Object sender, EventArgs e)
        {
            _flyTo = (((sender as ButtonItem).RowItemAirList).Item as Аэропорт);
            InitializeRowFlyTo(_flyTo);
            grdEventAddRowFly.Children.Clear();
        }

        //добавление Самолета
        private void ButtonAddAirplane_Click(object sender, RoutedEventArgs e)
        {
            List<СамолетИгрока> listAirplane = new List<СамолетИгрока>();
            listAirplane = Aircraft.GetAircrafts(); 
            choiseFlyAirplane = new ChoiceAirplaneFlight("Выберите самолет", listAirplane);
            choiseFlyAirplane.ItemSelected += AirplaneSelected;
            choiseFlyAirplane.Cancel += ChoiseFlyAirplane_Cancel;
            grdEventAddRowFly.Children.Add(choiseFlyAirplane);
        }

        private void AirplaneSelected(Object sender, EventArgs e)
        {
            _airplane = (((sender as ButtonItem).RowItemAirList).Item as СамолетИгрока);
            InitializeRowAirplane(_airplane);
            grdEventAddRowFly.Children.Clear();
        }

        private void ChoiseFlyAirplane_Cancel(object sender, EventArgs e)
        {
            grdEventAddRowFly.Children.Clear();
        }
    }
}
