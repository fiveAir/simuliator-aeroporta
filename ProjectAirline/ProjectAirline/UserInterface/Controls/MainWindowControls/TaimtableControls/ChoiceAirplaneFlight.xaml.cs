﻿using ProjectAirline.ORM;
using ProjectAirline.UserInterface.Controls.AirListView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjectAirline.UserInterface
{
    /// <summary>
    /// Логика взаимодействия для ChoiceAirplaneFlight.xaml
    /// </summary>
    public partial class ChoiceAirplaneFlight : UserControl
    {
        private ViewAirList _choiseAirList;

        public enum AirplaneFilter
        {
            NoFilter = -1,
            Cargo,
            Passenger
        }

        public enum FlightFilter
        {
            NoFilter = -1,
            Cargo,
            Passenger
        }

        public ChoiceAirplaneFlight(string nameTabel, List<СамолетИгрока> listAirplane, AirplaneFilter UsedFilter = AirplaneFilter.NoFilter)
        {
            InitializeComponent();
            _choiseAirList = new ViewAirList(nameTabel);
            _choiseAirList.AddButtonRow("Выбрать", ChoiseItem);
            _choiseAirList.AddItems(listAirplane);
            grdChoise.Children.Add(_choiseAirList);
            switch (UsedFilter)
            {
                case AirplaneFilter.Cargo:
                    _choiseAirList.SetPlaceholderText("Список грузовых самолетов пуст");
                    break;
                case AirplaneFilter.Passenger:
                    _choiseAirList.SetPlaceholderText("Список пассажирских самолетов пуст");
                    break;
                default:
                    _choiseAirList.SetPlaceholderText("Список самолетов пуст");
                    break;
            }
        }

        public ChoiceAirplaneFlight(string nameTabel, List<РейсИгрыИгрока> listFlight, FlightFilter UsedFilter = FlightFilter.NoFilter)
        {
            InitializeComponent();
            _choiseAirList = new ViewAirList(nameTabel);
            _choiseAirList.AddButtonRow("Выбрать", ChoiseItem);
            _choiseAirList.AddItems(listFlight);
            grdChoise.Children.Add(_choiseAirList);
            switch (UsedFilter)
            {
                case FlightFilter.Cargo:
                    _choiseAirList.SetPlaceholderText("Список грузовых рейсов пуст");
                    break;
                case FlightFilter.Passenger:
                    _choiseAirList.SetPlaceholderText("Список пассажирских рейсов пуст");
                    break;
                default:
                    _choiseAirList.SetPlaceholderText("Список рейсов пуст");
                    break;
            }
        }

        public ChoiceAirplaneFlight(string nameTabel, List<Аэропорт> listFlyToFrom)
        {
            InitializeComponent();
            _choiseAirList = new ViewAirList(nameTabel);
            _choiseAirList.AddButtonRow("Выбрать", ChoiseItem);
            _choiseAirList.AddItems(listFlyToFrom);
            grdChoise.Children.Add(_choiseAirList);
        }

        public event EventHandler ItemSelected;
        public event EventHandler Cancel;

        private void ChoiseItem(Object sender, EventArgs e)
        {
            ItemSelected?.Invoke(sender, e);
        }

        private void butCancel_Click(object sender, RoutedEventArgs e)
        {
            Cancel?.Invoke(sender, e);
        }
    }
}
