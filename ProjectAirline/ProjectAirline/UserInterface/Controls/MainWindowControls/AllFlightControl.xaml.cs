﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ProjectAirline.UserInterface.Controls.AirListView;
using System.Drawing;
using ProjectAirline.ORM;
using ProjectAirline.Tools.DataBaseTools;
using ProjectAirline.UserInterface.Controls.MainWindowControls.FlightControls;
using ProjectAirline.UserInterface.Controls.MessageBoxControls;

namespace ProjectAirline.UserInterface.Controls
{
    /// <summary>
    /// Логика взаимодействия для AllFlightControl.xaml
    /// </summary>
    public partial class AllFlightControl : UserControl
    {
        private ViewAirList _viewAirList;   //Список Рейсов

        public AllFlightControl()
        {
            InitializeComponent();
            _viewAirList = new ViewAirList("Рейсы", false, UpdateBody);
            _viewAirList.AddButtonRow("Взять", TakingFlightEventHandler);
            grdAllFlightControl.Children.Add(_viewAirList);
            UpdateBody();            
        }

        public void UpdateBody()
        {
            _viewAirList.Clear();
            grdEventAllFlightControl.Children.Clear();
            grdAllFlightControl.Opacity = 1;
            grdAllFlightControl.IsEnabled = true;
            List<РейсИгрыИгрока> listFlight = Flight.GetPossibleFlights();
            if (listFlight != null)
            {
                _viewAirList.AddItems(listFlight);
            }
        }

        ItemAirList itemAirList;
        РейсИгрыИгрока flight;

        /// <summary>
        /// Обработчик кнопки "взятие рейса"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TakingFlightEventHandler(Object sender, EventArgs e)
        {
            itemAirList = (sender as ButtonItem).RowItemAirList as ItemAirList;
            flight = itemAirList.Item as РейсИгрыИгрока;
            if (flight.РазовыйРейс != null)
            {
                AcceptMessageboxControl msgBox = new AcceptMessageboxControl("Вы действительно хотите взять этот рейс?",
                    "Вы должны выполнить его до: " + flight.РазовыйРейс.ДатаВыполнения, grdAllFlightControl, grdEventAllFlightControl);
                msgBox.Accept += MsgBox_Accept;
                msgBox.ShowDialog();
            }
            else
            {
                AcceptFlightControl acceptFlightControl = new AcceptFlightControl(flight, grdAllFlightControl, grdEventAllFlightControl);
                acceptFlightControl.Accept += LdShopControl_Accept;
                acceptFlightControl.ShowDialog();
            }
        }

        private void MsgBox_Accept(object sender, EventArgs e)
        {
            Flight.TakeFlight(flight);
            LdShopControl_Accept(sender, e);
        }

        private void LdShopControl_Accept(object sender, EventArgs e)
        {
            _viewAirList.Remove(itemAirList);
        }

    }
}
