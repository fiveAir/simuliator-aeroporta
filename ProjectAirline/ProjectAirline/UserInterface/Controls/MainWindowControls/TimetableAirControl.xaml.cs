﻿using ProjectAirline.ORM;
using ProjectAirline.Tools.DataBaseTools;
using ProjectAirline.UserInterface.Controls.AirListView;
using ProjectAirline.UserInterface.Controls.MainWindowControls.TaimtableControls;
using ProjectAirline.UserInterface.Controls.MessageBoxControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProjectAirline.UserInterface.Controls
{
    /// <summary>
    /// Логика взаимодействия для TimetableAirControl.xaml
    /// </summary>
    public partial class TimetableAirControl : UserControl
    {
        private ViewAirList _viewAirList;
        public TimetableAirControl()
        {
            InitializeComponent();
            _viewAirList = new ViewAirList("Расписание рейсов", false, UpdateBody);
            _viewAirList.AddButtonRow("Удалить", RemoveRowTimetable);
            grdTimetableControlMain.Children.Add(_viewAirList);
        }

        public void UpdateBody()
        {
            _viewAirList.Clear();
            grdEventTimetableControl.Children.Clear();
            grdTimetableControl.Visibility = Visibility.Visible;
            grdTimetableControl.Opacity = 1;
            grdTimetableControl.IsEnabled = true;
            List<Расписание> listTimetable = TimeTable.GetTimeTableRecords();
            if (listTimetable != null)
            {
                _viewAirList.AddItems(listTimetable);
            }
        }

        ItemAirList itemAirList;
        Расписание timetable;

        public void RemoveRowTimetable(Object sender, EventArgs e)
        {
            itemAirList = (sender as ButtonItem).RowItemAirList as ItemAirList;
            timetable = itemAirList.Item as Расписание;
            if (timetable.ДоговорНаРейс == null)
            {
                MessageBoxControl msgBoxControl = new MessageBoxControl("Рейс уже выполнен, обновитесь!", grdTimetableControl, grdEventTimetableControl);
                msgBoxControl.Ok += MsgBoxControl_Ok; ;
                msgBoxControl.ShowDialog();
            }
            else
            {
                AcceptMessageboxControl amsbControl = new AcceptMessageboxControl("Вы действительно хотите отменить полет?", "",
                grdTimetableControl, grdEventTimetableControl);
                amsbControl.Accept += AmsbControl_Accept;
                amsbControl.ShowDialog();
            }
        }

        private void MsgBoxControl_Ok(object sender, EventArgs e)
        {
            UpdateBody();
        }

        private void AmsbControl_Accept(object sender, EventArgs e)
        {
            TimeTable.RemoveTimeTableRecord(timetable);
            _viewAirList.Remove(itemAirList);
        }

        private void butAddFly_Click(object sender, RoutedEventArgs e)
        {
            AddFlyControl addFlyControl = new AddFlyControl("Добавление перелета", AddFlyControl.TypeChange.Add);
            addFlyControl.AddChange += AddFlyControl_AddChange;
            addFlyControl.Cancel += AddFlyControl_Cancel;
            grdEventTimetableControl.Children.Add(addFlyControl);
            grdTimetableControl.Visibility = Visibility.Hidden;
        }

        private void AddFlyControl_Cancel(object sender, EventArgs e)
        {
            grdEventTimetableControl.Children.Clear();
            grdTimetableControl.Visibility = Visibility.Visible;
        }

        private void AddFlyControl_AddChange(object sender, EventArgs e)
        {
            UpdateBody();
            grdEventTimetableControl.Children.Clear();
            grdTimetableControl.Visibility = Visibility.Visible;
        }
        
        private void butAddFlightAirplane_Click(object sender, RoutedEventArgs e)
        {
            ChangeTimetable ctrlChangeTimetable = new ChangeTimetable("Составление расписания", ChangeTimetable.TypeChange.Add);
            ctrlChangeTimetable.AddChange += CtrlChangeTimetable_AddChange; 
            ctrlChangeTimetable.Cancel += CtrlChangeTimetable_Cancel; 
            grdEventTimetableControl.Children.Add(ctrlChangeTimetable); 
            grdTimetableControl.Visibility = Visibility.Hidden;
        }

        private void CtrlChangeTimetable_Cancel(object sender, EventArgs e)
        {
            grdEventTimetableControl.Children.Clear();
            grdTimetableControl.Visibility = Visibility.Visible;
        }

        private void CtrlChangeTimetable_AddChange(object sender, EventArgs e)
        {
            UpdateBody();
            grdEventTimetableControl.Children.Clear();
            grdTimetableControl.Visibility = Visibility.Visible;
        }
    }
}
