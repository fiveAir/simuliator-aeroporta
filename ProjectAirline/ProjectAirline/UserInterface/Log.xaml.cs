﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ProjectAirline.UserInterface
{
    /// <summary>
    /// Логика взаимодействия для Log.xaml
    /// </summary>
    public partial class Log : Window
    {
        public Action<string> writeDelegate { get; }

        public Log()
        {
            writeDelegate = delegate (string message)
            {
                //Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                //    (ThreadStart)delegate
                //    {
                //        tbxLog.Text = Time.CurrentTime.ToString() + " => " + message + "\n" + tbxLog.Text;
                //    });
                this.Dispatcher.Invoke(() =>
                {
                    if ((Application.Current.MainWindow as MainWindow) != null)
                    {
                        string fullMessage = Time.CurrentTime.ToString() + " => " + message;

                        (Application.Current.MainWindow as MainWindow).AddLogData(
                            fullMessage, Palette.GetBrush(System.Drawing.Color.Black));
                    }
                });
                
            };
            //InitializeComponent();
        }
    }
}
