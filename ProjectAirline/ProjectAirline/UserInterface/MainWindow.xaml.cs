﻿using ProjectAirline.Events;
using ProjectAirline.ORM;
using ProjectAirline.Tools;
using ProjectAirline.Tools.DataBaseTools;
using ProjectAirline.UserInterface.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Threading;
using ProjectAirline.UserInterface.Controls.MessageBoxControls;

namespace ProjectAirline.UserInterface
{
    /// <summary>
    /// Логика взаимодействия для Main.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MapControl mapControl;                      // Контрол карты
        private AllFlightControl allFlightControl;          // Контрол генерируемых рейсов   
        private AirShopControl airShopControl;              // Контрол магазина
        private TimetableAirControl timetableAirControl;    // Контрол расписания
        private MyFlightControl myFlightControl;            // Контрол моих рейсов
        private MyAirplaneControl myAirplaneControl;        // Контрол моих самолетов

        private InfoUpdate infoUpdate;                      // Класс обновления времени и баланса

        public MainWindow()
        {
            InitializeComponent();

            labNameAirline.Content = Game.Player.НазваниеАвиакомпании;
            InitializeControls();
            StartInfoUpdate();
            
            Starting(btnPlayX1, null);
            grdBody.Children.Add(mapControl);

            Application.Current.MainWindow = this;
        }

        /// <summary>
        /// Инициализация контролов
        /// </summary>
        private void InitializeControls()
        {
            mapControl = new MapControl();
            allFlightControl = new AllFlightControl();
            airShopControl = new AirShopControl();
            timetableAirControl = new TimetableAirControl();
            myFlightControl = new MyFlightControl();
            myAirplaneControl = new MyAirplaneControl();
        }

        /// <summary>
        /// Обновление или изменение отображаемого контрола
        /// </summary>
        private void ChangeBody(object sender, RoutedEventArgs e)
        {
            grdBody.Children.Clear();
            switch (((Button)sender).Name)
            {
                case "btnMap":
                    grdBody.Children.Add(mapControl);
                    break;
                case "btnShop":
                    airShopControl.UpdateBody();
                    grdBody.Children.Add(airShopControl);
                    break;
                case "btnMyAirplanes":
                    myAirplaneControl.UpdateBody();
                    grdBody.Children.Add(myAirplaneControl);
                    break;
                case "btnAirfields":
                    allFlightControl.UpdateBody();
                    grdBody.Children.Add(allFlightControl);
                    break;
                case "btnMyAirfields":
                    myFlightControl.UpdateBody();
                    grdBody.Children.Add(myFlightControl);
                    break;
                case "btnTimeTable":
                    timetableAirControl.UpdateBody();
                    grdBody.Children.Add(timetableAirControl);
                    break;
            }
        }

        /// <summary>
        /// Управление ходом времени и подсветкой кнопок
        /// </summary>
        private void Starting(object sender, RoutedEventArgs e)
        {
            btnPlayX1.BorderBrush = Palette.GetBrush("#FFDDDDDD");
            btnPlayX2.BorderBrush = Palette.GetBrush("#FFDDDDDD");
            btnPlayX3.BorderBrush = Palette.GetBrush("#FFDDDDDD");
            btnPlayX4.BorderBrush = Palette.GetBrush("#FFDDDDDD");
            ((Button)sender).BorderBrush = Palette.GetBrush(Color.Red);
            switch (((Button)sender).Name)
            {
                case "btnPlayX1": Time.SpeedOfTime = 1; ILog.Write("SpeedOfTime = 1 игрСек/сек"); break;
                case "btnPlayX2": Time.SpeedOfTime = 60; ILog.Write("SpeedOfTime = 1 игрМин/сек"); break;
                case "btnPlayX3": Time.SpeedOfTime = 3600; ILog.Write("SpeedOfTime = 1 игрЧас/сек"); break;
                case "btnPlayX4": Time.SpeedOfTime = 86400; ILog.Write("SpeedOfTime = 1 игрДень/сек"); break;
            }
            //GameOver("asdasd");
        }

        /// <summary>
        /// Сохранение игры в БД
        /// </summary>
        private void Save(object sender, RoutedEventArgs e)
        {
            ILog.Write("Сохранение игры");
            Game.Save();
        }

        private void SaveAndExit(object sender, RoutedEventArgs e)
        {
            ILog.Write("Сохранение и выход из игры");
            infoUpdate.Stop();
            Close(); // Сохранение произойдёт в Window_Closing
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            ILog.Write("Закрытие окна игры");
            infoUpdate.Stop();
            Game.SaveAndExit();
        }

        /// <summary>
        /// Запуск обновления времени, баланса
        /// </summary>
        private void StartInfoUpdate()
        {
            List<Action> actions = new List<Action>();
            actions.Add(delegate() // Обновление времени
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                    (ThreadStart)delegate
                    {
                        labDateTimeGame.Content = Time.CurrentTime.ToString("dd.MM.yyyy - HH:mm:ss:fff");
                    });
            });
            actions.Add(delegate() // Обновление времени
            {
                Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                    (ThreadStart)delegate
                    {
                        try
                        {
                            labUsersMoney.Content = Math.Round(Game.Player.Баланс, 2).ToString();
                        }
                        catch
                        {

                        }
                    });
            });
            infoUpdate = new InfoUpdate(TimeSpan.FromMilliseconds(10), actions);
            infoUpdate.Start();
        }

        public void AddLogData(string text, System.Windows.Media.Brush colorText)
        {
            Label labForLog = new Label()
            {
                Content = text,
                Foreground = colorText,
                Margin = new Thickness(0, -7, 0, -7)
            };
            spBlockLog.Children.Add(labForLog);
            scrlViewer.ScrollToEnd();
            if (text.EndsWith("Конец игры!"))
            {
                GameOver("Баланс меньше нуля! Вы проиграли!");
            }
        }

        public void Warning(string text)
        {
            MessageBoxControl msgBoxControl = new MessageBoxControl(text, grdMain, grdEventMain);
            msgBoxControl.ShowDialog();
        }

        public void GameOver(string text)
        {
            MessageBoxControl msgBoxControl = new MessageBoxControl(text, grdMain, grdEventMain);
            msgBoxControl.Ok += MsgBoxControl_Ok;
            msgBoxControl.ShowDialog();
        }

        private void MsgBoxControl_Ok(object sender, EventArgs e)
        {
            Close();
        }
    }
}
