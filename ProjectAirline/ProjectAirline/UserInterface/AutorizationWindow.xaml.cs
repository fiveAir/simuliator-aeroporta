﻿using ProjectAirline.ORM;
using ProjectAirline.Tools;
using ProjectAirline.Tools.DataBaseTools;
using ProjectAirline.UserInterface.Controls.AutorizationControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProjectAirline.UserInterface
{
    /// <summary>
    /// Логика взаимодействия для AutorizationWindow.xaml
    /// </summary>
    public partial class AutorizationWindow : Window
    {
        private MainMenuControl mainMenu;
        private RegistrationControl registrationControl;
        private LoadGameControl loadPlayersControl;

        public AutorizationWindow()
        {
            InitializeComponent();

            mainMenu = new MainMenuControl();
            mainMenu.ClickNewGame += ShowRegistration;
            mainMenu.ClickLoadGame += ShowLoadGame;
            mainMenu.ClickExit += Exit;

            registrationControl = new RegistrationControl();
            registrationControl.ClickBack += ShowMainMenu;
            registrationControl.ClickStartGame += NewGame;

            loadPlayersControl = new LoadGameControl();
            loadPlayersControl.ClickBack += ShowMainMenu;
            loadPlayersControl.ClickStartGame += ContinueGame;

            grdBody.Children.Add(mainMenu);
            
            ILog.Initialize(); // Временый лог (на время разработки и отладки)
            ILog.Write("Запуск!");
        }

        private void ShowMainMenu(object sender, EventArgs e)
        {
            grdBody.Children.Clear();
            grdBody.Children.Add(mainMenu);
        }

        private void ShowLoadGame(object sender, EventArgs e)
        {
            grdBody.Children.Clear();
            grdBody.Children.Add(loadPlayersControl);
        }

        private void ShowRegistration(object sender, EventArgs e)
        {
            grdBody.Children.Clear();
            grdBody.Children.Add(registrationControl);
        }

        private void NewGame(object sender, EventArgs e)
        {
            Игрок player = new Игрок()
            {
                Баланс = 3000000,
                ИгровоеВремя = DateTime.Now.ToString(),
                НазваниеАвиакомпании = registrationControl.NameAirline,
                id_СтартовыйАэропорт = registrationControl.SelectedAirport.id
            };
            Player.CreateNewPlayer(player);
            Game.NewGame(player);
            Close();
        }

        public void ContinueGame(object sender, EventArgs e)
        {
            var player = (sender as Игрок);
            if (player.Баланс < 0)
            {
                MessageBox.Show("Начните новую игру", "Вы банкрот", MessageBoxButton.OK);
            }
            else
            {
                Game.ContinueGame(sender as Игрок);
                Close();
            }
        }

        public void Exit(object sender, EventArgs e)
        {
            ILog.Close(); // Временый лог (на время разработки и отладки)
            Close();
        }
    }
}
