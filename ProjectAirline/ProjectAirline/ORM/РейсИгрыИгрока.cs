namespace ProjectAirline.ORM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class РейсИгрыИгрока
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public РейсИгрыИгрока()
        {
            РегулярныйРейс = new HashSet<РегулярныйРейс>();
        }

        public long id { get; set; }

        public long id_Игрок { get; set; }

        public long id_Перелет { get; set; }

        public long? id_ГрузовойРейс { get; set; }

        public long? id_ПассажирскийРейс { get; set; }

        public long? id_РазовыйРейс { get; set; }

        public long? id_РегулярныйРейс { get; set; }

        public long? id_Договор { get; set; }

        public virtual ГрузовойРейс ГрузовойРейс { get; set; }

        public virtual ДоговорНаРейс ДоговорНаРейс { get; set; }

        public virtual Игрок Игрок { get; set; }

        public virtual ПассажирскийРейс ПассажирскийРейс { get; set; }

        public virtual Перелет Перелет { get; set; }

        public virtual РазовыйРейс РазовыйРейс { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<РегулярныйРейс> РегулярныйРейс { get; set; }

        public virtual РегулярныйРейс РегулярныйРейс1 { get; set; }
    }
}
