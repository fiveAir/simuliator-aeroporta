namespace ProjectAirline.ORM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ДоговорНаСамолет
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ДоговорНаСамолет()
        {
            СамолетИгрока = new HashSet<СамолетИгрока>();
        }

        public long id { get; set; }

        public long id_Авиакомпания { get; set; }

        public long? id_Покупка { get; set; }

        public long? id_Аренда { get; set; }

        public long? id_Лизинг { get; set; }

        public long? СрокЛизингаМес { get; set; }

        public long id_СтатусДоговора { get; set; }

        public virtual Авиакомпания Авиакомпания { get; set; }

        public virtual Аренда Аренда { get; set; }

        public virtual СтатусДоговора СтатусДоговора { get; set; }

        public virtual Лизинг Лизинг { get; set; }

        public virtual Покупка Покупка { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<СамолетИгрока> СамолетИгрока { get; set; }
    }
}
