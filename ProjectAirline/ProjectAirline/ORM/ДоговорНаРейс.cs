namespace ProjectAirline.ORM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ДоговорНаРейс
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ДоговорНаРейс()
        {
            Расписание = new HashSet<Расписание>();
            РейсИгрыИгрока = new HashSet<РейсИгрыИгрока>();
        }

        public long id { get; set; }

        [Required]
        [StringLength(2147483647)]
        public string ДатаНачала { get; set; }

        [Required]
        [StringLength(2147483647)]
        public string ДатаОкончания { get; set; }

        [Column(TypeName = "real")]
        public double Неустойка { get; set; }

        public long id_СтатусДоговора { get; set; }

        public long? Количество_выполненных { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Расписание> Расписание { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<РейсИгрыИгрока> РейсИгрыИгрока { get; set; }
    }
}
