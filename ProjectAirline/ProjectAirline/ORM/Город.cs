namespace ProjectAirline.ORM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Город
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Город()
        {
            Местоположение = new HashSet<Местоположение>();
        }

        public long id { get; set; }

        [Required]
        [StringLength(2147483647)]
        public string Название { get; set; }

        public long id_Страна { get; set; }

        public virtual Страна Страна { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Местоположение> Местоположение { get; set; }
    }
}
