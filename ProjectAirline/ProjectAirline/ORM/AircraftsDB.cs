﻿namespace ProjectAirline.ORM
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class AircraftsDB : DbContext
    {
        public AircraftsDB()
            : base("name=AircraftsDB")
        {
        }

        public virtual DbSet<Авиакомпания> Авиакомпания { get; set; }
        public virtual DbSet<Аренда> Аренда { get; set; }
        public virtual DbSet<Аэропорт> Аэропорт { get; set; }
        public virtual DbSet<Город> Город { get; set; }
        public virtual DbSet<ГрузовойРейс> ГрузовойРейс { get; set; }
        public virtual DbSet<ГрузовойСамолет> ГрузовойСамолет { get; set; }
        public virtual DbSet<ДоговорНаРейс> ДоговорНаРейс { get; set; }
        public virtual DbSet<ДоговорНаСамолет> ДоговорНаСамолет { get; set; }
        public virtual DbSet<Игрок> Игрок { get; set; }
        public virtual DbSet<Лизинг> Лизинг { get; set; }
        public virtual DbSet<Магазин> Магазин { get; set; }
        public virtual DbSet<Местоположение> Местоположение { get; set; }
        public virtual DbSet<МодельСамолета> МодельСамолета { get; set; }
        public virtual DbSet<ПассажирскийРейс> ПассажирскийРейс { get; set; }
        public virtual DbSet<ПассажирскийСамолет> ПассажирскийСамолет { get; set; }
        public virtual DbSet<Перелет> Перелет { get; set; }
        public virtual DbSet<Покупка> Покупка { get; set; }
        public virtual DbSet<РазовыйРейс> РазовыйРейс { get; set; }
        public virtual DbSet<Расписание> Расписание { get; set; }
        public virtual DbSet<РаспределениеПассажиров> РаспределениеПассажиров { get; set; }
        public virtual DbSet<Регулярность> Регулярность { get; set; }
        public virtual DbSet<РегулярныйРейс> РегулярныйРейс { get; set; }
        public virtual DbSet<РейсИгрыИгрока> РейсИгрыИгрока { get; set; }
        public virtual DbSet<СамолетИгрока> СамолетИгрока { get; set; }
        public virtual DbSet<СамолетИгрыИгрока> СамолетИгрыИгрока { get; set; }
        public virtual DbSet<СтатусДоговора> СтатусДоговора { get; set; }
        public virtual DbSet<Страна> Страна { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Авиакомпания>()
                .HasMany(e => e.ДоговорНаСамолет)
                .WithRequired(e => e.Авиакомпания)
                .HasForeignKey(e => e.id_Авиакомпания)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Авиакомпания>()
                .HasMany(e => e.Магазин)
                .WithRequired(e => e.Авиакомпания)
                .HasForeignKey(e => e.id_Авиакомпания)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Аренда>()
                .HasMany(e => e.ДоговорНаСамолет)
                .WithOptional(e => e.Аренда)
                .HasForeignKey(e => e.id_Аренда);

            modelBuilder.Entity<Аренда>()
                .HasMany(e => e.Магазин)
                .WithOptional(e => e.Аренда)
                .HasForeignKey(e => e.id_Аренда);

            modelBuilder.Entity<Аэропорт>()
                .HasMany(e => e.Игрок)
                .WithRequired(e => e.Аэропорт)
                .HasForeignKey(e => e.id_СтартовыйАэропорт)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Аэропорт>()
                .HasMany(e => e.Перелет)
                .WithRequired(e => e.Аэропорт)
                .HasForeignKey(e => e.id_АэропортПрибытия)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Аэропорт>()
                .HasMany(e => e.Перелет1)
                .WithRequired(e => e.Аэропорт1)
                .HasForeignKey(e => e.id_АэропортОтправления)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Аэропорт>()
                .HasMany(e => e.СамолетИгрока)
                .WithOptional(e => e.Аэропорт)
                .HasForeignKey(e => e.id_ТекущийАэропорт);

            modelBuilder.Entity<Город>()
                .HasMany(e => e.Местоположение)
                .WithRequired(e => e.Город)
                .HasForeignKey(e => e.id_Город)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ГрузовойРейс>()
                .HasMany(e => e.РейсИгрыИгрока)
                .WithOptional(e => e.ГрузовойРейс)
                .HasForeignKey(e => e.id_ГрузовойРейс);

            modelBuilder.Entity<ГрузовойСамолет>()
                .HasMany(e => e.МодельСамолета)
                .WithOptional(e => e.ГрузовойСамолет)
                .HasForeignKey(e => e.id_ГрузовойСамолет);

            modelBuilder.Entity<ДоговорНаРейс>()
                .HasMany(e => e.Расписание)
                .WithRequired(e => e.ДоговорНаРейс)
                .HasForeignKey(e => e.id_ДоговорНаРейс)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ДоговорНаРейс>()
                .HasMany(e => e.РейсИгрыИгрока)
                .WithOptional(e => e.ДоговорНаРейс)
                .HasForeignKey(e => e.id_Договор);

            modelBuilder.Entity<ДоговорНаСамолет>()
                .HasMany(e => e.СамолетИгрока)
                .WithOptional(e => e.ДоговорНаСамолет)
                .HasForeignKey(e => e.id_Договор);

            modelBuilder.Entity<Игрок>()
                .HasMany(e => e.РейсИгрыИгрока)
                .WithRequired(e => e.Игрок)
                .HasForeignKey(e => e.id_Игрок)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Игрок>()
                .HasMany(e => e.СамолетИгрока)
                .WithRequired(e => e.Игрок)
                .HasForeignKey(e => e.id_Игрок)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Игрок>()
                .HasMany(e => e.СамолетИгрыИгрока)
                .WithRequired(e => e.Игрок)
                .HasForeignKey(e => e.id_Игрок)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Лизинг>()
                .HasMany(e => e.ДоговорНаСамолет)
                .WithOptional(e => e.Лизинг)
                .HasForeignKey(e => e.id_Лизинг);

            modelBuilder.Entity<Лизинг>()
                .HasMany(e => e.Магазин)
                .WithOptional(e => e.Лизинг)
                .HasForeignKey(e => e.id_Лизинг);

            modelBuilder.Entity<Местоположение>()
                .HasMany(e => e.Аэропорт)
                .WithRequired(e => e.Местоположение)
                .HasForeignKey(e => e.id_Местоположение)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<МодельСамолета>()
                .HasMany(e => e.СамолетИгрыИгрока)
                .WithRequired(e => e.МодельСамолета)
                .HasForeignKey(e => e.id_МодельСамолета)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ПассажирскийРейс>()
                .HasMany(e => e.РейсИгрыИгрока)
                .WithOptional(e => e.ПассажирскийРейс)
                .HasForeignKey(e => e.id_ПассажирскийРейс);

            modelBuilder.Entity<ПассажирскийСамолет>()
                .HasMany(e => e.МодельСамолета)
                .WithOptional(e => e.ПассажирскийСамолет)
                .HasForeignKey(e => e.id_ПассажирскийСамолет);

            modelBuilder.Entity<Перелет>()
                .HasMany(e => e.РейсИгрыИгрока)
                .WithRequired(e => e.Перелет)
                .HasForeignKey(e => e.id_Перелет)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Покупка>()
                .HasMany(e => e.ДоговорНаСамолет)
                .WithOptional(e => e.Покупка)
                .HasForeignKey(e => e.id_Покупка);

            modelBuilder.Entity<Покупка>()
                .HasMany(e => e.Магазин)
                .WithOptional(e => e.Покупка)
                .HasForeignKey(e => e.id_Покупка);

            modelBuilder.Entity<РазовыйРейс>()
                .HasMany(e => e.РейсИгрыИгрока)
                .WithOptional(e => e.РазовыйРейс)
                .HasForeignKey(e => e.id_РазовыйРейс);

            modelBuilder.Entity<РаспределениеПассажиров>()
                .HasMany(e => e.ПассажирскийРейс)
                .WithRequired(e => e.РаспределениеПассажиров)
                .HasForeignKey(e => e.id_РаспределениеПассажиров)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Регулярность>()
                .HasMany(e => e.РегулярныйРейс)
                .WithRequired(e => e.Регулярность)
                .HasForeignKey(e => e.id_Регулярность)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<РегулярныйРейс>()
                .HasMany(e => e.РейсИгрыИгрока1)
                .WithOptional(e => e.РегулярныйРейс1)
                .HasForeignKey(e => e.id_РегулярныйРейс);

            modelBuilder.Entity<РейсИгрыИгрока>()
                .HasMany(e => e.РегулярныйРейс)
                .WithOptional(e => e.РейсИгрыИгрока)
                .HasForeignKey(e => e.id_ОбратногоРейса);

            modelBuilder.Entity<СамолетИгрыИгрока>()
                .HasMany(e => e.Магазин)
                .WithRequired(e => e.СамолетИгрыИгрока)
                .HasForeignKey(e => e.id_СамолетаИгрыИгрока)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<СамолетИгрыИгрока>()
                .HasMany(e => e.Расписание)
                .WithRequired(e => e.СамолетИгрыИгрока)
                .HasForeignKey(e => e.id_Самолет)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<СамолетИгрыИгрока>()
                .HasMany(e => e.СамолетИгрока)
                .WithRequired(e => e.СамолетИгрыИгрока)
                .HasForeignKey(e => e.id_СамолетаИгрыИгрока)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<СтатусДоговора>()
                .HasMany(e => e.ДоговорНаСамолет)
                .WithRequired(e => e.СтатусДоговора)
                .HasForeignKey(e => e.id_СтатусДоговора)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Страна>()
                .HasMany(e => e.Город)
                .WithRequired(e => e.Страна)
                .HasForeignKey(e => e.id_Страна)
                .WillCascadeOnDelete(false);
        }
    }
}
