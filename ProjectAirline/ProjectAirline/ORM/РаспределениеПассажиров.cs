namespace ProjectAirline.ORM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class РаспределениеПассажиров
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public РаспределениеПассажиров()
        {
            ПассажирскийРейс = new HashSet<ПассажирскийРейс>();
        }

        public long id { get; set; }

        [Required]
        [StringLength(2147483647)]
        public string ПиковоеВремяДня { get; set; }

        [Required]
        [StringLength(2147483647)]
        public string Дисперсия { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ПассажирскийРейс> ПассажирскийРейс { get; set; }
    }
}
