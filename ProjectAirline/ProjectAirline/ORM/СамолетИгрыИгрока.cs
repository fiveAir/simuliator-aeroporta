namespace ProjectAirline.ORM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class СамолетИгрыИгрока
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public СамолетИгрыИгрока()
        {
            Магазин = new HashSet<Магазин>();
            Расписание = new HashSet<Расписание>();
            СамолетИгрока = new HashSet<СамолетИгрока>();
        }

        public long id { get; set; }

        public long id_Игрок { get; set; }

        public long id_МодельСамолета { get; set; }

        [Required]
        [StringLength(2147483647)]
        public string ДатаСборки { get; set; }

        public virtual Игрок Игрок { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Магазин> Магазин { get; set; }

        public virtual МодельСамолета МодельСамолета { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Расписание> Расписание { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<СамолетИгрока> СамолетИгрока { get; set; }
    }
}
