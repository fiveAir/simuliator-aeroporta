namespace ProjectAirline.ORM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Местоположение
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Местоположение()
        {
            Аэропорт = new HashSet<Аэропорт>();
        }

        public long id { get; set; }

        public long id_Город { get; set; }

        [Column(TypeName = "real")]
        public double Широта { get; set; }

        [Column(TypeName = "real")]
        public double Долгота { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Аэропорт> Аэропорт { get; set; }

        public virtual Город Город { get; set; }
    }
}
