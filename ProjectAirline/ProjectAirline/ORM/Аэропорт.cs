namespace ProjectAirline.ORM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Аэропорт
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Аэропорт()
        {
            Игрок = new HashSet<Игрок>();
            Перелет = new HashSet<Перелет>();
            Перелет1 = new HashSet<Перелет>();
            СамолетИгрока = new HashSet<СамолетИгрока>();
        }

        public long id { get; set; }

        [Required]
        [StringLength(2147483647)]
        public string Наименование { get; set; }

        public long id_Местоположение { get; set; }

        public virtual Местоположение Местоположение { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Игрок> Игрок { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Перелет> Перелет { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Перелет> Перелет1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<СамолетИгрока> СамолетИгрока { get; set; }
    }
}
