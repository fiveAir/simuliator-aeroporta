namespace ProjectAirline.ORM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Магазин
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_СамолетаИгрыИгрока { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_Авиакомпания { get; set; }

        public long? id_Покупка { get; set; }

        public long? id_Аренда { get; set; }

        public long? id_Лизинг { get; set; }

        public virtual Авиакомпания Авиакомпания { get; set; }

        public virtual Аренда Аренда { get; set; }

        public virtual Лизинг Лизинг { get; set; }

        public virtual Покупка Покупка { get; set; }

        public virtual СамолетИгрыИгрока СамолетИгрыИгрока { get; set; }
    }
}
