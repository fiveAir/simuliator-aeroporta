namespace ProjectAirline.ORM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Перелет
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Перелет()
        {
            РейсИгрыИгрока = new HashSet<РейсИгрыИгрока>();
        }

        public long id { get; set; }

        public long id_АэропортОтправления { get; set; }

        public long id_АэропортПрибытия { get; set; }

        public virtual Аэропорт Аэропорт { get; set; }

        public virtual Аэропорт Аэропорт1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<РейсИгрыИгрока> РейсИгрыИгрока { get; set; }
    }
}
