namespace ProjectAirline.ORM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class СтатусДоговора
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public СтатусДоговора()
        {
            ДоговорНаСамолет = new HashSet<ДоговорНаСамолет>();
        }

        public long id { get; set; }

        [Required]
        [StringLength(2147483647)]
        public string Статус { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ДоговорНаСамолет> ДоговорНаСамолет { get; set; }
    }
}
