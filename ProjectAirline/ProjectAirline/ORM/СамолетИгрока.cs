namespace ProjectAirline.ORM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class СамолетИгрока
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_Игрок { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_СамолетаИгрыИгрока { get; set; }

        public long? id_Договор { get; set; }

        public long? id_ТекущийАэропорт { get; set; }

        public virtual Аэропорт Аэропорт { get; set; }

        public virtual ДоговорНаСамолет ДоговорНаСамолет { get; set; }

        public virtual Игрок Игрок { get; set; }

        public virtual СамолетИгрыИгрока СамолетИгрыИгрока { get; set; }
    }
}
