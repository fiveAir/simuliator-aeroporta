namespace ProjectAirline.ORM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class МодельСамолета
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public МодельСамолета()
        {
            СамолетИгрыИгрока = new HashSet<СамолетИгрыИгрока>();
        }

        public long id { get; set; }

        [Required]
        [StringLength(2147483647)]
        public string Наименование { get; set; }

        [Column(TypeName = "real")]
        public double МаксимальнаяДальность { get; set; }

        [Column(TypeName = "real")]
        public double Скорость { get; set; }

        public long? id_ГрузовойСамолет { get; set; }

        public long? id_ПассажирскийСамолет { get; set; }

        [Column(TypeName = "real")]
        public double РасходТоплива { get; set; }

        [Column(TypeName = "real")]
        public double СтандартнаяСтоимость { get; set; }

        public virtual ГрузовойСамолет ГрузовойСамолет { get; set; }

        public virtual ПассажирскийСамолет ПассажирскийСамолет { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<СамолетИгрыИгрока> СамолетИгрыИгрока { get; set; }
    }
}
