namespace ProjectAirline.ORM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Расписание
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_ДоговорНаРейс { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long id_Самолет { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(2147483647)]
        public string ДатаВремяВылета { get; set; }

        public virtual ДоговорНаРейс ДоговорНаРейс { get; set; }

        public virtual СамолетИгрыИгрока СамолетИгрыИгрока { get; set; }
    }
}
