namespace ProjectAirline.ORM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Игрок
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Игрок()
        {
            РейсИгрыИгрока = new HashSet<РейсИгрыИгрока>();
            СамолетИгрока = new HashSet<СамолетИгрока>();
            СамолетИгрыИгрока = new HashSet<СамолетИгрыИгрока>();
        }

        public long id { get; set; }

        [Column(TypeName = "real")]
        public double Баланс { get; set; }

        [Required]
        [StringLength(2147483647)]
        public string ИгровоеВремя { get; set; }

        [Required]
        [StringLength(2147483647)]
        public string НазваниеАвиакомпании { get; set; }

        public long id_СтартовыйАэропорт { get; set; }

        public virtual Аэропорт Аэропорт { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<РейсИгрыИгрока> РейсИгрыИгрока { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<СамолетИгрока> СамолетИгрока { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<СамолетИгрыИгрока> СамолетИгрыИгрока { get; set; }
    }
}
