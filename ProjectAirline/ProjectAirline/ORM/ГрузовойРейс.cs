namespace ProjectAirline.ORM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ГрузовойРейс
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ГрузовойРейс()
        {
            РейсИгрыИгрока = new HashSet<РейсИгрыИгрока>();
        }

        public long id { get; set; }

        [Column(TypeName = "real")]
        public double ВесГруза { get; set; }

        [Column(TypeName = "real")]
        public double ЦенаТоннаКм { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<РейсИгрыИгрока> РейсИгрыИгрока { get; set; }
    }
}
