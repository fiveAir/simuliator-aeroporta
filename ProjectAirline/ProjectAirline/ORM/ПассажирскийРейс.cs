namespace ProjectAirline.ORM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ПассажирскийРейс
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ПассажирскийРейс()
        {
            РейсИгрыИгрока = new HashSet<РейсИгрыИгрока>();
        }

        public long id { get; set; }

        public long МаксимальноеКоличествоПассажиров { get; set; }

        public long id_РаспределениеПассажиров { get; set; }

        [Column(TypeName = "real")]
        public double? ЦенаПассажирКм { get; set; }

        public virtual РаспределениеПассажиров РаспределениеПассажиров { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<РейсИгрыИгрока> РейсИгрыИгрока { get; set; }
    }
}
