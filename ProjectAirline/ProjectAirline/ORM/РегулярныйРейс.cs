namespace ProjectAirline.ORM
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class РегулярныйРейс
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public РегулярныйРейс()
        {
            РейсИгрыИгрока1 = new HashSet<РейсИгрыИгрока>();
        }

        public long id { get; set; }

        public long? id_ОбратногоРейса { get; set; }

        public long id_Регулярность { get; set; }

        public virtual Регулярность Регулярность { get; set; }

        public virtual РейсИгрыИгрока РейсИгрыИгрока { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<РейсИгрыИгрока> РейсИгрыИгрока1 { get; set; }
    }
}
