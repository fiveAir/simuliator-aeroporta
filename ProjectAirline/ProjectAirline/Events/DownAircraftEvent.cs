﻿using ProjectAirline.ORM;
using ProjectAirline.Tools;
using ProjectAirline.Tools.DataBaseTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAirline.Events
{
    [Serializable]
    public class DownAircraftEvent : Event
    {
        public double Прибыль { get; set; }
        public long Id_Aircraft { get; set; }
        public long Id_Airport { get; set; }
        public long Id_ДоговорНаРейс { get; set; }

        public DownAircraftEvent() : base()
        {

        }

        public DownAircraftEvent(DateTime dataTime, double прибыль, long id_aircraft, string aircraftName, long id_airport, long id_Договор) : base(dataTime)
        {
            Прибыль = прибыль;
            Id_Aircraft = id_aircraft;
            Id_Airport = id_airport;
            Name = "Событие приземления " + aircraftName;
            Id_ДоговорНаРейс = id_Договор;
        }

        public override void Execute()
        {
            Game.DBMutex.WaitOne();
            Аэропорт аэропорт = Airport.GetAirport(Id_Airport);
            Aircraft.GetСамолетИгрока(Id_Aircraft).Аэропорт = аэропорт;
            ДоговорНаРейс договор = Flight.GetContract(Id_ДоговорНаРейс);
            РейсИгрыИгрока рейсИгрыИгрока = Flight.GetFlight(договор);
            if (рейсИгрыИгрока.РазовыйРейс != null)
            {
                Flight.RemoveOneTimeFlight(рейсИгрыИгрока);
            }
            if (рейсИгрыИгрока.РегулярныйРейс1 != null)
            {
                договор.Количество_выполненных++;
            }
            Connection.db.SaveChanges();
            ILog.Write(Name + ": Самолёт приземлился: " + аэропорт.Наименование);
            if (Прибыль != 0) // Если не пустой перелёт
            {
                ILog.Write(Name + ": Начисление прибыли за рейс");
                Player.ChangeBalance(Прибыль);
            }
            Game.DBMutex.ReleaseMutex();
        }
    }
}
