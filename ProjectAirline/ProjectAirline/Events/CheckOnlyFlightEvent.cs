﻿using ProjectAirline.ORM;
using ProjectAirline.Tools;
using ProjectAirline.Tools.DataBaseTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAirline.Events
{
    public class CheckOnlyFlightEvent : Event
    {
        public long Id_ДоговорНаРейс { get; set; }

        public CheckOnlyFlightEvent()
        {

        }

        public CheckOnlyFlightEvent(ДоговорНаРейс договорНаРейс) : base()
        {
            Name = "Событие - Проверка выполненности разового рейса";
            РейсИгрыИгрока рейсИгрыИгрока = 
                (from рейс in Connection.db.РейсИгрыИгрока
                 where рейс.ДоговорНаРейс.id == договорНаРейс.id
                 select рейс).ToList()[0];
            Id_ДоговорНаРейс = договорНаРейс.id;
            StartTime = DateTime.Parse(рейсИгрыИгрока.РазовыйРейс.ДатаВыполнения);
        }

        public override void Execute()
        {
            Game.DBMutex.WaitOne();
            var рейсы =
                (from рейс in Connection.db.РейсИгрыИгрока
                 where рейс.ДоговорНаРейс.id == Id_ДоговорНаРейс
                 select рейс).ToList();
            if (рейсы.Count != 0)
            {
                ILog.Write("Рейс не был исполнен!");
                Flight.CancelOneTimeFlight(рейсы.ToList()[0]);
            }
            Game.DBMutex.ReleaseMutex();
        }
    }
}
