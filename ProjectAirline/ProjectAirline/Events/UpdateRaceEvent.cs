﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectAirline.Tools.DataBaseTools;
using ProjectAirline.Tools;

namespace ProjectAirline.Events
{
    [Serializable]
    public class UpdateRaceEvent : Event
    {
        public long NextEventTime { get; set; } // Временной промежуток между обновлениями (кол-во tick)

        public UpdateRaceEvent() : base()
        {

        }

        public UpdateRaceEvent(DateTime dataTime) : base(dataTime)
        {
            Name = "Генерация новых рейсов";
            NextEventTime = new TimeSpan(1, 0, 0, 0).Ticks;
        }

        public override void Execute()
        {
            Game.DBMutex.WaitOne();
            ILog.Write(Name);
            Flight.GenerateFlights();
            UpdateRaceEvent nextEvent = new UpdateRaceEvent(StartTime + TimeSpan.FromTicks(NextEventTime));
            Calendar.AddEvent(nextEvent);
            Game.DBMutex.ReleaseMutex();
        }
    }
}
