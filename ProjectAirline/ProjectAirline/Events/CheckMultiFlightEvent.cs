﻿using ProjectAirline.ORM;
using ProjectAirline.Tools;
using ProjectAirline.Tools.DataBaseTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAirline.Events
{
    public class CheckMultiFlightEvent : Event
    {
        public long Id_ДоговорНаРейс { get; set; }

        public CheckMultiFlightEvent()
        {

        }

        public CheckMultiFlightEvent(DateTime dateStart, ДоговорНаРейс договорНаРейс) : base(dateStart)
        {
            Name = "Событие - Проверка частоты регулярного рейса";
            Id_ДоговорНаРейс = договорНаРейс.id;
        }

        public override void Execute()
        {
            Game.DBMutex.WaitOne();
            var договоры =
                (from договор in Connection.db.ДоговорНаРейс
                 where договор.id == Id_ДоговорНаРейс
                 select договор).ToList();
            if (договоры.Count != 0) // Если договор ещё существует, то проверяем
            {
                ДоговорНаРейс договорНаРейс = договоры[0];
                РейсИгрыИгрока рейсИгрыИгрока = Flight.GetFlight(договорНаРейс);
                Регулярность регулярность = рейсИгрыИгрока.РегулярныйРейс1.Регулярность;
                if (StartTime >= DateTime.Parse(договорНаРейс.ДатаОкончания)) // Если время совпало с датой окончания договора
                {
                    TimeSpan delay = DateTime.Parse(договорНаРейс.ДатаОкончания) - DateTime.Parse(договорНаРейс.ДатаНачала);
                    if (delay.Days % регулярность.Диапазон == 0 && договорНаРейс.Количество_выполненных < регулярность.КоличествоПовторов)
                    {   // Если диапазон полный, иначе кол-во вылетов не считается
                        ILog.Write("Договор: частость наружена! Выплата неустойки");
                        Player.ChangeBalance(-1 * договорНаРейс.Неустойка);
                    }
                    ILog.Write("Договор завершен");
                    Flight.RemoveRegularFlight(рейсИгрыИгрока); // Удаление рейса
                }
                else
                {
                    if (договорНаРейс.Количество_выполненных == null || договорНаРейс.Количество_выполненных < регулярность.КоличествоПовторов)
                    {
                        ILog.Write("Договор: частость наружена! Выплата неустойки");
                        Player.ChangeBalance(-1 * договорНаРейс.Неустойка);
                    }
                    else
                    {
                        ILog.Write("Договор успешно выполняется");
                    }
                    договорНаРейс.Количество_выполненных = 0;
                    DateTime nextCheck = StartTime + TimeSpan.FromDays(регулярность.Диапазон);
                    nextCheck = nextCheck > DateTime.Parse(договорНаРейс.ДатаОкончания) ? DateTime.Parse(договорНаРейс.ДатаОкончания) : nextCheck;
                    Calendar.AddEvent(new CheckMultiFlightEvent(nextCheck, договорНаРейс)); // Следующая проверка
                }
            }
            Game.DBMutex.ReleaseMutex();
        }
    }
}
