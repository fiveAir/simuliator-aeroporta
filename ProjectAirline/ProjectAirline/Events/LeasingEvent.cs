﻿using ProjectAirline.ORM;
using ProjectAirline.Tools;
using ProjectAirline.Tools.DataBaseTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAirline.Events
{
    public class LeasingEvent : Event
    {
        public long NextEventTime { get; set; } // Временной промежуток между снятием средств со счёта
        public long Id_Договор { get; set; }
        public int CountMount { get; set; }
        public int Duration { get; set; }

        public LeasingEvent()
        {

        }

        /// <summary>
        /// Оформление лизинга
        /// </summary>
        /// <param name="dataTime">Время взятия лизинга</param>
        /// <param name="countMount">Количество оставшихся месяцев оплаты</param>
        /// <param name="duration">Количество изначальных месяцев</param>
        /// <param name="id_Договор">Id договора</param>
        public LeasingEvent(DateTime dataTime, int countMount, int duration, long id_Договор) : base(dataTime)
        {
            Name = "Событие - Погашение лизинга";
            NextEventTime = new TimeSpan(1, 0, 0, 0).Ticks;
            Id_Договор = id_Договор;
            CountMount = countMount;
            Duration = duration;
        }

        public override void Execute()
        {
            Game.DBMutex.WaitOne();
            var query =
                from договор in Connection.db.ДоговорНаСамолет
                where договор.id == Id_Договор
                select договор;
            if (query.Count() == 1) // Есть ли договор, если нет, лизинг остановлен
            {
                ДоговорНаСамолет contract = query.ToList()[0];
                double costMount = Shop.MonthPaymentLease(query.ToList()[0].Лизинг, Duration); // Плата за месяц
                if (Game.Player.Баланс - costMount < 0) // если денег не хватает, самолёт удаляется
                {
                    ILog.Write("Погашение лизинга невозможно! Самолёт " 
                        + contract.СамолетИгрока.ToList()[0].СамолетИгрыИгрока.МодельСамолета.Наименование + " удалён");
                    Aircraft.RemovePlayersAircraft(contract.СамолетИгрока.ToList()[0]);
                }
                else
                {
                    ILog.Write("Снятие денег за лизинг");
                    Player.ChangeBalance(-1 * costMount);
                    if (CountMount == 0)
                    {
                        ILog.Write("Лизинг оплачен, самолёт сохранён");
                        foreach (СамолетИгрока самолет in query.ToList()[0].СамолетИгрока)
                        {
                            самолет.ДоговорНаСамолет.Лизинг = null;

                            #region Указание стоимости самолёта
                            Покупка покупка = new Покупка()
                            {
                                СтоимостьСамолета = самолет.СамолетИгрыИгрока.МодельСамолета.СтандартнаяСтоимость
                            };
                            Connection.db.Покупка.Add(покупка);
                            Connection.db.SaveChanges();
                            #endregion

                            самолет.ДоговорНаСамолет.Покупка = покупка;
                        }
                        contract.id_СтатусДоговора = 2;
                        Connection.db.SaveChanges();
                    }
                    else
                    {
                        Calendar.AddEvent(new LeasingEvent(StartTime + TimeSpan.FromTicks(NextEventTime), CountMount - 1, Duration, Id_Договор));
                    }
                }
                Game.DBMutex.ReleaseMutex();
                return;
            }
            ILog.Write(Name + " - лизинг остановлен");
            Game.DBMutex.ReleaseMutex();
        }
    }
}
// MonthPaymentLease