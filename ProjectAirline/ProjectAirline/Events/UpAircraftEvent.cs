﻿using ProjectAirline.ORM;
using ProjectAirline.Tools;
using ProjectAirline.Tools.DataBaseTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAirline.Events
{
    [Serializable]
    public class UpAircraftEvent : Event
    {
        public long Id_Contract { get; set; }
        public long Id_Aircraft { get; set; }

        public UpAircraftEvent() : base()
        {

        }

        public UpAircraftEvent(Расписание record) : base(DateTime.Parse(record.ДатаВремяВылета))
        {
            Name = "Событие взлёта " + record.СамолетИгрыИгрока.МодельСамолета.Наименование;
            Id_Aircraft = record.СамолетИгрыИгрока.id;
            Id_Contract = record.ДоговорНаРейс.id;
        }

        public override void Execute()
        {
            Game.DBMutex.WaitOne();
            ДоговорНаРейс договор = Flight.GetContract(Id_Contract);
            if (договор == null)
            {
                Game.DBMutex.ReleaseMutex();
                ILog.Write(Name + ": Рейс был отменён!");
                return;
            }
            СамолетИгрыИгрока самолетИгрыИгрока = Aircraft.GetAircraft(Id_Aircraft);
            if (самолетИгрыИгрока == null)
            {
                Game.DBMutex.ReleaseMutex();
                ILog.Write(Name + ": Самолёт был продан!");
                return;
            }
            Расписание расписание = TimeTable.GetRecord(договор, самолетИгрыИгрока, StartTime); // Восстанавливаем (загружаем) расписание
            
            if (Aircraft.IsAircraftReadyToFlight(расписание)) // На месте ли самолёт
            {
                double тратыНаПерелет = Flight.GetFlightsCost(расписание);
                double приыльЗаПерелет = Flight.GetFlightsIncome(расписание);
                ILog.Write(Name + ": Заправка самолёта");
                Player.ChangeBalance(-1 * тратыНаПерелет);
                Aircraft.GetAircraft(самолетИгрыИгрока).Аэропорт = null;
                Connection.db.SaveChanges();
                DateTime датаПосадки = StartTime + TimeTable.GetFlyingTime(расписание);
                Перелет перелет = TimeTable.GetFlyFromTimeTable(расписание); // Получение перелёта
                Calendar.AddEvent(new DownAircraftEvent(датаПосадки, приыльЗаПерелет, Id_Aircraft, самолетИгрыИгрока.МодельСамолета.Наименование, перелет.Аэропорт.id, договор.id)); // Генерация события посадки
                ILog.Write(Name + ": Самолёт взлетел из точки " + перелет.Аэропорт1.Наименование + ". Дата посадки " + датаПосадки.ToString());
            }
            else
            {
                ILog.Write(Name + ": Самолёта на точке нет! Перелёт не состоится!");
            }
            TimeTable.RemoveTimeTableRecord(расписание); // Удаление строки расписания
            Game.DBMutex.ReleaseMutex();
        }
    }
}
