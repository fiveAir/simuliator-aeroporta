﻿using ProjectAirline.ORM;
using ProjectAirline.Tools;
using ProjectAirline.Tools.DataBaseTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAirline.Events
{
    public class RentEvent : Event
    {
        public long NextEventTime { get; set; } // Временной промежуток между снятием средств со счёта
        public long Id_Договор { get; set; }

        public RentEvent()
        {

        }

        public RentEvent(DateTime dataTime, long id_Договор) : base(dataTime)
        {
            Name = "Событие - Плата за аренду";
            NextEventTime = new TimeSpan(1, 0, 0, 0).Ticks;
            Id_Договор = id_Договор;
        }

        public override void Execute()
        {
            Game.DBMutex.WaitOne();
            var query =
                from договор in Connection.db.ДоговорНаСамолет
                where договор.id == Id_Договор
                select договор;
            if (query.Count() == 1)
            {
                double costMount = query.ToList()[0].Аренда.СтоимостьАрендыМес;
                ILog.Write(Name);
                Player.ChangeBalance(-1 * costMount);
                Calendar.AddEvent(new RentEvent(StartTime + TimeSpan.FromTicks(NextEventTime), Id_Договор));
                Game.DBMutex.ReleaseMutex();
                return;
            }
            ILog.Write(Name + " - аренда прекращена");
            Game.DBMutex.ReleaseMutex();
        }
    }
}
