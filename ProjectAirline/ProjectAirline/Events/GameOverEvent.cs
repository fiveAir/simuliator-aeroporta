﻿using ProjectAirline.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAirline.Events
{
    public class GameOverEvent : Event
    {
        public GameOverEvent()
        {

        }

        public GameOverEvent(DateTime dataTime) : base(dataTime)
        {
            Name = "Событие - Конец игры";
        }

        public override void Execute()
        {
            Game.DBMutex.WaitOne();
            ILog.Write("Конец игры!");
            Game.DBMutex.ReleaseMutex();
        }
    }
}
