﻿using ProjectAirline.Tools;
using ProjectAirline.Tools.DataBaseTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAirline.Events
{
    [Serializable]
    public class UpdateShopEvent : Event
    {
        public long NextEventTime { get; set; } // Временной промежуток между обновлениями (кол-во tick)

        public UpdateShopEvent() : base()
        {

        }

        public UpdateShopEvent(DateTime dataTime) : base(dataTime)
        {
            Name = "Генерация новых самолётов";
            NextEventTime = new TimeSpan(3, 0, 0, 0).Ticks;
        }

        public override void Execute()
        {
            Game.DBMutex.WaitOne();
            ILog.Write(Name);
            Shop.RefreshAllAircrafts();
            UpdateShopEvent nextEvent = new UpdateShopEvent(StartTime + TimeSpan.FromTicks(NextEventTime));
            Calendar.AddEvent(nextEvent);
            Game.DBMutex.ReleaseMutex();
        }
    }
}
