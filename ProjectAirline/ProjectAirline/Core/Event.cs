﻿using ProjectAirline.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ProjectAirline
{
    [XmlInclude(typeof(UpdateRaceEvent))]
    [XmlInclude(typeof(UpdateShopEvent))]
    [XmlInclude(typeof(RentEvent))]
    [XmlInclude(typeof(LeasingEvent))]
    [XmlInclude(typeof(CheckOnlyFlightEvent))]
    [XmlInclude(typeof(CheckMultiFlightEvent))]
    [XmlInclude(typeof(DownAircraftEvent))]
    [XmlInclude(typeof(UpAircraftEvent))]
    [Serializable]
    public abstract class Event
    {
        public string Name { get; set; }
        public DateTime StartTime { get; set; }

        public Event()
        {

        }

        public Event(DateTime startTime)
        {
            StartTime = startTime;
        }
        
        public abstract void Execute();

        public override string ToString()
        {
            return Name;
        }
    }
}
