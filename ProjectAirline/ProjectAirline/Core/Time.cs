﻿using ProjectAirline.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectAirline
{
    public static class Time
    {
        private static DateTime currentTime = DateTime.Now; // Текущее время в игре
        private static double speedOfTime = 1; // Скорость хода времени
        private static bool IsUpdating = false; // Работа потока TickThread
        private static Mutex mutex = new Mutex(); // Контроль доступа к ходу временем

        public static DateTime CurrentTime
        {
            get
            {
                return currentTime;
            }
        }

        public static double SpeedOfTime
        {
            get
            {
                return speedOfTime;
            }
            set
            {
                if (value >= 0)
                {
                    speedOfTime = value;
                }
            }
        }

        public static void Initialize(DateTime startTime)
        {
            // Нельзя инициализировать время в игре, если оно не остановлено
            IsUpdating = false;
            mutex.WaitOne();
            currentTime = new DateTime(startTime.Ticks);
            speedOfTime = 1;
            mutex.ReleaseMutex();
        }

        public static void Start()
        {
            // Пока не завершится поток TickThread, новый поток не создаётся
            mutex.WaitOne();
            ILog.Write("Время запущено");
            IsUpdating = true;
            new Thread(TickThread).Start();
            mutex.ReleaseMutex();
        }

        public static void Stop()
        {
            IsUpdating = false;
        }

        private static void TickThread()
        {
            mutex.WaitOne();
            DateTime prevTime = new DateTime(DateTime.Now.Ticks); // Время предыдущего тика
            DateTime timeNow; // Текущее время
            TimeSpan deltaTimeReal; // Реально прошедшее время
            double deltaTimeGame; // Прошедшее время в игре
            while (IsUpdating)
            {
                timeNow = DateTime.Now;
                deltaTimeReal = timeNow - prevTime;
                deltaTimeGame = deltaTimeReal.Ticks * speedOfTime;
                currentTime += TimeSpan.FromTicks((long)deltaTimeGame); // Увеличение текущего времени
                Calendar.ExecuteEvents(currentTime); // Исполнение прошедших событий
                prevTime = timeNow;
            }
            ILog.Write("Время остановлено");
            mutex.ReleaseMutex();
        }
    }
}
