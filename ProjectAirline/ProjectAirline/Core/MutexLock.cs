﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProjectAirline.Core
{
    class MutexLock : IDisposable
    {
        private Mutex lockedMutex;

        public MutexLock(Mutex Mutex)
        {
            lockedMutex = Mutex;
            Mutex.WaitOne();
        }

        public void Dispose()
        {
            lockedMutex.ReleaseMutex();
        }
    }
}
