﻿using ProjectAirline.Events;
using ProjectAirline.ORM;
using ProjectAirline.Tools;
using ProjectAirline.Tools.DataBaseTools;
using ProjectAirline.UserInterface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ProjectAirline
{
    public static class Game
    {
        private static Игрок player;
        public static Игрок Player
        {
            get
            {
                return player;
            }
        }
        public static Mutex DBMutex = new Mutex();

        /// <summary>
        /// Инициализация всех событий
        /// Начало новой игры
        /// </summary>
        public static void NewGame(Игрок игрок)
        {
            ILog.Write("Создание новой игры");
            Initialize(игрок);
        }

        /// <summary>
        /// Загрузка событий
        /// Продолжение игры
        /// </summary>
        public static void ContinueGame(Игрок игрок)
        {
            ILog.Write("Загрузка игры");
            Initialize(игрок);
        }

        /// <summary>
        /// Инициализация игрока, календаря и времени
        /// </summary>
        private static void Initialize(Игрок игрок)
        {
            if (player != null)
            {
                throw new Exception("Игра уже инициализирована!");
            }
            else
            {
                player = игрок;
                ILog.Write("Загружен игрок. Время игрока " + player.ИгровоеВремя);
                Time.Initialize(DateTime.Parse(player.ИгровоеВремя));
                bool FileExists = File.Exists("User_" + игрок.id.ToString() + ".xml");
                switch (FileExists)
                {
                    case true:
                        ILog.Write("Загрузка данных в календарь");
                        Calendar.Initialize(Deserializable());
                        break;
                    case false:
                        ILog.Write("Инициализация календаря");
                        Calendar.Initialize(new List<Event>());
                        Calendar.AddEvent(new UpdateShopEvent(Time.CurrentTime));
                        Calendar.AddEvent(new UpdateRaceEvent(Time.CurrentTime));
                        break;
                }

                Time.Start(); // Запуск времени
                new MainWindow().Show();
            }
        }

        public static void SaveAndExit()
        {
            if (player == null) throw new Exception("Игра не была инициализирована!");

            Time.Stop();
            SaveGame();

            Calendar.Initialize(new List<Event>()); // Очистка календаря
            player = null;
            new AutorizationWindow().Show();
        }

        public static void Save()
        {
            if (player == null) throw new Exception("Игра не была инициализирована!");

            Time.Stop();
            SaveGame();
            Time.Start();
        }

        private static void SaveGame()
        {
            DBMutex.WaitOne();
            Serializable(Calendar.ListEvents);
            player.ИгровоеВремя = Time.CurrentTime.ToString(); // Сохранение игрока в БД
            ILog.Write("Время игрока сохранено в БД: " + player.ИгровоеВремя);
            Connection.db.SaveChanges();
            DBMutex.ReleaseMutex();
        }

        private static void Serializable(List<Event> events)
        {
            ILog.Write("Сериализация в файл " + "User_" + Player.id.ToString() + ".xml");
            XmlSerializer Convert = new XmlSerializer(typeof(List<Event>));
            using (FileStream fileStream = new FileStream("User_" + Player.id.ToString() + ".xml", FileMode.OpenOrCreate))
            {
                Convert.Serialize(fileStream, events);
            }
        }

        private static List<Event> Deserializable()
        {
            ILog.Write("Десериализация из файла " + "User_" + Player.id.ToString() + ".xml");
            XmlSerializer Serialization = new XmlSerializer(typeof(List<Event>));
            using (FileStream fileStream = new FileStream("User_" + Player.id.ToString() + ".xml", FileMode.OpenOrCreate))
            {
                return (List<Event>)Serialization.Deserialize(fileStream);
            }
        }
    }
}