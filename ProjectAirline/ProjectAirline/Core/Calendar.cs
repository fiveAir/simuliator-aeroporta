﻿using ProjectAirline.Tools;
using System;
using System.Collections.Generic;
using System.Threading;

namespace ProjectAirline
{
    public static class Calendar
    {
        private static List<Event> listEvents = new List<Event>();
        private static Mutex mutex = new Mutex();

        public static List<Event> ListEvents
        {
            get
            {
                return listEvents;
            }
        }

        public static void Initialize(List<Event> events)
        {
            // Не трогать список событий, пока с ним работают потоки
            mutex.WaitOne();
            ILog.Write("В календарь загружено " + events.Count.ToString() + " событий");
            listEvents = events;
            mutex.ReleaseMutex();
        }

        public static void AddEvent(Event e)
        {
            //Добавление происходит по возрастанию параметра Event.DateTime,
            //т.е. список отсортированн по возрастанию
            mutex.WaitOne();
            ILog.Write("В календарь добавлено событие " + e.Name);
            for (int i = 0; i < listEvents.Count; i++)
            {
                if (e.StartTime < listEvents[i].StartTime)
                {
                    listEvents.Insert(i, e);
                    mutex.ReleaseMutex();
                    return;
                }
            }
            listEvents.Add(e);
            mutex.ReleaseMutex();
        }
        
        public static void ExecuteEvents(DateTime currentTime)
        {
            mutex.WaitOne();
            try
            {
                while (listEvents.Count != 0 && listEvents[0].StartTime <= currentTime)
                {
                    ILog.Write("Запуск потока на событие " + listEvents[0].Name + ". Время запуска " + listEvents[0].StartTime);
                    new Thread(ExecuteEvent).Start(listEvents[0]);
                    listEvents.RemoveAt(0);
                }
            }
            finally
            {
                mutex.ReleaseMutex();
            }
        }

        private static void ExecuteEvent(object e)
        {
            (e as Event).Execute();
        }
    }
}
